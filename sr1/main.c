#include <stdlib.h>
#include <stdio.h>
#include <check.h>
#include "module.h"


#define using(VARNAME, INITVALUE)     for (int __##VARNAME = 1; __##VARNAME; )         for (Set * VARNAME = INITVALUE; __##VARNAME; __##VARNAME--, Set_free(VARNAME))

#define LEN(ARRAY) sizeof(ARRAY)/sizeof(ARRAY[0])

#define INSERTALL(VARNAME, ARRAY)     for (int VARNAME##i = 0; VARNAME##i < LEN(ARRAY); VARNAME##i++)         Set_insert(VARNAME, ARRAY[VARNAME##i]);

#define ARRAYEQUALS(A, B) arrayEquals(A, LEN(A), B, LEN(B))

static int arrayEquals(int a[], int alen, int b[], int blen) {
    if (alen != blen) return 0;
    for (int i = 0; i < alen; i++) {
        if (a[i] != b[i]) return 0;
    }
    return 1;
}

// TESTS BEGIN

// common tests

START_TEST (size_new_empty) {
        using(s, Set_new()) {
                ck_assert_int_eq(0, Set_size(s));
            }
    }
END_TEST

START_TEST (size_removeFromNew_zero) {
        using(s, Set_new()) {
                Set_remove(s, 10);
                ck_assert_int_eq(0, Set_size(s));
            }
    }
END_TEST

START_TEST (size_insertOne_one) {
        using(s, Set_new()) {
                Set_insert(s, 10);
                ck_assert_int_eq(1, Set_size(s));
            }
    }
END_TEST

START_TEST (size_insertOneRemoveSame_zero) {
        using(s, Set_new()) {
                const int val = 10;
                Set_insert(s, val);
                Set_remove(s, val);
                ck_assert_int_eq(0, Set_size(s));
            }
    }
END_TEST

START_TEST (size_insertTwoRemoveFirst_one) {
        using(s, Set_new()) {
                const int val = 10;
                const int val2 = -10;
                Set_insert(s, val);
                Set_insert(s, val2);
                Set_remove(s, val);
                ck_assert_int_eq(1, Set_size(s));
            }
    }
END_TEST

START_TEST (contains_notInserted_false) {
        using(s, Set_new()) {
                ck_assert(false == Set_contains(s, 10));
            }
    }
END_TEST

START_TEST (contains_inserted_true) {
        using(s, Set_new()) {
                const int val = 10;
                Set_insert(s, val);
                ck_assert(true == Set_contains(s, val));
            }
    }
END_TEST

START_TEST (contains_insertedAndRemoved_false) {
        using(s, Set_new()) {
                const int val = 10;
                Set_insert(s, val);
                Set_remove(s, val);
                ck_assert(false == Set_contains(s, val));
            }
    }
END_TEST

START_TEST (copyToArr_insertThreeReverseOrder_sortedAsc) {
        using(s, Set_new()) {
                const int vals[] = {100, 2, -13};
                INSERTALL(s, vals);
                int expected[] = {-13, 2, 100};
                int result[LEN(vals)];
                Set_copyToArraySorted(s, result);
                ck_assert(ARRAYEQUALS(expected, result));
            }
    }
END_TEST

START_TEST (equals_empty_asSelf) {
        using(s, Set_new()) {
                ck_assert(Set_equals(s, s));
            }
    }
END_TEST

START_TEST (equals_withValues_asSelf) {
        using(s, Set_new()) {
                const int vals[] = {100, 2, -13};
                INSERTALL(s, vals);
                ck_assert(Set_equals(s, s));
            }
    }
END_TEST

START_TEST (equals_twoWithSameInserted_true) {
        using(s, Set_new())
                using(s2, Set_new()) {
                        const int vals[] = {100, 2, -13};
                        INSERTALL(s, vals);
                        INSERTALL(s2, vals);
                        ck_assert(Set_equals(s, s2));
                    }
    }
END_TEST

START_TEST (equalsArr_emptySetEmptyArr_true) {
        using(s, Set_new()) {
                ck_assert(Set_equalsArray(s, NULL, 0));
            }
    }
END_TEST

START_TEST (equalsArr_fillFromArr_equals) {
        using(s, Set_new()) {
                int vals[] = {100, 2, -13};
                INSERTALL(s, vals);
                ck_assert(Set_equalsArray(s, vals, LEN(vals)));
            }
    }
END_TEST

START_TEST (equalsArr_fillFromArr_equalsReversedArr) {
        using(s, Set_new()) {
                const int vals[] = {100, 2, -13};
                int expected[] = {-13, 2, 100};
                INSERTALL(s, vals);
                ck_assert(Set_equalsArray(s, expected, LEN(expected)));
            }
    }
END_TEST

// 2

// TESTS END

Suite * create_test_suite(void);

int main(void) {
    SRunner *sr = srunner_create(create_test_suite());
    // srunner_set_fork_status(sr, CK_NOFORK);  // enable this when debugging
    srunner_run_all(sr, CK_VERBOSE);
    int number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return number_failed;
}

Suite * create_test_suite(void) {
    Suite *s = suite_create("Set");
    TCase *tc_core = tcase_create("Core");
    //

    // common
    tcase_add_test(tc_core, size_new_empty);
    tcase_add_test(tc_core, size_removeFromNew_zero);
    tcase_add_test(tc_core, size_insertOne_one);
    tcase_add_test(tc_core, size_insertOneRemoveSame_zero);
    tcase_add_test(tc_core, size_insertTwoRemoveFirst_one);
    tcase_add_test(tc_core, contains_notInserted_false);
    tcase_add_test(tc_core, contains_inserted_true);
    tcase_add_test(tc_core, contains_insertedAndRemoved_false);
    tcase_add_test(tc_core, copyToArr_insertThreeReverseOrder_sortedAsc);
    tcase_add_test(tc_core, equals_empty_asSelf);
    tcase_add_test(tc_core, equals_withValues_asSelf);
    tcase_add_test(tc_core, equals_twoWithSameInserted_true);
    tcase_add_test(tc_core, equalsArr_emptySetEmptyArr_true);
    tcase_add_test(tc_core, equalsArr_fillFromArr_equals);
    tcase_add_test(tc_core, equalsArr_fillFromArr_equalsReversedArr);

    // 2

    //
    suite_add_tcase(s, tc_core);
    return s;
}
