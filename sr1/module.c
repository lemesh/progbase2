#include "module.h"

#define CAPACITY 20;

static int cmpfunc (const void * a, const void * b);

struct __Set {
    int * items;
    int length;
    int capacity;
};

Set * Set_new(void) {
    Set * self = malloc(sizeof(Set));
    self->capacity = CAPACITY;
    self->length = 0;
    self->items = malloc(sizeof(int) * self->capacity);
    return self;
}

Set * Set_newCopy(Set * set) {
    Set * self = malloc(sizeof(Set));
    self->items = set->items;
    self->capacity = set->capacity;
    self->length = set->length;
    return self;
}

void Set_free(Set * self) {
    free(self->items);
    free(self);
}

void Set_insert(Set * self, int value) {
    if (Set_contains(self, value)) return;
    if (self->length == self->capacity) {
        self->capacity += self->capacity;
        self->items = realloc(self->items, sizeof(int) * self->capacity);
    }
    self->items[self->length] = value;
    self->length++;
}

void Set_insertSet(Set * self, Set * other) {
    for (int i = 0; i < other->length; i++) {
        bool exist = false;
        for (int j = 0; j < self->length; i++) {
            if (self->items[i] == other->items[i]) {
                exist = true;
            }
        }
        if (!exist) {
            Set_insert(self, other->items[i]);
        }
    }
}

void Set_remove(Set * self, int value) {
    for (int i = 0; i < self->length; i++) {
        if (self->items[i] == value) {
            for (int j = i; j < self->length; j++) {
                self->items[j] = self->items[j + 1];
            }
            self->length--;
            return;
        }
    }
}

void Set_removeSet(Set * self, Set * other) {

}

bool Set_contains(Set * self, int value) {
    for (int i = 0; i < self->length; i++) {
        if (self->items[i] == value) return true;
    }
    return false;
}

bool Set_containsAny(Set * self, int value[], size_t arrayLen) {
    for (int i = 0; i < arrayLen; i++) {
        if (Set_contains(self, value[0])) {
            return true;
        }
    }
    return false;
}

size_t Set_size(Set * self) {
    return self->length;
}
/*
    use qsort() here, ascending order
*/
void Set_copyToArraySorted(Set * self, int array[]) {
    for (int i = 0; i < self->length; i++) {
        array[i] = self->items[i];
    }
    qsort(array, self->length, sizeof(int), cmpfunc);
}

// set operations
Set * Set_newUnion(Set * self, Set * other) {
    Set * new = malloc(sizeof(Set));
    new->capacity = CAPACITY;
    new->items = malloc(sizeof(int) * self->capacity);
    new->length = 0;
    for (int i = 0; i < self->length; i++) {
        Set_insert(new,self->items[i]);
    }
    for (int j = 0; j < other->length; j++) {
        if (!Set_contains(self, other->items[j])) {
            Set_insert(new, other->items[j]);
        }
    }
    return new;
}

bool Set_equals(Set * self, Set * other) {
    if (self->length == other->length) {
        for (int i = 0; i < self->length; i++) {
            if(!Set_contains(self, other->items[i])) return false;
        }
        return true;
    }
    return false;
}
/*
    position independent
*/
bool Set_equalsArray(Set * self, int array[], size_t arrayLen) {
    if (self->length != arrayLen) return false;
    for (int i = 0; i < self->length; i++) {
        for (int j = 0; j < arrayLen; j++) {
            if(!Set_contains(self, array[j])) {
                return false;
            }
        }
    }
    return true;
}

static int cmpfunc (const void * a, const void * b) {
    return ( *(int*)a - *(int*)b );
}