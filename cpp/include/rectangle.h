#include <iostream>
#include <string>
using namespace std;

class Rectangle
{
    string id;
    int length;
    int width;

  public:

    Rectangle(string id, int len, int wid);

    string get_id();
    int get_length();
    int get_width();

    int rectangleEArea();
};