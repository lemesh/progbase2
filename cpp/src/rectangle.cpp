#include <iostream>
#include <rectangle.h>

Rectangle::Rectangle(string id, int len, int wid){
    id = id;
    length = len;
    width = wid;
}

string Rectangle::get_id(){
    return this->id;
}
int Rectangle::get_length(){
    return length;
}
int Rectangle::get_width(){
    return width;
}

int Rectangle::rectangleEArea(){
    return length*width;
}