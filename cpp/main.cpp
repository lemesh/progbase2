#include <iostream>
#include <vector>
#include <limits>
#include <rectangle.h>

int validInput(string message);

int main()
{
    vector<Rectangle *> rects;

    int prog = 1;
    while (prog)
    {
        cout << "Push [1] for print | [2] for add | [3] for rects with area bigger than S | [4] exit" << endl;
        switch (validInput("Choose command:"))
        {
        case 1:
        {
            if (!rects.empty())
            {
                for (auto i : rects)
                {
                    cout << "{ ID:" << i->get_id() << endl;
                    cout << "Length:" << i->get_length() << endl;
                    cout << "Width:" << i->get_width() << " }" << endl;
                }
            }
            else
            {
                cout << "Empty" << endl;
            }
            break;
        }
        case 2:
        {
            cout << "Enter the ID of your rect:" << endl;
            string id;
            getline(cin, id);
            cout << id << endl;
            rects.push_back(new Rectangle(id, validInput("Enter the length: "), validInput("Enter the width: ")));
            break;
        }
        case 3:
        {
            break;
        }
        case 4:
        {
            prog = 0;
            break;
        }
        }
    }

    return 0;
}

int validInput(string message)
{
    int x = 0;
    while (x <= 0)
    {
        cout << message << endl;
        cin >> x;

        if (cin.fail())
        {
            cin.clear(); // clears error flags
        }
        cin.ignore(numeric_limits<streamsize>::max(), '\n');
    }
    return x;
}