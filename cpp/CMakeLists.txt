cmake_minimum_required(VERSION 3.5.1)
#Name your project
project(a.out)

#Bring the headers, such as nlp.h into the project
include_directories(include)

#The file(GLOB...) allows for wildcard additions:
file(GLOB SOURCES "src/*.cpp")

#Specify that this project is an executable and is compiled from SOURCES
add_executable(${PROJECT_NAME} main.cpp ${SOURCES})

#Set compiler flags
SET(CMAKE_CXX_FLAGS "-std=c++14 -Werror -Wall -pedantic-errors")
