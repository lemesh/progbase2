#include <vector>
#include <iostream>
#include <QtXml>

using namespace std;

class Student {
public:
    QString name;
    int age;

};

class Group
{
public:
    QString name;
    vector<Student> students;
};

void printGroup(const Group & g);
string toXmlString(const Group & g);
Group fromXmlString(string str);

int main(int argc, char *argv[])
{
    Group g;
    g.name = "KP72";
    g.students.push_back(Student{"Anna", 17});
    g.students.push_back(Student{"Ivan", 18});
    g.students.push_back(Student{"Misha", 19});

    printGroup(g);

    string str = toXmlString(g);

    cout << "----------------------------" << endl
        << str << endl
        << "----------------------------" << endl;

    Group g2 = fromXmlString(str);
    printGroup(g2);

    return 0;
}

Group fromXmlString(string str) {
    QDomDocument doc;
    if(!doc.setContent(QString::fromStdString(str))) {
        cerr << "xml parsing error" << endl;
        return Group();
    }
    QDomElement root = doc.documentElement();
    Group g;
    g.name = root.attribute("name");
    for(int i = 0; i < root.childNodes().length(); i++) {
        QDomNode node = root.childNodes().at(i);
        QDomElement el = node.toElement();
        Student s;
        s.name = el.attribute("name");
        s.age = el.attribute("age").toInt();
        g.students.push_back(s);
    }

    return g;
}

string toXmlString(const Group &g) {
    QDomDocument doc;
    QDomElement groupEl = doc.createElement("group");
    groupEl.setAttribute("name", g.name);
    for (const Student & st : g.students) {
        QDomElement stEl = doc.createElement("student");
        stEl.setAttribute("name", st.name);
        stEl.setAttribute("age", st.age);
        groupEl.appendChild(stEl);

    }
    doc.appendChild(groupEl);
    return doc.toString().toStdString();

}

void printGroup(const Group &g) {
    cout << "name: " << g.name.toStdString() << endl
         << "students(" << g.students.size() << "): " << endl;
    for (const Student & s : g.students) {
        cout << " student: " << s.name.toStdString() << " | " << s.age << endl;
    }
}
