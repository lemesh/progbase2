#ifndef CAPITALS
#define CAPITALS
#include <QString>

class Capitals {
public:
    Capitals();
    Capitals(QString Name, int Populaion, float Square);

    QString getName();
    int getPopulation();
    float getSquare();

    void setName(QString Name);
    void setPopulation(int Populaion);
    void setSquare(float Square);

private:
    QString Name;
    int Populaion;
    float Square;
};

#endif // CAPITALS

