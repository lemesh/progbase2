#include "adddialog.h"
#include "ui_adddialog.h"
#include "capitals.h"
#include "mainwindow.h"

addDialog::addDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::addDialog)
{
    ui->setupUi(this);
}

addDialog::~addDialog()
{
    delete ui;
}

void addDialog::on_addButton_clicked()
{
    QString Name = ui->capitalNameEl->text();
    int population = ui->populationEl->text().toInt();
    float square = ui->squareEl->text().toDouble();
    MainWindow mainWindow;
    mainWindow.capitals.push_back(Capitals(Name, population, square));
}
