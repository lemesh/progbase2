#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <vector>
#include "capitals.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    std::vector<Capitals> capitals;

    QString print();

private slots:
    void on_addButton_clicked();

    void on_editButton_clicked();

    void on_executeButton_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
