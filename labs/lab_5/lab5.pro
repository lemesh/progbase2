#-------------------------------------------------
#
# Project created by QtCreator 2018-04-14T18:46:58
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG = c++11

TARGET = lab5
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    capitals.cpp \
    adddialog.cpp \
    editdialog.cpp

HEADERS  += mainwindow.h \
    capitals.h \
    adddialog.h \
    editdialog.h

FORMS    += mainwindow.ui \
    adddialog.ui \
    editdialog.ui
