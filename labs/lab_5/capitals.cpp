#include "capitals.h"

Capitals::Capitals() {
    Name = "";
    Populaion = 0;
    Square = 0;
}

Capitals::Capitals(QString Name, int Populaion, float Square) {
    this->Name = Name;
    this->Populaion = Populaion;
    this->Square = Square;
}

QString Capitals::getName() {
    return Name;
}

int Capitals::getPopulation() {
    return Populaion;
}

float Capitals::getSquare() {
    return Square;
}

void Capitals::setName(QString Name) {
    this->Name = Name;
}

void Capitals::setPopulation(int Populaion) {
    this->Populaion = Populaion;
}

void Capitals::setSquare(float Square) {
    this->Square = Square;
}
