#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "capitals.h"
#include <vector>
#include "adddialog.h"
#include "editdialog.h"
#include <string>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

QString MainWindow::print() {
    QString str;
    for (int i = 0; i < capitals.size(); i++) {
        str.append(capitals[i].getName());
    }
    return str;
}

void MainWindow::on_addButton_clicked()
{
    addDialog dialog;
    dialog.exec();
    ui->showCapitals->setText(print());
}

void MainWindow::on_editButton_clicked()
{
    editDialog dialog;
    dialog.exec();
}

void MainWindow::on_executeButton_clicked()
{
    int execute = ui->executeValue->text().toInt();
}
