#ifndef DIALOGADD_H
#define DIALOGADD_H

#include <QDialog>
#include <capital.h>

namespace Ui {
class DialogAdd;
}

class DialogAdd : public QDialog
{
    Q_OBJECT

public:
    explicit DialogAdd(QWidget *parent = 0);
    ~DialogAdd();
signals:
    void sendCapital(Capital *);

private slots:
    void on_buttonBox_accepted();

private:
    Ui::DialogAdd *ui;
};

#endif // DIALOGADD_H
