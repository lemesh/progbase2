#include "clientmenu.h"
#include "ui_clientmenu.h"
#include <QTcpSocket>
#include <QMessageBox>
#include <QListWidgetItem>
#include <QVariant>
#include <dialogadd.h>
#include <dialogedit.h>
#include <jsonserialization.h>

static Capital * getCapitalFromList(QListWidgetItem *item);

ClientMenu::ClientMenu(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ClientMenu)
{
    ui->setupUi(this);
}

ClientMenu::~ClientMenu()
{
    delete ui;
}

void ClientMenu::filesList(QString str) {
    if (ui->fileList->count() > 0) { ui->fileList->clear(); }
    JsonSerialization json;
    QStringList list = json.getFileResponse(str);
    for (int i = 0; i < list.size(); i++) {
        ui->fileList->addItem(list.at(i));
    }

}

void ClientMenu::capitalClear()
{
    ui->addButton->setEnabled(true);
    ui->capitalList->setEnabled(true);
    if (ui->capitalList->count() > 0) {
        for (int i = 0; i < ui->capitalList->count(); i++) {
            QListWidgetItem * item = ui->capitalList->takeItem(i);
            i--;
            delete item;
        }
    }
}

void ClientMenu::addToList(QString str)
{
    JsonSerialization json;
    Capital * c = json.jsonToCapital(str);
    QListWidgetItem * item = new QListWidgetItem();
    QVariant variant;
    variant.setValue(c);
    item->setData(Qt::UserRole, variant);
    item->setText(c->caption());
    ui->capitalList->addItem(item);
}

void ClientMenu::editList(QString str) {
    JsonSerialization json;
    Capital * c = json.jsonToCapital(str);
    for (int i = 0; i < ui->capitalList->count(); i++) {
        QListWidgetItem * item = ui->capitalList->item(i);
        Capital * c2 = getCapitalFromList(item);
        if (c2->Id() == c->Id()) {
            c2->setName(c->Name());
            c2->setPopulation(c->Population());
            c2->setSquare(c->Square());
            c2->setLongitude(c->Longitude());
            c2->setLatitude(c->Latitude());
            item->setText(c2->caption());
            emit(on_capitalList_itemSelectionChanged());
        }
    }
}

void ClientMenu::removeFromList(QString str)
{
    JsonSerialization json;
    Capital * c = json.jsonToCapital(str);
    QList<QListWidgetItem *> selectedItems = ui->capitalList->selectedItems();
    for (QListWidgetItem * item : selectedItems) {
        int row = ui->capitalList->row(item);
        QListWidgetItem* i = ui->capitalList->item(row);
        Capital * c2 = getCapitalFromList(i);
        if (c2->Id() == c->Id()) {
            i = ui->capitalList->takeItem(row);
            delete i;
        }
    }
}

void ClientMenu::loadList(QString str)
{
    JsonSerialization json;
    QList<Capital*> list = json.jsonToList(str);
    if (ui->capitalList->count() > 0) {
        for (int i = 0; i < ui->capitalList->count(); i++) {
            QListWidgetItem * item = ui->capitalList->takeItem(i);
            i--;
            delete item;
        }
    }

    for(int i = 0; i < list.size(); i++) {
        QListWidgetItem * item = new QListWidgetItem();
        QVariant variant;
        variant.setValue(list.at(i));
        item->setData(Qt::UserRole, variant);
        item->setText(list.at(i)->caption());
        ui->capitalList->addItem(item);
    }

}

void ClientMenu::connection() {
    QTcpSocket * socket = static_cast<QTcpSocket*>(sender());
    address = socket->peerAddress();
    port = socket->peerPort();
}

void ClientMenu::onConnected() {
    QTcpSocket * socket = static_cast<QTcpSocket*>(sender());

    connect(socket, SIGNAL(readyRead()),
            this, SLOT(onReadyRead()));
    connect(socket, SIGNAL(disconnected()),
            this, SLOT(onDisconnected()));

    QByteArray data = request.toUtf8();
    socket->write(data);
    socket->flush();
}

void ClientMenu::onReadyRead() {
        QTcpSocket * socket = static_cast<QTcpSocket*> (sender());
    QByteArray bytes = socket->readAll();
    QString responseStr = QString::fromStdString(bytes.toStdString());
    JsonSerialization json;
    int response = json.getResponseStatus(responseStr);
    switch (response) {
    case ADD_RESP:
        addToList(responseStr);
        break;
    case EDIT_RESP:
        editList(responseStr);
        break;
    case REMOVE_RESP:
        removeFromList(responseStr);
        break;
    case AVAIL_FILES_RESP:
        filesList(responseStr);
        break;
    case CREATE_RESP:
        capitalClear();
        break;
    case LISTLOAD_RESP:
        loadList(responseStr);
        break;
    case EMPTYLIST_RESP: {
        QMessageBox * msgBox = new QMessageBox();
        msgBox->setText("List from server is empty!");
        msgBox->show();
        break;
    }
    case LOAD_RESP: {
        loadList(responseStr);
        QMessageBox * msgBox = new QMessageBox();
        msgBox->setText("Successful load!");
        msgBox->show();
        break;
    }
    case LOAD_EMPTY_RESP: {
        QMessageBox * msgBox = new QMessageBox();
        msgBox->setText("File is empty!");
        msgBox->show();
        break;
    }
    case LOAD_ERROR_RESP: {
        QMessageBox * msgBox = new QMessageBox();
        msgBox->setText("Failed to open file");
        msgBox->show();
        break;
    }
    case SAVE_ERROR_RESP: {
        QMessageBox * msgBox = new QMessageBox();
        msgBox->setText("Failed to open file");
        msgBox->show();
        break;
    }
    case SAVE_RESP: {
        QMessageBox * msgBox = new QMessageBox();
        msgBox->setText("Successful save!");
        msgBox->show();
        break;
    }
    case SAVE_EMPTY_RESP: {
        QMessageBox * msgBox = new QMessageBox();
        msgBox->setText("List is empty! Cannot to save!");
        msgBox->show();
        break;
    }
    default: {
        QMessageBox * msgBox = new QMessageBox();
        msgBox->setText("Response is incorrect!");
        msgBox->show();
        break;
    }
    }
    socket->disconnectFromHost();
}

void ClientMenu::onDisconnected() {
    request = "";
}

void ClientMenu::on_filesButton_clicked()
{
    QTcpSocket * socket = new QTcpSocket();

    connect(socket, SIGNAL(connected()),
            this, SLOT(onConnected()));

    JsonSerialization json;
    request = json.generateFileRequest();
    socket->connectToHost(address, port);
    if (!socket->waitForConnected(2000)) {
        QMessageBox * msgBox = new QMessageBox();
        msgBox->setText("Connection failed!");
        msgBox->show();
        request = "";
    }
}

void ClientMenu::on_createButton_clicked()
{
    QTcpSocket * socket = new QTcpSocket();

    connect(socket, SIGNAL(connected()),
            this, SLOT(onConnected()));

    JsonSerialization json;
    request = json.generateCreateRequest();
    socket->connectToHost(address, port);
    if (!socket->waitForConnected(2000)) {
        QMessageBox * msgBox = new QMessageBox();
        msgBox->setText("Connection failed!");
        msgBox->show();
        request = "";
    }
}

void ClientMenu::on_addButton_clicked()
{
    DialogAdd * dialog = new DialogAdd();

    connect(dialog, SIGNAL(sendCapital(Capital*)),
            this, SLOT(getCapital(Capital*)));

    dialog->exec();
}

void ClientMenu::getCapital(Capital * c) {
    QTcpSocket * socket = new QTcpSocket();

    connect(socket, SIGNAL(connected()),
            this, SLOT(onConnected()));

    JsonSerialization json;
    request = json.generateAddRequest(c);
    socket->connectToHost(address, port);
    if (!socket->waitForConnected(2000)) {
        QMessageBox * msgBox = new QMessageBox();
        msgBox->setText("Connection failed!");
        msgBox->show();
        request = "";
    }
}

void ClientMenu::getEditCapital(Capital * c) {
    QTcpSocket * socket = new QTcpSocket();

    connect(socket, SIGNAL(connected()),
            this, SLOT(onConnected()));

    JsonSerialization json;
    request = json.generateEditRequest(c);
    socket->connectToHost(address, port);
    if(!socket->waitForConnected(2000)) {
        QMessageBox * msgBox = new QMessageBox();
        msgBox->setText("Connection failed!");
        msgBox->show();
        request = "";
    }
}

void ClientMenu::on_editButton_clicked()
{
    DialogEdit * dialog = new DialogEdit();

    connect(dialog, SIGNAL(sendCapital(Capital*)),
            this, SLOT(getEditCapital(Capital*)));
    connect(this, SIGNAL(sendCapital(Capital*)),
            dialog, SLOT(getCapital(Capital*)));

    QListWidgetItem * item = ui->capitalList->selectedItems().at(0);

    emit sendCapital(getCapitalFromList(item));

    dialog->exec();
}

static Capital * getCapitalFromList(QListWidgetItem * item) {
    QVariant variant = item->data(Qt::UserRole);
    Capital * c = variant.value<Capital*>();
    return c;
}

void ClientMenu::on_capitalList_itemSelectionChanged()
{
    QList<QListWidgetItem *> selectedItems = ui->capitalList->selectedItems();
    if (selectedItems.count() == 0) {
        ui->capitalEl->setText("");
        ui->populationEl->setText("");
        ui->squareEl->setText("");
        ui->longitudeEl->setText("");
        ui->latitudeEl->setText("");
        ui->removeButton->setEnabled(false);
        ui->editButton->setEnabled(false);
        ui->idEl->setText("");
    } else {
        QListWidgetItem * first = selectedItems.at(0);
        QVariant variant = first->data(Qt::UserRole);
        Capital * c = variant.value<Capital *>();
        ui->capitalEl->setText(c->Name());
        ui->populationEl->setText(QString::number(c->Population()));
        ui->squareEl->setText(QString::number(c->Square()));
        ui->longitudeEl->setText("λ -> " + QString::number(c->Longitude()) + "°");
        ui->latitudeEl->setText("φ -> " + QString::number(c->Latitude()) + "°");
        ui->idEl->setText(QString::number((int)c->Id()));
        ui->removeButton->setEnabled(true);
        ui->editButton->setEnabled(true);
    }
}

void ClientMenu::on_removeButton_clicked()
{
    QListWidgetItem * item = ui->capitalList->selectedItems().at(0);
    Capital * c = getCapitalFromList(item);

    QTcpSocket * socket = new QTcpSocket();

    connect(socket, SIGNAL(connected()),
            this, SLOT(onConnected()));

    JsonSerialization json;
    request = json.generateRemoveRequest(c);
    socket->connectToHost(address, port);
    if(!socket->waitForConnected(2000)) {
        QMessageBox * msgBox = new QMessageBox();
        msgBox->setText("Connection failed!");
        msgBox->show();
        request = "";
    }
}

void ClientMenu::on_actionLoad_list_from_server_triggered()
{
    QTcpSocket * socket = new QTcpSocket();

    connect(socket, SIGNAL(connected()),
            this, SLOT(onConnected()));

    JsonSerialization json;
    request = json.generateListLoadRequest();
    socket->connectToHost(address, port);
    if(!socket->waitForConnected(2000)) {
        QMessageBox * msgBox = new QMessageBox();
        msgBox->setText("Connection failed!");
        msgBox->show();
        request = "";
    }
}

void ClientMenu::on_fileList_itemSelectionChanged()
{
    QList<QListWidgetItem*> selected_items = ui->fileList->selectedItems();
    if (selected_items.count() == 0) {
        ui->savePath->setText("");
        ui->loadButon->setEnabled(false);
    } else {
        QListWidgetItem * i = selected_items.at(0);
        QString str = i->text();
        ui->savePath->setText(str);
        ui->loadButon->setEnabled(true);
    }
}

void ClientMenu::on_loadButon_clicked()
{
    if (ui->capitalList->isEnabled() == false) {
        QMessageBox * msgBox = new QMessageBox();
        msgBox->setText("List not create!");
        msgBox->show();
        return;
    }
    QList<QListWidgetItem*> selected_items = ui->fileList->selectedItems();
    QListWidgetItem * i = selected_items.at(0);
    QTcpSocket * socket = new QTcpSocket();

    connect(socket, SIGNAL(connected()),
            this, SLOT(onConnected()));

    JsonSerialization json;
    request = json.generateLoadRequest(i->text());
    socket->connectToHost(address, port);
    if(!socket->waitForConnected(2000)) {
        QMessageBox * msgBox = new QMessageBox();
        msgBox->setText("Connection failed!");
        msgBox->show();
        request = "";
    }
}

void ClientMenu::on_saveButton_clicked()
{
    if(ui->savePath->text() == "") {
        QMessageBox * msgBox = new QMessageBox();
        msgBox->setText("Filename slot is empty! Please, enter the filename!");
        msgBox->show();
    } else {
        QTcpSocket * socket = new QTcpSocket();

        connect(socket, SIGNAL(connected()),
                this, SLOT(onConnected()));

        JsonSerialization json;
        request = json.generateSaveRequest(ui->savePath->text());
        socket->connectToHost(address, port);
        if(!socket->waitForConnected(2000)) {
            QMessageBox * msgBox = new QMessageBox();
            msgBox->setText("Connection failed!");
            msgBox->show();
            request = "";
        }
    }
}
