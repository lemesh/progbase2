#include "dialogadd.h"
#include "ui_dialogadd.h"
#include <capital.h>

DialogAdd::DialogAdd(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogAdd)
{
    ui->setupUi(this);
}

DialogAdd::~DialogAdd()
{
    delete ui;
}



void DialogAdd::on_buttonBox_accepted()
{
    Capital * c = new Capital(ui->nameLine->text(),
                              ui->populationEl->value(),
                              ui->squareEl->value(),
                              ui->longitudeEl->value(),
                              ui->latitudeEl->value(),
                              nullptr);

    emit sendCapital(c);
}
