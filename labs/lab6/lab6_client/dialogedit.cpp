#include "dialogedit.h"
#include "ui_dialogedit.h"
#include <capital.h>

DialogEdit::DialogEdit(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogEdit)
{
    ui->setupUi(this);
}

DialogEdit::~DialogEdit()
{
    delete ui;
}

void DialogEdit::getCapital(Capital * c)
{
    ui->nameLine->setText(c->Name());
    ui->populationEl->setValue(c->Population());
    ui->squareEl->setValue(c->Square());
    ui->longitudeEl->setValue(c->Longitude());
    ui->latitudeEl->setValue(c->Latitude());
    ui->idEl->setValue(c->Id());
}

void DialogEdit::on_buttonBox_accepted()
{
    Capital * c = new Capital(ui->nameLine->text(),
                              ui->populationEl->value(),
                              ui->squareEl->value(),
                              ui->longitudeEl->value(),
                              ui->latitudeEl->value(),
                              nullptr);

    c->setId(ui->idEl->value());
    emit sendCapital(c);
}
