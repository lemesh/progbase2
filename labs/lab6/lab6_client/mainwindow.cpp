#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QTcpSocket>
#include <QHostAddress>
#include <clientmenu.h>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_connectButton_clicked()
{
    QTcpSocket * socket = new QTcpSocket();
    ClientMenu * client = new ClientMenu();

    connect(socket, SIGNAL(connected()),
            client, SLOT(connection()));

    QHostAddress address (ui->ipLine->text());
    socket->connectToHost(address, ui->portLine->text().toInt());
    if (!socket->waitForConnected(2000)) {
        QMessageBox * msgBox = new QMessageBox();
        msgBox->setText("Connection failed!");
        msgBox->show();
    } else {
        hide();
        client->show();
    }
}
