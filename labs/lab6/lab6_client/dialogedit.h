#ifndef DIALOGEDIT_H
#define DIALOGEDIT_H

#include <QDialog>
#include <capital.h>

namespace Ui {
class DialogEdit;
}

class DialogEdit : public QDialog
{
    Q_OBJECT

public:
    explicit DialogEdit(QWidget *parent = 0);
    ~DialogEdit();

private:
    Ui::DialogEdit *ui;
public slots:
    void getCapital(Capital *);
signals:
    void sendCapital(Capital *);
private slots:
    void on_buttonBox_accepted();
};

#endif // DIALOGEDIT_H
