#ifndef CLIENTMENU_H
#define CLIENTMENU_H

#include <QMainWindow>
#include <QHostAddress>
#include <capital.h>

namespace Ui {
class ClientMenu;
}

class ClientMenu : public QMainWindow
{
    Q_OBJECT

public:
    explicit ClientMenu(QWidget *parent = 0);
    ~ClientMenu();

private:
    Ui::ClientMenu *ui;
    QHostAddress address;
    quint16 port;
    QString request;
    void filesList(QString);
    void capitalClear();
    void addToList(QString);
    void editList(QString);
    void removeFromList(QString);
    void loadList(QString);

public slots:
    void connection();
    void onConnected();
    void onReadyRead();
    void onDisconnected();
    void getEditCapital(Capital *);
private slots:
    void on_filesButton_clicked();
    void on_createButton_clicked();
    void on_addButton_clicked();

    void getCapital(Capital *);
    void on_editButton_clicked();
    void on_capitalList_itemSelectionChanged();

    void on_removeButton_clicked();

    void on_actionLoad_list_from_server_triggered();

    void on_fileList_itemSelectionChanged();

    void on_loadButon_clicked();

    void on_saveButton_clicked();

signals:
    void sendCapital(Capital *);
};

#endif // CLIENTMENU_H
