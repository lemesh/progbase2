#include "capital.h"

Capital::Capital(QObject *parent) : QObject(parent) {
    name = "";
    population = 0;
    square = 0.0;
    longitude = 0.0;
    latitude = 0.0;
    id = 0;
}

Capital::Capital(QString name, int population, float square, float longitude, float latitude, QObject *parent) : QObject(parent) {
    this->name = name;
    this->population = population;
    this->square = square;
    this->longitude = longitude;
    this->latitude = latitude;
    this->id = 0;
}

QString Capital::caption() {
    return name + "(" + QString::number(id) + ")";
}

QString Capital::Name() {
    return name;
}

int Capital::Population() {
    return population;
}

float Capital::Square() {
    return square;
}

float Capital::Longitude() {
    return longitude;
}

float Capital::Latitude() {
    return latitude;
}

size_t Capital::Id() {
    return id;
}

void Capital::setName(QString name) {
    this->name = name;
}

void Capital::setPopulation(int populaion) {
    this->population = populaion;
}

void Capital::setSquare(float square) {
    this->square = square;
}

void Capital::setLongitude(float longitude) {
    this->longitude = longitude;
}

void Capital::setLatitude(float latitude) {
    this->latitude = latitude;
}

void Capital::setId(size_t id) {
    this->id = id;
}
