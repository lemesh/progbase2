#ifndef CAPITAL_H
#define CAPITAL_H

#include <QObject>
#include <lab6_lib_global.h>

class LAB6_LIBSHARED_EXPORT Capital : public QObject
{
    Q_OBJECT
    QString name;
    int population;
    float square;
    float longitude;
    float latitude;
    size_t id;
public:
    explicit Capital(QObject *parent = nullptr);
    explicit Capital(QString name,
                     int population,
                     float square,
                     float longitude,
                     float latitude,
                     QObject *parent = nullptr);

    QString Name();
    int Population();
    float Square();
    float Longitude();
    float Latitude();
    size_t Id();

    void setName(QString);
    void setPopulation(int);
    void setSquare(float);
    void setLongitude(float);
    void setLatitude(float);
    void setId(size_t);

    QString caption();

signals:

public slots:
};

#endif // CAPITAL_H
