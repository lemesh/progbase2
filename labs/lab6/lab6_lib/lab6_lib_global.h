#ifndef LAB6_LIB_GLOBAL_H
#define LAB6_LIB_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(LAB6_LIB_LIBRARY)
#  define LAB6_LIBSHARED_EXPORT Q_DECL_EXPORT
#else
#  define LAB6_LIBSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // LAB6_LIB_GLOBAL_H
