#include "jsonserialization.h"
#include <QDir>
#include <capital.h>

JsonSerialization::JsonSerialization()
{

}

void JsonSerialization::addRequest(QJsonDocument * doc, int requestStatus) {
    QJsonObject docObj;
    if (!doc->isEmpty()) {
        docObj = doc->object();
    }
    docObj.insert("request", requestStatus);
    doc->setObject(docObj);
}

void JsonSerialization::addResponse(QJsonDocument * doc, int responseStatus)
{
    QJsonObject docObj;
    if (!doc->isEmpty()) {
        docObj = doc->object();
    }
    docObj.insert("response", responseStatus);
    doc->setObject(docObj);
}

QString JsonSerialization::generateFileRequest() {
    QJsonDocument doc;
    addRequest(&doc, AVAIL_FILES);
    return QString::fromStdString(doc.toJson().toStdString());
}

QString JsonSerialization::generateFileResponse() {
    QDir dir ("../lab6_server/data");
    //dir.cd("data");
    dir.setFilter(QDir::Files | QDir::Hidden | QDir::NoSymLinks);
    QStringList filters;
    filters << "*.json";
    dir.setNameFilters(filters);
    QFileInfoList list = dir.entryInfoList();
    QStringList resList;
    for (int i = 0; i < list.size(); i++) {
        QString filename = list.at(i).fileName();
        resList.push_back(list.at(i).fileName());
    }

    QJsonDocument doc;
    addResponse(&doc, AVAIL_FILES_RESP);
    QJsonObject docObj = doc.object();
    QJsonArray fileArray;
    for(int i = 0; i < resList.size(); i++) {
        QJsonObject fileObj;
        fileObj.insert("fileName", resList.at(i));
        fileArray.append(fileObj);
    }
    docObj.insert("files", fileArray);
    doc.setObject(docObj);
    return QString::fromStdString(doc.toJson().toStdString());
}

QString JsonSerialization::generateCreateRequest()
{
    QJsonDocument doc;
    addRequest(&doc, CREATE);
    return QString::fromStdString(doc.toJson().toStdString());
}

QString JsonSerialization::generateCreateResponse()
{
    QJsonDocument doc;
    addResponse(&doc, CREATE_RESP);
    QJsonObject obj = doc.object();
    QJsonArray listArray;
    obj.insert("list", listArray);
    doc.setObject(obj);
    return QString::fromStdString(doc.toJson().toStdString());
}

QString JsonSerialization::capitalToJson(Capital * c)
{
    QJsonDocument doc;
    QJsonObject capitalObj;
    capitalObj.insert("name", c->Name());
    capitalObj.insert("population", c->Population());
    capitalObj.insert("square", c->Square());
    capitalObj.insert("longitude", c->Longitude());
    capitalObj.insert("latitude", c->Latitude());
    capitalObj.insert("id", (int)c->Id());
    doc.setObject(capitalObj);
    return QString::fromStdString(doc.toJson().toStdString());
}

QString JsonSerialization::capitalToJson(Capital * c, QJsonDocument * doc)
{
    QJsonObject obj = doc->object();
    QJsonObject capitalObj;
    capitalObj.insert("name", c->Name());
    capitalObj.insert("population", c->Population());
    capitalObj.insert("square", c->Square());
    capitalObj.insert("longitude", c->Longitude());
    capitalObj.insert("latitude", c->Latitude());
    capitalObj.insert("id", (int)c->Id());
    obj.insert("capital", capitalObj);
    doc->setObject(obj);
    return QString::fromStdString(doc->toJson().toStdString());
}

Capital *JsonSerialization::jsonToCapital(QString str)
{
    QJsonDocument doc = QJsonDocument::fromJson(QByteArray::fromStdString(str.toStdString()));
    QJsonObject capitalObj = doc.object().value("capital").toObject();
    Capital * c = new Capital(capitalObj.value("name").toString(),
                              capitalObj.value("population").toInt(),
                              capitalObj.value("square").toDouble(),
                              capitalObj.value("longitude").toDouble(),
                              capitalObj.value("latitude").toDouble());
    c->setId(capitalObj.value("id").toInt());
    return c;
}

QString JsonSerialization::generateEditRequest(Capital * c)
{
    QJsonDocument doc;
    addRequest(&doc, EDIT);
    return capitalToJson(c, &doc);
}

QString JsonSerialization::generateEditResponse(Capital * c) {
    QJsonDocument doc;
    addResponse(&doc, EDIT_RESP);
    return capitalToJson(c, &doc);
}

QString JsonSerialization::generateRemoveRequest(Capital * c)
{
    QJsonDocument doc;
    addRequest(&doc, REMOVE);
    return capitalToJson(c, &doc);
}

QString JsonSerialization::generateRemoveResponse(Capital * c)
{
    QJsonDocument doc;
    addResponse(&doc, REMOVE_RESP);
    return capitalToJson(c, &doc);
}

QString JsonSerialization::generateListLoadRequest()
{
    QJsonDocument doc;
    addRequest(&doc, LISTLOAD);
    return QString::fromStdString(doc.toJson().toStdString());
}

QString JsonSerialization::generateListLoadResponse(QList<Capital *> l)
{
    QJsonDocument doc;
    addResponse(&doc, LISTLOAD_RESP);
    return  listToJson(l, &doc);
}

QString JsonSerialization::generateAddRequest(Capital * c)
{
    QJsonDocument doc;
    addRequest(&doc, ADD);
    return capitalToJson(c, &doc);
}

QString JsonSerialization::generateAddResponse(Capital * c)
{
    QJsonDocument doc;
    addResponse(&doc, ADD_RESP);
    return capitalToJson(c, &doc);
}

QStringList JsonSerialization::getFileResponse(QString str)
{
    QJsonDocument doc= QJsonDocument::fromJson(QByteArray::fromStdString(str.toStdString()));
    QStringList list;
    if (doc.isEmpty()) { return list; }
    QJsonArray fileArray = doc.object().value("files").toArray();
    for (int i = 0; i < fileArray.size(); i++) {
        QJsonObject fileObj = fileArray.at(i).toObject();
        QString fileStr = fileObj.value("fileName").toString();
        list.push_back(fileStr);
    }
    return list;
}

int JsonSerialization::getRequestStatus(QString * request) {
    QJsonDocument doc = QJsonDocument::fromJson(QByteArray::fromStdString(request->toStdString()));
    int reqStatus = doc.object().value("request").toInt();
    return reqStatus;
}

int JsonSerialization::getResponseStatus(QString response) {
    QJsonDocument doc = QJsonDocument::fromJson(QByteArray::fromStdString(response.toStdString()));
    int responseStatus = doc.object().value("response").toInt();
    return responseStatus;
}

QString JsonSerialization::listToJson(QList<Capital *> l)
{
    QJsonDocument doc;
    QJsonArray listArray;
    for(int i = 0; i < l.size(); i++) {
        QJsonObject listObject;
        listObject.insert("name", l.at(i)->Name());
        listObject.insert("population", l.at(i)->Population());
        listObject.insert("square", l.at(i)->Square());
        listObject.insert("longitude", l.at(i)->Longitude());
        listObject.insert("latitude", l.at(i)->Latitude());
        listObject.insert("id", (int)l.at(i)->Id());
        listArray.append(listObject);
    }
    doc.setArray(listArray);
    return QString::fromStdString(doc.toJson().toStdString());
}

QString JsonSerialization::listToJson(QList<Capital *> l, QJsonDocument * doc)
{
    QJsonObject obj;
    if (!doc->isEmpty()) { obj = doc->object(); }
    QJsonArray listArray;
    for(int i = 0; i < l.size(); i++) {
        QJsonObject listObject;
        listObject.insert("name", l.at(i)->Name());
        listObject.insert("population", l.at(i)->Population());
        listObject.insert("square", l.at(i)->Square());
        listObject.insert("longitude", l.at(i)->Longitude());
        listObject.insert("latitude", l.at(i)->Latitude());
        listObject.insert("id", (int)l.at(i)->Id());
        listArray.append(listObject);
    }
    obj.insert("capitals", listArray);
    doc->setObject(obj);
    return QString::fromStdString(doc->toJson().toStdString());
}

QList<Capital*> JsonSerialization::jsonToList(QString str) {
    QJsonDocument doc = QJsonDocument::fromJson(QByteArray::fromStdString(str.toStdString()));
    QList<Capital *> list;
    if(!doc.isEmpty()) {
        QJsonArray listArray;
        if (!doc.object().isEmpty()) { listArray = doc.object().value("capitals").toArray(); }
        else { listArray = doc.array(); }
        for (int i = 0; i< listArray.size(); i++) {
            QJsonObject listObj = listArray.at(i).toObject();
            list.push_back(new Capital(listObj.value("name").toString(),
                                       listObj.value("population").toInt(),
                                       listObj.value("square").toDouble(),
                                       listObj.value("longitude").toDouble(),
                                       listObj.value("latitude").toDouble(),
                                       nullptr));
            list.back()->setId(listObj.value("id").toInt());
        }
    }
    return list;
}

QList<Capital *> JsonSerialization::fileJsonToList(QString str)
{
    QJsonDocument doc = QJsonDocument::fromJson(QByteArray::fromStdString(str.toStdString()));
    QList<Capital *> list;
    if(!doc.isEmpty()) {
        QJsonArray listArray = doc.array();
        for (int i = 0; i< listArray.size(); i++) {
            QJsonObject listObj = listArray.at(i).toObject();
            list.push_back(new Capital(listObj.value("name").toString(),
                                       listObj.value("population").toInt(),
                                       listObj.value("square").toDouble(),
                                       listObj.value("longitude").toDouble(),
                                       listObj.value("latitude").toDouble(),
                                       nullptr));
            list.back()->setId(listObj.value("id").toInt());
        }
    }
    return list;
}

QString JsonSerialization::generateEmptyListLoadResponse() {
    QJsonDocument doc;
    addResponse(&doc, EMPTYLIST_RESP);
    return QString::fromStdString(doc.toJson().toStdString());
}

QString JsonSerialization::generateLoadRequest(QString path) {
    QJsonDocument doc;
    addRequest(&doc, LOAD);
    QJsonObject obj = doc.object();
    obj.insert("filename", path);
    doc.setObject(obj);
    return QString::fromStdString(doc.toJson().toStdString());
}

QString JsonSerialization::generateLoadResponse(QList<Capital*> l) {
    QJsonDocument doc;
    addResponse(&doc, LOAD_RESP);
    return listToJson(l, &doc);
}

QString JsonSerialization::generateLoadErrorResponse() {
    QJsonDocument doc;
    addResponse(&doc, LOAD_ERROR_RESP);
    return QString::fromStdString(doc.toJson().toStdString());
}

QString JsonSerialization::generateLoadEmptyResponse() {
    QJsonDocument doc;
    addResponse(&doc, LOAD_EMPTY_RESP);
    return QString::fromStdString(doc.toJson().toStdString());
}

QString JsonSerialization::generateSaveResponse() {
    QJsonDocument doc;
    addResponse(&doc, SAVE_RESP);
    return QString::fromStdString(doc.toJson().toStdString());
}

QString JsonSerialization::generateSaveRequest(QString path) {
    QJsonDocument doc;
    addRequest(&doc, SAVE);
    QJsonObject obj = doc.object();
    obj.insert("filename", path);
    doc.setObject(obj);
    return QString::fromStdString(doc.toJson().toStdString());
}

QString JsonSerialization::generateSaveEmptyResponse() {
    QJsonDocument doc;
    addResponse(&doc, SAVE_EMPTY_RESP);
    return QString::fromStdString(doc.toJson().toStdString());
}

QString JsonSerialization::generateSaveErrorResponse() {
    QJsonDocument doc;
    addResponse(&doc, SAVE_ERROR_RESP);
    return QString::fromStdString(doc.toJson().toStdString());
}
