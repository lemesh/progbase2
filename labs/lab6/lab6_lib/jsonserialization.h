#ifndef JSONSERIALIZATION_H
#define JSONSERIALIZATION_H

#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <capital.h>
#include <lab6_lib_global.h>

enum {
    ADD,
    EDIT,
    REMOVE,
    AVAIL_FILES,
    ADD_RESP,
    EDIT_RESP,
    REMOVE_RESP,
    AVAIL_FILES_RESP,
    CREATE,
    CREATE_RESP,
    LISTLOAD,
    LISTLOAD_RESP,
    EMPTYLIST_RESP,
    LOAD,
    LOAD_RESP,
    LOAD_ERROR_RESP,
    LOAD_EMPTY_RESP,
    SAVE,
    SAVE_RESP,
    SAVE_ERROR_RESP,
    SAVE_EMPTY_RESP
};


class LAB6_LIBSHARED_EXPORT JsonSerialization
{
public:
    JsonSerialization();

    void addRequest(QJsonDocument *, int requestStatus);
    void addResponse(QJsonDocument *, int responseStatus);

    QString generateFileRequest();
    QString generateFileResponse();

    QString generateCreateRequest();
    QString generateCreateResponse();

    QString capitalToJson(Capital *);
    QString capitalToJson(Capital *, QJsonDocument *);
    Capital * jsonToCapital(QString);

    QString generateEditRequest(Capital *);
    QString generateEditResponse(Capital *);

    QString generateRemoveRequest(Capital *);
    QString generateRemoveResponse(Capital *);

    QString generateListLoadRequest();
    QString generateListLoadResponse(QList<Capital*>);
    QString generateEmptyListLoadResponse();

    QString generateAddRequest(Capital *);
    QString generateAddResponse(Capital *);

    QString generateLoadRequest(QString);
    QString generateLoadResponse(QList<Capital*>);
    QString generateLoadErrorResponse();
    QString generateLoadEmptyResponse();

    QString generateSaveRequest(QString);
    QString generateSaveResponse();
    QString generateSaveErrorResponse();
    QString generateSaveEmptyResponse();

    int getRequestStatus(QString *);
    int  getResponseStatus(QString);

    QString listToJson(QList<Capital*>);
    QString listToJson(QList<Capital*>, QJsonDocument *);
    QList<Capital*> jsonToList(QString);
    QList<Capital*> fileJsonToList(QString);

    QStringList getFileResponse(QString);
};

#endif // JSONSERIALIZATION_H
