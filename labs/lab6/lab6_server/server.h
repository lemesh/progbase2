#ifndef SERVER_H
#define SERVER_H

#include <QObject>
#include <QTcpServer>
#include <storage.h>

class Server : public QObject
{
    QTcpServer * server;
    QString request;
    QString response;
    Storage * storage;
    Q_OBJECT
public:
    explicit Server(QObject *parent = nullptr);

signals:

public slots:
    void onConnected();
    void onReadyRead();
    void onDisconnected();
};

#endif // SERVER_H
