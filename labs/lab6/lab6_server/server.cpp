#include "server.h"
#include <iostream>
#include <QTcpSocket>
#include <jsonserialization.h>
#include <storage.h>

Server::Server(QObject *parent) : QObject(parent)
{
    server = new QTcpServer(parent);
    request = "";
    storage = new Storage(parent);

    connect(server, SIGNAL(newConnection()),
            this, SLOT(onConnected()));

    const quint16 PORT = 3000;
    if (server->listen(QHostAddress::Any, PORT)) {
        std::cout << "Server connected on port " << PORT << std::endl;
    } else {
        std::cout << "Server could not start!" << std::endl;
    }
}

void Server::onConnected() {
    std::cout << "Client connected" << std::endl;
    QTcpSocket * socket = server->nextPendingConnection();

    connect(socket, SIGNAL(readyRead()),
            this, SLOT(onReadyRead()));
    connect(socket, SIGNAL(disconnected()),
            this, SLOT(onDisconnected()));

    JsonSerialization json;
    if(socket->waitForReadyRead(2000)) {
        QByteArray data = response.toUtf8();
        std::cout << "Sending: " << response.toStdString() << std::endl;
        socket->write(data);
        socket->flush();
    }
}

void Server::onReadyRead() {
    QTcpSocket * socket = static_cast<QTcpSocket*>(sender());
    QByteArray bytes = socket->readAll();
    request = QString::fromStdString(bytes.toStdString());
    JsonSerialization json;
    int reqStatus = json.getRequestStatus(&request);
    switch (reqStatus) {
    case ADD:
        storage->add(json.jsonToCapital(request));
        response = json.generateAddResponse(storage->getLast());
        break;
    case EDIT:
        storage->edit(json.jsonToCapital(request));
        response = json.generateEditResponse(json.jsonToCapital(request));
        break;
    case REMOVE:
        storage->remove(json.jsonToCapital(request));
        response = json.generateRemoveResponse(json.jsonToCapital(request));
        break;
    case AVAIL_FILES:
        response = json.generateFileResponse();
        break;
    case CREATE:
        storage->clean();
        response = json.generateCreateResponse();
        break;
    case LISTLOAD:
        if (storage->isEmpty()) { response = json.generateEmptyListLoadResponse(); }
        else { response = json.generateListLoadResponse(storage->getList()); }
        break;
    case LOAD: {
        int rs = storage->loadFromFile(request);
        if  (storage->isEmpty()) { response = json.generateLoadEmptyResponse(); }
        else if (rs == LOAD_ERROR_RESP) { response = json.generateLoadErrorResponse(); }
        else if (rs == LOAD_RESP) { response = json.generateLoadResponse(storage->getList()); }
        break;
    }
    case SAVE: {
        int rs = storage->saveToFile(request);
        if (storage->isEmpty()) { response = json.generateSaveEmptyResponse(); }
        else if (rs == SAVE_ERROR_RESP) { response = json.generateSaveErrorResponse(); }
        else if (rs == SAVE_RESP) { response = json.generateSaveResponse(); }
        break;
    }
    default:
        break;
    }
    std::cout << "Received: " << bytes.toStdString() << std::endl;
}

void Server::onDisconnected() {
    std::cout << "Client disconnected" << std::endl;
}
