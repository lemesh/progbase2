QT -= gui
QT += network

CONFIG += c++11 console
CONFIG -= app_bundle

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += main.cpp \
    server.cpp \
    storage.cpp

HEADERS += \
    server.h \
    storage.h

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../build-lab6_lib-Desktop_Qt_5_10_1_MinGW_32bit-Debug/release/ -llab6_lib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../build-lab6_lib-Desktop_Qt_5_10_1_MinGW_32bit-Debug/debug/ -llab6_lib
else:unix: LIBS += -L$$PWD/../build-lab6_lib-Desktop_Qt_5_10_1_MinGW_32bit-Debug/ -llab6_lib

INCLUDEPATH += $$PWD/../lab6_lib
DEPENDPATH += $$PWD/../lab6_lib
