#include "storage.h"
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonObject>
#include <fstream>
#include <jsonserialization.h>
\
Storage::Storage(QObject *parent) : QObject(parent)
{
}

void Storage::add(Capital * c) {
    size_t id = 0;
    if(!list.isEmpty()) { id = list.back()->Id(); }
    c->setId(++id);
    list.push_back(c);
}

void Storage::edit(Capital * c) {
    for (int i = 0; i < list.size(); i++) {
        Capital * copy = list.at(i);
        if (copy->Id() == c->Id()) {
            list.replace(i, c);
            return;
        }
    }
}

void Storage::remove(Capital * c) {
    for (int i = 0; i < list.size(); i++) {
        Capital * copy = list.at(i);
        if (copy->Id() == c->Id()) {
            list.removeAt(i);
            return;
        }
    }
}

void Storage::clean()
{
    if (!list.isEmpty())
        list.clear();
}

Capital *Storage::getLast()
{
    if(!list.isEmpty()) { return list.back(); }
    return NULL;
}

bool Storage::isEmpty() {
    return list.isEmpty();
}

QList<Capital*> Storage::getList() {
    return list;
}

int Storage::loadFromFile(QString str)
{
    QJsonDocument doc = QJsonDocument::fromJson(QByteArray::fromStdString(str.toStdString()));
    QJsonObject obj = doc.object();
    std::string filename = obj.value("filename").toString().toStdString();
    std::ifstream fin("../lab6_server/data/" + filename);
    if(!fin.is_open()) {
        return LOAD_ERROR_RESP;
    }

    std::string s {std::string(std::istreambuf_iterator<char>(fin), std::istreambuf_iterator<char>())};
    fileStorage = QString::fromStdString(s);

    JsonSerialization json;
    clean();
    list = json.jsonToList(fileStorage);
    return LOAD_RESP;
}

int Storage::saveToFile(QString str)
{
    if (isEmpty()) { return SAVE_EMPTY_RESP; }
    QJsonDocument doc = QJsonDocument::fromJson(QByteArray::fromStdString(str.toStdString()));
    QJsonObject obj = doc.object();
    std::string filepath = "../lab6_server/data/" + obj.value("filename").toString().toStdString();

    JsonSerialization json;
    QString strR = json.listToJson(list);

    std::ofstream F (filepath, std::ios::out);
    F << strR.toStdString();
    F.close();
    return SAVE_RESP;
}
