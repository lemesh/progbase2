#ifndef STORAGE_H
#define STORAGE_H

#include <QObject>
#include <capital.h>

class Storage : public QObject
{
    Q_OBJECT
    QList<Capital*> list;
    QString fileStorage;
public:
    explicit Storage(QObject *parent = nullptr);

    void add(Capital *);
    void edit(Capital *);
    void remove(Capital *);
    void clean();
    Capital * getLast();
    bool isEmpty();
    QList<Capital*> getList();
    int loadFromFile(QString);
    int saveToFile(QString);

signals:

public slots:
};

#endif // STORAGE_H
