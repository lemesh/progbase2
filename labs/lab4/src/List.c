#include <List.h>
#include <Memory.h>

const int LIST_CAPACITY = 15;

struct __List {
    void ** items;
    int capacity;
    int length;
};

List * List_new(Memory * memory) {
    List * self = malloc(sizeof(List));
    Memory_add(memory, self);
    self->capacity = LIST_CAPACITY;
    self->length = 0;
    self->items = calloc(LIST_CAPACITY, sizeof(void *));
    Memory_add(memory, self->items);
    return self;
}

void * List_set(List * self, size_t index, void * value) {
    void * oldValue = self->items[index];
    self->items[index] = value;
    return oldValue;
}

List * List_newCapacity(size_t initialCapacity) {
    List *self = malloc(sizeof(List));
    self->capacity = (int)initialCapacity;
    self->length = 0;
    self->items = calloc((size_t)self->capacity, sizeof(void *));
    return self;
}

void List_free(List * self) {
    if (self != NULL) {
        free(self->items);
        free(self);
    }
}

void List_insert(List * self, void * value, size_t index) {
    if (self != NULL && index < self->length) {
        if (self->length == self->capacity) {
            self->capacity++;
            self->items = realloc(self->items, sizeof(void *) * self->capacity);
        }
        if (self->length == 0) {
            List_add(self, value);
            return;
        }
        for (int i = self->length - 1; i >= index && i > -1; i--) {
            self->items[i + 1] = self->items[i];
        }
        self->length++;
        self->items[index] = value;
    }
}

void List_add(List * self, void * value) {
    if (self != NULL) {
        if (self->length == self->capacity) {
            self->capacity++;
            self->items = realloc(self->items, sizeof(void *) * self->capacity);
        }
        self->items[self->length] = value;
        self->length++;
    }
}

void * List_at(List * self, size_t index) {
    if (self != NULL && index < self->length) {
        return self->items[index];
    }
    return NULL;
}

void * List_removeAt(List * self, size_t index) {
    if (self != NULL && index < self->length) {
        void *value = self->items[index];
        for (int i = (int)index; i < self->length; i++) {
            self->items[i] = self->items[i + 1];
        }
        self->length--;
        return value;
    }
    return NULL;
}

size_t List_count(List * self) {
    if (self != NULL) {
        return self->length;
    }
    return 0;
}