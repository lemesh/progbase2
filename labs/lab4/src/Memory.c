#include <Memory.h>
#include <stdlib.h>

#define CAPACITY 20;

struct __Memory {
    void ** address;
    size_t length;
    size_t capacity;
};

Memory * Memory_new(void) {
    Memory * self = malloc(sizeof(Memory));
    self->length = 0;
    self->capacity = CAPACITY;
    self->address = malloc(sizeof(void *) * self->capacity);
    return self;
}

void Memory_free(Memory * self) {
    for (int i = 0; i < self->length; i++) {
        free(self->address[i]);
    }
    free(self->address);
    free(self);
}

void Memory_add(Memory * self, void * address) {
    if (self->capacity == self->length) {
        self->capacity += CAPACITY;
        self->address = realloc(self->address, sizeof(void * ) * self->capacity);
    }
    self->length++;
    self->address[self->length - 1] = address;
}