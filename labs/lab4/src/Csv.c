#include <Csv.h>
#include <stdbool.h>
#include <string.h>
#include <StringBuffer.h>

struct __CsvTable {
    List * rows;
};

struct __CsvRow {
    List * value;
};

CsvRow * CsvRow_new(Memory * memory) {
    CsvRow * self = malloc(sizeof(CsvRow));
    Memory_add(memory, self);
    self->value = List_new(memory);
    return self;
}

void CsvRow_add(CsvRow * self, const char * value) {
    List_add(self->value, (void *)value);
}

void CsvRow_values(CsvRow * self, List * values) {
    for (int i = 0; i < List_count(self->value); i++) {
        List_add(values, List_at(self->value, (size_t)i));
    }
}

CsvTable * CsvTable_new(Memory * memory) {
    CsvTable * self = malloc(sizeof(CsvTable));
    Memory_add(memory, self);
    self->rows = List_new(memory);
    return self;
}

void CsvTable_add(CsvTable * self, CsvRow * row) {
    List_add(self->rows, row);
}

void CsvTable_rows(CsvTable * self, List * rows) {
    for (int i = 0; i < List_count(self->rows); i++) {
        List_add(rows, List_at(self->rows, (size_t)i));
    }
}

CsvTable * CsvTable_newFromString(const char * csvString, Memory * memory) {
    CsvTable * table = CsvTable_new(memory);
    CsvRow * row = CsvRow_new(memory);
    int buffCapacity = 2048;
    char * buff = malloc(sizeof(char) * buffCapacity);
    Memory_add(memory, buff);
    memset(buff, '\0', (size_t)buffCapacity);
    int length = 0;
    while (true) {
        char ch = *csvString;
        if (ch == '\0') {
            CsvRow_add(row, buff);
            CsvTable_add(table, row);
            break;
        } else if (ch == '\n') {
            CsvRow_add(row, buff);
            CsvTable_add(table, row);
            buffCapacity = 2048;
            buff = malloc(sizeof(char) * buffCapacity);
            Memory_add(memory, buff);
            row = CsvRow_new(memory);
            length = 0;
        } else if (ch == ',') {
            CsvRow_add(row, buff);
            buffCapacity = 2048;
            buff = malloc(sizeof(char) * buffCapacity);
            Memory_add(memory, buff);
            length = 0;
        } else {
            if(length == buffCapacity){
                buffCapacity *= 2;
                buff = realloc(buff, (size_t)buffCapacity);
            }
            buff[length] = ch;
            length++;
            buff[length] = '\0';
        }
        csvString++;
    }
    return table;
}

char * CsvTable_toNewString(CsvTable * self, Memory * memory){
    StringBuffer * string = StringBuffer_new(memory);
    bool firstRow = true;
    for (int i = 0; i < List_count(self->rows); i++) {
        CsvRow * row = List_at(self->rows, (size_t)i);
        if (firstRow) firstRow = false;
        else StringBuffer_appendChar(string, '\n');

        bool firstValue = true;
        for (int j = 0; j < List_count(row->value); j++) {
            char * value = List_at(row->value, (size_t)j);
            if(firstValue) firstValue = false;
            else StringBuffer_appendChar(string, ',');

            StringBuffer_append(string, value);
        }
    }

    char * copy = StringBuffer_toNewString(string, memory);
    return copy;
}