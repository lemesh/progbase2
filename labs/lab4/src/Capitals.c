#include <Capitals.h>
#include <Cui.h>
#include <progbase/console.h>
#include <StringBuffer.h>
#include <Csv.h>
#include <string.h>
#include <File.h>

struct __Capitals {
    char * name;
    size_t population;
    float square;
    float latitude;
    float longtitude;
    bool highlight;
};

Capitals * Capitals_new(Memory * memory) {
    Capitals * self = malloc(sizeof(Capitals));
    Memory_add(memory, self);
    self->name = "";
    self->population = 0;
    self->square = 0.0;
    self->latitude = 0.0;
    self->longtitude = 0.0;
    self->highlight = false;
}

void createCapitals() {
    Memory * memory = Memory_new();
    List * self = List_new(memory);
    List_add(self, Capitals_new(memory));
    printEditMenu(self, memory);
    Memory_free(memory);
}

void printCapitals(List * self) {
    int k = 1;
    for (int i = 0; i < List_count(self); i++) {
        Capitals * capital = List_at(self, (size_t)i);
        Console_setCursorPosition((unsigned short)k, 60);
        printf("-----------------------------------------------");
        k++;
        if (capital->highlight == true) Console_setCursorAttributes(FG_RED);
        Console_setCursorPosition((unsigned short)k, 60);
        printf("%i. Capital name = %s", i, capital->name);
        k++;
        Console_setCursorPosition((unsigned short)k, 60);
        printf("Population = %li Square = %.2f",
               capital->population, capital->square);
        k++;
        Console_setCursorPosition((unsigned short)k, 60);
        printf("Coordinates:");
        k++;
        Console_setCursorPosition((unsigned short)k, 60);
        printf("%.2f°\t%.2f°", capital->longtitude,
               capital->latitude);
        Console_setCursorAttributes(FG_DEFAULT);
        k++;
        Console_setCursorPosition((unsigned short)k, 60);
        printf("-----------------------------------------------");
    }
}

void Capitals_add(List * self, Memory * memory) {
    Capitals_unHighlight(self);
    Capitals * capital = List_at(self, 0);
    bool new = true;
    if (List_count(self) == 1 && !strcmp(capital->name, "")) {
        capital = List_at(self, 0);
        new = false;
    } else {
        capital = Capitals_new(memory);
    }
    Console_clear();
    printf("Enter the name -> ");
    capital->name = StringBuffer_enter(memory);
    Console_clear();
    printf("Enter the population -> ");
    population(&(capital->population));
    Console_clear();
    printf("Enter the square -> ");
    getFloat(&(capital->square));
    Console_clear();
    printf("Enter the longtitude -> ");
    getFloat(&(capital->longtitude));
    Console_clear();
    printf("Enter the latitude -> ");
    getFloat(&(capital->latitude));
    bool save = saveChanges();
    if(save == true && new == true) {
        List_add(self, capital);
    } else if (save == false && new == false) {
        capital->name = "";
        capital->population = 0;
        capital->square = 0.0;
        capital->latitude = 0.0;
        capital->longtitude = 0.0;
    }
}

void Capitals_editOne(List * self, ItemType type, size_t index, Memory * memory) {
    Capitals_unHighlight(self);
    Capitals * capital = List_at(self, index);
    Console_clear();
    printCapital(capital, 1);
    Console_setCursorPosition(0, 0);
    switch (type) {
        case NAME: {
            char * oldName = capital->name;
            printf("Enter the name -> ");
            capital->name = StringBuffer_enter(memory);
            if(!saveChanges()) {
                capital->name = oldName;
            }
            break;
        }
        case POPULATION: {
            size_t oldPop = capital->population;
            printf("Enter the population -> ");
            population(&(capital->population));
            if(!saveChanges()) {
                capital->population = oldPop;
            }
            break;
        }
        case SQUARE: {
            float oldSquare = capital->square;
            printf("Enter the square -> ");
            getFloat(&(capital->square));
            if(!saveChanges()) {
                capital->square = oldSquare;
            }
            break;
        }
        case LATITUDE: {
            float oldLatitude = capital->latitude;
            printf("Enter the latitude -> ");
            getFloat(&(capital->latitude));
            if(!saveChanges()) {
                capital->latitude = oldLatitude;
            }
            break;
        }
        case LONGTITUDE: {
            float oldLong = capital->longtitude;
            printf("Enter the longtitude -> ");
            getFloat(&(capital->longtitude));
            if(!saveChanges()) {
                capital->longtitude = oldLong;
            }
            break;
        }
        default:
            break;
    }
}

void printCapital(Capitals * self, int k) {
    Console_setCursorPosition((unsigned short)k, 60);
    printf("-----------------------------------------------");
    k++;
    Console_setCursorPosition((unsigned short)k, 60);
    printf("Capital name = %s", self->name);
    k++;
    Console_setCursorPosition((unsigned short)k, 60);
    printf("Population = %li Square = %.2f",
           self->population, self->square);
    k++;
    Console_setCursorPosition((unsigned short)k, 60);
    printf("Coordinates:");
    k++;
    Console_setCursorPosition((unsigned short)k, 60);
    printf("%.2f°\t%.2f°", self->longtitude,
           self->latitude);
    k++;
    Console_setCursorPosition((unsigned short)k, 60);
    printf("-----------------------------------------------");
}

void searchCapital(List * self, float longtitude) {
    for (int i = 0; i < List_count(self); i++) {
        Capitals * capital = List_at(self, (size_t)i);
        if (capital->longtitude > longtitude) {
            capital->highlight = true;
        }
    }
}

void Capitals_unHighlight(List * self) {
    for (int i = 0; i < List_count(self); i++) {
        Capitals * capital = List_at(self, (size_t)i);
        if (capital->highlight == true) {
            capital->highlight = false;
        }
    }
}

void Capitals_readFromFile() {
    char * fileName;
    Memory * memory = Memory_new();
    Console_clear();
    printf("Enter the name of input file -> ");
    fileName = StringBuffer_enter(memory);
    printf("(%s)\n", fileName);
    getchar();
    List * list = List_new(memory);
    Capitals_makeListFromCsv(fileName, memory, list);
    Memory_free(memory);
}

void Capitals_makeListFromCsv(const char * fileName, Memory * memory, List * self) {
    if (!fileExists(fileName)) return;
    long length = getFileSize(fileName);
    char buff[length];
    memset(buff, '\0', (size_t)length);
    readFileToBuffer(fileName, buff, (int)length);
    CsvTable * table = CsvTable_newFromString(buff, memory);
    List * rows = List_new(memory);
    List_add(self, Capitals_new(memory));
    CsvTable_rows(table, rows);
    for (int i = 0; i < List_count(rows); i++) {
        List * values = List_new(memory);
        CsvRow_values(List_at(rows, (size_t)i), values);
        for (int j = 0; j < List_count(values); j++) {
            char * value = List_at(values, (size_t)j);
            Capitals_editAllFromFile(self, (size_t)i, value, (ItemType)j);
        }
        List_add(self, Capitals_new(memory));
    }
    (void)List_removeAt(self, List_count(self) - 1);
    printEditMenu(self, memory);
}

void Capitals_editAllFromFile(List * self, size_t index, char * value, ItemType type) {
    Capitals_unHighlight(self);
    Capitals * capital = List_at(self, index);
    switch (type) {
        case NAME: {
            capital->name = value;
            break;
        }
        case POPULATION: {
            capital->population = (size_t)atol(value);
            break;
        }
        case SQUARE: {
            capital->square = (float)atof(value);
            break;
        }
        case LATITUDE: {
            capital->latitude = (float)atof(value);
            break;
        }
        case LONGTITUDE: {
            capital->longtitude = (float)atof(value);
            break;
        }
        default:
            break;
    }
}

void Capitals_editCapital(List * self, size_t index, Memory * memory) {
    Capitals_unHighlight(self);
    Capitals * capital = List_at(self, index);
    Console_clear();
    printCapital(capital, 1);
    Console_setCursorPosition(0, 0);
    char * oldName = capital->name;
    printf("Enter the name -> ");
    capital->name = StringBuffer_enter(memory);
    size_t oldPop = capital->population;
    Console_clear();
    printCapital(capital, 1);
    Console_setCursorPosition(0, 0);
    printf("Enter the population -> ");
    population(&(capital->population));
    float oldSquare = capital->square;
    Console_clear();
    printCapital(capital, 1);
    printf("Enter the square -> ");
    getFloat(&(capital->square));
    float oldLatitude = capital->latitude;
    Console_clear();
    printCapital(capital, 1);
    Console_setCursorPosition(0, 0);
    printf("Enter the latitude -> ");
    getFloat(&(capital->latitude));
    float oldLong = capital->longtitude;
    Console_clear();
    printCapital(capital, 1);
    Console_setCursorPosition(0, 0);
    printf("Enter the longtitude -> ");
    getFloat(&(capital->longtitude));
    if (!saveChanges()) {
        capital->name = oldName;
        capital->population = oldPop;
        capital->square = oldSquare;
        capital->longtitude = oldLong;
        capital->latitude = oldLatitude;
    }
}

void Capitals_writeToFile(List * self, Memory * memory) {
    Capitals_unHighlight(self);
    char * outputFile;
    Console_clear();
    printf("Enter the name of output file -> ");
    outputFile = StringBuffer_enter(memory);
    outputFile[strlen(outputFile) - 1] = '\0';
    CsvTable * rows = CsvTable_new(memory);
    for (int i = 0; i < List_count(self); i++) {
        CsvRow * values = CsvRow_new(memory);
        Capitals * capital = List_at(self, (size_t)i);
        CsvRow_add(values, capital->name);
        char * value = malloc(sizeof(char) * 100);
        Memory_add(memory, value);
        memset(value, '\0', 100);
        sprintf(value, "%li", capital->population);
        CsvRow_add(values, value);
        memset(value, '\0', 100);
        sprintf(value, "%f", capital->square);
        CsvRow_add(values, value);
        memset(value, '\0', 100);
        sprintf(value, "%f", capital->latitude);
        CsvRow_add(values, value);
        memset(value, '\0', 100);
        sprintf(value, "%f", capital->longtitude);
        CsvRow_add(values, value);
        CsvTable_add(rows, values);
    }
    char * buff = CsvTable_toNewString(rows, memory);
    FILE * f = fopen(outputFile, "wb");
    fwrite(buff, 1, strlen(buff), f);
    fclose(f);
}

void Capitals_remove(List * self, size_t index) {
    List_removeAt(self, index);
}