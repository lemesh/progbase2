#include <Cui.h>
#include <stdio.h>
#include <progbase/console.h>
#include <Capitals.h>
#include <ctype.h>
#include <string.h>

void printMainMenu() {
    Console_hideCursor();
    int menu = 1;
    int key = 0;
    while (true) {
        Console_clear();
        Console_setCursorPosition(0,0);
        if (menu == 1) printFormatMenuPoint("Create new empty list!");
        else printf("Create new empty list!\n");
        if (menu == 2) printFormatMenuPoint("Create list from file");
        else printf("Create list from file\n");
        printf("\nBackspace -> quit\n");
        key = Console_getChar();
        if (key == 65) menu--;
        if (key == 66) menu++;
        if (key == 127) return;
        if (key == 10 && menu == 1) createCapitals();
        if (key == 10 && menu == 2) Capitals_readFromFile();
        if (menu > 2) menu = 2;
        if (menu < 1) menu = 1;
    }
}

void printFormatMenuPoint(char *str) {
    printf("   ");
    Console_setCursorAttribute(BG_INTENSITY_BLACK);
    puts(str);
    Console_setCursorAttribute(BG_DEFAULT);
}

void printEditMenu(List * self, Memory * memory) {
    int menu = 1;
    int key = 0;
    while (true) {
        Console_clear();
        printCapitals(self);
        Console_setCursorPosition(0,0);
        if (menu == 1) printFormatMenuPoint("Add new capital");
        else printf("Add new capital\n");
        if (menu == 2) printFormatMenuPoint("Edit capital on some position of list");
        else printf("Edit capital on some position of list\n");
        if (menu == 3) printFormatMenuPoint("Search all capitals with greater than input longitude");
        else printf("Search all capitals with greater than input longitude\n");
        if (menu == 4) printFormatMenuPoint("Save array of struct to file");
        else printf("Save array of struct to file\n");
        if (menu == 5) printFormatMenuPoint("Back to main menu");
        else printf("Back to main menu\n");
        key = Console_getChar();
        if (key == 65) menu--;
        else if (key == 66) menu++;
        if (key == 10 && menu == 5) return;
        else if (key == 10 && menu == 1) Capitals_add(self, memory);
        else if (key == 10 && menu == 2) printEditCapital(self, memory);
        else if (key == 10 && menu == 3) printSearch(self);
        else if (key == 10 && menu == 4) Capitals_writeToFile(self, memory);
        if (menu > 5) menu = 5;
        else if (menu < 1) menu = 1;
    }
}

void printEditCapital(List * self, Memory * memory) {
    int menu = 1;
    int key = 0;
    size_t index = 0;
    bool truth = false;
    do {
        Console_clear();
        printCapitals(self);
        Console_setCursorPosition(0, 0);
        printf("Enter the index of capital to edit -> ");
        population(&index);
        if (index < List_count(self) && index >= 0) {
            truth = true;
        }
    } while (truth == false);
    while (true) {
        Console_clear();
        printCapitals(self);
        Console_setCursorPosition(0,0);
        if (menu == 1) printFormatMenuPoint("Edit all");
        else printf("Edit all\n");
        if (menu == 2) printFormatMenuPoint("Edit item");
        else printf("Edit item\n");
        if (menu == 3) printFormatMenuPoint("Remove capital");
        else printf("Remove capital\n");
        if (menu == 4) printFormatMenuPoint("Back to menu");
        else printf("Back to menu\n");
        key = Console_getChar();
        if (key == 65) menu--;
        else if (key == 66) menu++;
        if (key == 10 && menu == 5) return;
        else if (key == 10 && menu == 1) Capitals_editCapital(self, index, memory);
        else if (key == 10 && menu == 2) printEditOne(self, memory, index);
        else if (key == 10 && menu == 3) {
            Capitals_remove(self, index);
            return;
        }
        else if (key == 10 && menu == 4) return;
        if (menu > 4) menu = 4;
        else if (menu < 1) menu = 1;
    }
}

void population(size_t * population) {
    char line[10];
    do {
        fgets(line, 10, stdin);
        size_t last = strlen(line) - 1;
        if (line[last] == '\n') line[last] = '\0';
        if (fgetsNumCheck(line) == false) *population = 123456789;
        else sscanf(line, "%li", population);
        Console_clear();
    } while (*population == 123456789);
}

bool fgetsNumCheck(const char * buffer) {
    int i = 0;
    if (buffer[0] == '-') i = 1;
    for (; buffer[i] != '\0'; i++){
        if ((buffer[i] == '.' || buffer[i] == ',') && !isdigit(buffer[i + 1]))
            return false;
        else if(!isdigit(buffer[i]) && buffer[i] != '.' && buffer[i] != ',')
            return false;
    }
    return true;
}

void getFloat(float * smt) {
    char line[10];
    do {
        fgets(line, 10, stdin);
        size_t last = strlen(line) - 1;
        if (line[last] == '\n') line[last] = '\0';
        if (fgetsNumCheck(line) == false) *smt = 123456789;
        else sscanf(line, "%f", smt);
        Console_clear();
    } while (*smt == 123456789);
}

bool saveChanges() {
    Console_clear();
    printf("Save changes? Y or N -> ");
    char answer[3];
    fgets(answer, 3, stdin);
    if (answer[0] == 'Y' || answer[0] == 'y') {
        return true;
    } else {
        return false;
    }
}

void printSearch(List * self) {
    float longtitude = 0.0;
    Console_clear();
    printCapitals(self);
    Console_setCursorPosition(0, 0);
    printf("Enter the longtitude -> ");
    getFloat(&longtitude);
    searchCapital(self, longtitude);
}

void printEditOne(List * self, Memory * memory, size_t index) {
    int menu = 1;
    int key = 0;
    while (true) {
        Console_clear();
        printCapital(List_at(self, index), 1);
        Console_setCursorPosition(0, 0);
        if (menu == 1) printFormatMenuPoint("Edit name");
        else printf("Edit name\n");
        if (menu == 2) printFormatMenuPoint("Edit population");
        else printf("Edit population\n");
        if (menu == 3) printFormatMenuPoint("Edit square");
        else printf("Edit square\n");
        if (menu == 4) printFormatMenuPoint("Edit longtitude");
        else printf("Edit latitude\n");
        if (menu == 5) printFormatMenuPoint("Edit latitude");
        else printf("Edit longtitude\n");
        if (menu == 6) printFormatMenuPoint("Back to menu");
        else printf("Back to menu\n");
        key = Console_getChar();
        if (key == 65) menu--;
        else if (key == 66) menu++;
        if (key == 10 && menu == 6) return;
        else if (key == 10 && menu == 1) Capitals_editOne(self, NAME, index, memory);
        else if (key == 10 && menu == 2) Capitals_editOne(self, POPULATION, index, memory);
        else if (key == 10 && menu == 3) Capitals_editOne(self, SQUARE, index, memory);
        else if (key == 10 && menu == 4) Capitals_editOne(self, LONGTITUDE, index, memory);
        else if (key == 10 && menu == 5) Capitals_editOne(self, LATITUDE, index, memory);
        if (menu > 6) menu = 5;
        else if (menu < 1) menu = 1;
    }
}