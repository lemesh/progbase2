#pragma once

#include <Memory.h>
#include <List.h>

typedef enum {
    NAME,
    POPULATION,
    SQUARE,
    LATITUDE,
    LONGTITUDE
} ItemType;

typedef struct __Capitals Capitals;

Capitals * Capitals_new(Memory * memory);
void createCapitals();
void printCapitals(List * self);
void Capitals_add(List * self, Memory * memory);
void Capitals_editOne(List * self, ItemType item, size_t index, Memory * memory);
void printCapital(Capitals * self, int k);
void searchCapital(List * self, float longtitude);
void Capitals_unHighlight(List * self);
void Capitals_remove(List * self, size_t index);
void Capitals_editCapital(List * self, size_t index, Memory * memory);

void Capitals_readFromFile();
void Capitals_makeListFromCsv(const char * fileName, Memory * memory, List * self);
void Capitals_editAllFromFile(List * self, size_t index, char * value, ItemType type);
void Capitals_writeToFile(List * self, Memory * memory);