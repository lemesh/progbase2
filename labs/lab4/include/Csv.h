#pragma once

#include <List.h>
#include <Memory.h>

typedef struct __CsvTable CsvTable;
typedef struct __CsvRow   CsvRow;

CsvRow * CsvRow_new(Memory * memory);
void CsvRow_add(CsvRow * self, const char * value);
void CsvRow_values(CsvRow * self, List * values);

CsvTable * CsvTable_new(Memory * memory);
void CsvTable_free(CsvTable * self);
void CsvTable_add (CsvTable * self, CsvRow * row);
void CsvTable_rows(CsvTable * self, List * rows);

CsvTable * CsvTable_newFromString(const char * csvString, Memory * memory);
char * CsvTable_toNewString  (CsvTable * self, Memory * memory);