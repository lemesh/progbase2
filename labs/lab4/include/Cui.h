#pragma once

#include <stdbool.h>
#include <List.h>

void printMainMenu();
void printFormatMenuPoint(char * str);
void printEditMenu(List * self, Memory * memory);
void printEditCapital(List * self, Memory * memory);
void printSearch(List * self);
void printEditOne(List * self, Memory * , size_t index);

void population(size_t * population);
void getFloat(float * smt);
bool fgetsNumCheck(const char * buffer);
bool saveChanges();