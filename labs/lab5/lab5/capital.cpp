#include "capital.h"

Capital::Capital(QObject *parent) : QObject(parent) {
    name = "Name";
    populaion = 0;
    square = 0.0;
    longitude = 0.0;
    latitude = 0.0;
}

Capital::Capital(QString name, int population, float square, float longitude, float latitude, QObject *parent) : QObject(parent) {
    this->name = name;
    this->populaion = population;
    this->square = square;
    this->longitude = longitude;
    this->latitude = latitude;
}

QString Capital::caption() {
    return name;
}

QString Capital::getName() {
    return name;
}

int Capital::getPopulation() {
    return populaion;
}

float Capital::getSquare() {
    return square;
}

float Capital::getLongitude() {
    return longitude;
}

float Capital::getLatitude() {
    return latitude;
}

void Capital::setName(QString name) {
    this->name = name;
}

void Capital::setPopulation(int populaion) {
    this->populaion = populaion;
}

void Capital::setSquare(float square) {
    this->square = square;
}

void Capital::setLongitude(float longitude) {
    this->longitude = longitude;
}

void Capital::setLatitude(float latitude) {
    this->latitude = latitude;
}
