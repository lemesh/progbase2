#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "capital.h"
#include <vector>
#include "adddialog.h"
#include "editdialog.h"
#include <string>
#include <QFileDialog>
#include <iostream>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <vector>
#include <fstream>
#include <QXmlStreamReader>
#include <QNetworkReply>
#include <QUrlQuery>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    apiUrl = "https://en.wikipedia.org/w/api.php";
    manager = new QNetworkAccessManager(this);
    connect(manager, SIGNAL( finished( QNetworkReply* ) ), SLOT( onFinished( QNetworkReply* ) ) );
}

MainWindow::~MainWindow()
{
    delete ui;
}

std::string writeFileToString(std::string fileName) {
    std::ifstream fin(fileName);
    if(!fin.is_open()) {
        std::cout << "Not exist" << std::endl;
    }
    std::string s {std::string(std::istreambuf_iterator<char>(fin), std::istreambuf_iterator<char>())};
    return s;
}

void MainWindow::on_addButton_clicked()
{
    addDialog dialog;
    connect(&dialog, SIGNAL(sendCapital(Capital*)), this, SLOT(getCapital(Capital*)));
    dialog.exec();
}

void MainWindow::on_editButton_clicked()
{
    editDialog dialog;
    connect(this, SIGNAL(sendCapital(Capital*)), &dialog, SLOT(getCapital(Capital*)));
    QList<QListWidgetItem *> selectedItems = ui->listWidget_2->selectedItems();
    QListWidgetItem * first = selectedItems.at(0);
    QVariant variant = first->data(Qt::UserRole);
    Capital * c = variant.value<Capital *>();
    emit sendCapital(c);
    dialog.exec();
    first->setText(c->caption());
    ui->capitalEl->setText(c->getName());
    ui->populationEl->setText(QString::number(c->getPopulation()));
    ui->squareEl->setText(QString::number(c->getSquare()));
    ui->longitudeEl->setText("λ -> " + QString::number(c->getLongitude()) + "°");
    ui->latitudeEl->setText("φ -> " + QString::number(c->getLatitude()) + "°");
}

void MainWindow::on_executeButton_clicked()
{
    if (ui->executeView->count() > 0) {
        for (int i = 0; i < ui->executeView->count(); i++) {
            ui->executeView->clear();
        }
    }
    float executeValue = ui->executeValue->value();
    for (int i = 0; i < ui->listWidget_2->count(); i++) {
        QListWidgetItem * item = ui->listWidget_2->item(i);
        QVariant variant = item->data(Qt::UserRole);
        Capital * c = variant.value<Capital *>();
        if (c->getLongitude() >= executeValue) {
            QListWidgetItem * item2 = new QListWidgetItem();
            item2->setText(c->caption());
            QVariant variant;
            variant.setValue(c);
            item2->setData(Qt::UserRole, variant);
            ui->executeView->addItem(item2);
        }
    }
}

void MainWindow::getCapital(Capital * c) {
    QListWidgetItem * i = new QListWidgetItem();
    i->setText(c->caption());
    QVariant variant;
    variant.setValue(c);
    i->setData(Qt::UserRole, variant);
    ui->listWidget_2->addItem(i);
}

void MainWindow::on_listWidget_2_itemSelectionChanged()
{
    QList<QListWidgetItem *> selectedItems = ui->listWidget_2->selectedItems();
    if (selectedItems.count() == 0) {
        ui->capitalEl->setText("");
        ui->populationEl->setText("");
        ui->squareEl->setText("");
        ui->longitudeEl->setText("");
        ui->latitudeEl->setText("");
        ui->removeButton->setEnabled(false);
        ui->editButton->setEnabled(false);
        ui->linkLabel->setText("");
    } else {
        QListWidgetItem * first = selectedItems.at(0);
        QVariant variant = first->data(Qt::UserRole);
        Capital * c = variant.value<Capital *>();
        ui->capitalEl->setText(c->getName());
        ui->populationEl->setText(QString::number(c->getPopulation()));
        ui->squareEl->setText(QString::number(c->getSquare()));
        ui->longitudeEl->setText("λ -> " + QString::number(c->getLongitude()) + "°");
        ui->latitudeEl->setText("φ -> " + QString::number(c->getLatitude()) + "°");
        ui->removeButton->setEnabled(true);
        ui->editButton->setEnabled(true);
        search(c->getName());
    }
}

void MainWindow::on_removeButton_clicked()
{
    QList<QListWidgetItem *> selectedItems = ui->listWidget_2->selectedItems();
    for (QListWidgetItem * item : selectedItems) {
        int row = ui->listWidget_2->row(item);
        QListWidgetItem* i = ui->listWidget_2->takeItem(row);
        delete i;
    }
}

void MainWindow::on_saveBtn_clicked()
{
    QString saveFile = QFileDialog::getSaveFileName(this, tr("Save file"), "", tr("Json File (*.json)"));
    std::cout << saveFile.toStdString() << std::endl;
    std::vector<Capital *> capitals;
    for (int i = 0; i < ui->listWidget_2->count(); i++) {
        QListWidgetItem * item = ui->listWidget_2->item(i);
        QVariant variant = item->data(Qt::UserRole);
        capitals.push_back(variant.value<Capital *>());
    }
    QJsonDocument doc;
    QJsonArray capitalArray;
    for(int i = 0; i < capitals.size(); i++) {
        QJsonObject capitalObj;
        capitalObj.insert("name", capitals[i]->getName());
        capitalObj.insert("population", capitals[i]->getPopulation());
        capitalObj.insert("square", capitals[i]->getSquare());
        QJsonObject coordsObj;
        coordsObj.insert("longitude", capitals[i]->getLongitude());
        coordsObj.insert("latitude", capitals[i]->getLatitude());
        capitalObj.insert("coordinates", coordsObj);
        capitalArray.append(capitalObj);
    }
    doc.setArray(capitalArray);
    std::string str = doc.toJson().toStdString();
    //std::cout << str << std::endl;
    std::ofstream F (saveFile.toStdString(), std::ios::out);
    F << str;
    F.close();
}

void MainWindow::on_loadBtn_clicked()
{
    QString loadFile = QFileDialog::getOpenFileName(this, tr("Open file"), "", tr("Json File (*.json)"));
    std::cout << loadFile.toStdString() << std::endl;
    QJsonParseError err;
    std::string fileInclude = writeFileToString(loadFile.toStdString());
    QJsonDocument doc = QJsonDocument::fromJson(QByteArray::fromStdString(fileInclude), &err);
    if (err.error != QJsonParseError::NoError) {
        std::string msg = "JSon Parse Error" + err.errorString().toStdString();
        std::cout << msg << std::endl;
    }
    QJsonArray capitalsArray = doc.array();
    if (ui->listWidget_2->count() > 0) {
        ui->listWidget_2->clear();
    }
    for (int j = 0; j < capitalsArray.size(); j++) {
        QJsonObject capitalObj = capitalsArray.at(j).toObject();
        QJsonValue nameValue = capitalObj.value("name");
        QJsonValue populationValue = capitalObj.value("population");
        QJsonValue squareValue = capitalObj.value("square");
        QJsonObject coords = capitalObj.value("coordinates").toObject();
        QJsonValue longitudeValue = coords.value("longitude");
        QJsonValue latitudeValue = coords.value("latitude");
        Capital * c = new Capital(nameValue.toString(),
                                  populationValue.toInt(),
                                  squareValue.toDouble(),
                                  longitudeValue.toDouble(),
                                  latitudeValue.toDouble());
        QListWidgetItem * item = new QListWidgetItem();
        item->setText(c->caption());
        QVariant variant;
        variant.setValue(c);
        item->setData(Qt::UserRole, variant);
        ui->listWidget_2->addItem(item);
    }
}

void MainWindow::search(const QString& searchTerm) {
    QUrl url = apiUrl;
    QUrlQuery urlQuery;
    urlQuery.addQueryItem(QString("action"), QString("query"));
    urlQuery.addQueryItem(QString("format"), QString("xml"));
    urlQuery.addQueryItem(QString("list"), QString("search"));
    urlQuery.addQueryItem(QString("srsearch"), searchTerm);
    urlQuery.addQueryItem(QString("srlimit"), QString::number(1));
    url.setQuery(urlQuery);

    QNetworkRequest request;
    request.setUrl(url);
    request.setRawHeader("User-Agent", "MyOwnBrowser 0.1");

    qDebug() << "Constructed search URL: " << request.url();
    QNetworkReply * reply = manager->get(request);
}

bool MainWindow::processSearchResult(QIODevice * source) {
    results.clear();
    QXmlStreamReader reader(source);
    qDebug() << reader.attributes().value("title");
    while(!reader.atEnd()) {
        QXmlStreamReader::TokenType tokenType = reader.readNext();
        qDebug() << "Token" << int(tokenType);
        if (tokenType == QXmlStreamReader::StartElement) {
            if (reader.name() == QString("p")) {
                QXmlStreamAttributes attrs = reader.attributes();
                qDebug() << "Found page" << attrs.value("pageid");
                results << attrs.value("pageid").toString();
                return true;
            }
        } else if (tokenType == QXmlStreamReader::Invalid) {
            qDebug() << "Invalid";
            return false;
        }
    }
    qDebug() << "Results" << results;
    return true;
}

void MainWindow::onFinished(QNetworkReply *reply) {
    if (reply->error() != QNetworkReply::NoError) {
        qDebug() << "Request failed, " << reply->errorString();
        return;
    }

    processSearchResult(reply);
    ui->linkLabel->setText("");
    if (results.count() > 0) { ui->linkLabel->setText("< a href=\"https://en.wikipedia.org/?curid=" + results.at(0) + "\"> Link to Wiki (" + ui->capitalEl->text() + ")</a>"); }
    reply->deleteLater();
}
