#include "adddialog.h"
#include "ui_adddialog.h"
#include "mainwindow.h"
#include <iostream>

addDialog::addDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::addDialog)
{
    ui->setupUi(this);
}

addDialog::~addDialog()
{
    delete ui;
}

void addDialog::on_addButton_clicked()
{
    QString Name = ui->capitalNameEl->text();
    int population = ui->populationEl->value();
    float square = ui->squareEl->value();
    float longitude = ui->longitudeEl->value();
    float latitude = ui->latitudeEl->value();
    Capital * c = new Capital(Name, population, square, longitude, latitude);
    emit sendCapital(c);
    close();
}

void addDialog::on_cancelButton_clicked()
{
    close();
}
