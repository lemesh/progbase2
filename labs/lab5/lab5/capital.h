#ifndef CAPITAL_H
#define CAPITAL_H

#include <QObject>

class Capital : public QObject
{
    Q_OBJECT
    QString name;
    int populaion;
    float square;
    float longitude;
    float latitude;

public:
    QString getName();
    int getPopulation();
    float getSquare();
    float getLongitude();
    float getLatitude();

    void setName(QString name);
    void setPopulation(int populaion);
    void setSquare(float square);
    void setLongitude(float longitude);
    void setLatitude(float latitude);

    explicit Capital(QObject *parent = nullptr);
    explicit Capital(QString name, int population, float square, float longitude, float latitude, QObject *parent = nullptr);

    QString caption();

signals:

public slots:
};

#endif // CAPITAL_H
