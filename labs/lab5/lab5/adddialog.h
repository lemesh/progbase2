#ifndef ADDDIALOG_H
#define ADDDIALOG_H

#include <QDialog>
#include "capital.h"

namespace Ui {
class addDialog;
}

class addDialog : public QDialog
{
    Q_OBJECT

public:
    explicit addDialog(QWidget *parent = 0);
    ~addDialog();

signals:
    void sendCapital(Capital * c);

private slots:

    void on_addButton_clicked();

    void on_cancelButton_clicked();

private:
    Ui::addDialog *ui;
};

#endif // ADDDIALOG_H
