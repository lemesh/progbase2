#ifndef EDITDIALOG_H
#define EDITDIALOG_H

#include <QDialog>
#include "capital.h"

namespace Ui {
class editDialog;
}

class editDialog : public QDialog
{
    Q_OBJECT

public:
    explicit editDialog(QWidget *parent = 0);
    ~editDialog();

signals:
    void sendCapital(Capital *);

public slots:
    void getCapital(Capital *);

private slots:
    void on_addBtn_clicked();

    void on_cancelBtn_clicked();

private:
    Ui::editDialog *ui;
    Capital * capital;
};

#endif // EDITDIALOG_H
