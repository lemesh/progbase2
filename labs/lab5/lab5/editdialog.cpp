#include "editdialog.h"
#include "ui_editdialog.h"

editDialog::editDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::editDialog)
{
    ui->setupUi(this);
}

editDialog::~editDialog()
{
    delete ui;
}

void editDialog::getCapital(Capital * c) {
    ui->capitalEl->setText(c->getName());
    ui->populationEl->setValue(c->getPopulation());
    ui->squareEl->setValue(c->getSquare());
    ui->longitudeEl->setValue(c->getLongitude());
    ui->latitudeEl->setValue(c->getLatitude());
    this->capital = c;
}

void editDialog::on_addBtn_clicked()
{
    QString Name = ui->capitalEl->text();
    long population = ui->populationEl->value();
    float square = ui->squareEl->value();
    float longitude = ui->longitudeEl->value();
    float latitude = ui->latitudeEl->value();
    capital->setName(Name);
    capital->setPopulation(population);
    capital->setSquare(square);
    capital->setLongitude(longitude);
    capital->setLatitude(latitude);
    close();
}

void editDialog::on_cancelBtn_clicked()
{
    close();
}
