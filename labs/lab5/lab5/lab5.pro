#-------------------------------------------------
#
# Project created by QtCreator 2018-04-14T20:30:12
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = lab5
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    adddialog.cpp \
    editdialog.cpp \
    capital.cpp

HEADERS  += mainwindow.h \
    adddialog.h \
    editdialog.h \
    capital.h

FORMS    += mainwindow.ui \
    adddialog.ui \
    editdialog.ui
