#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <vector>
#include <QNetworkAccessManager>
#include "capital.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    QStringList results;
    QUrl apiUrl;
    QNetworkAccessManager * manager;

    void search(const QString& searchTerm);
    bool processSearchResult(QIODevice * source);

signals:
    void sendCapital(Capital *);

public slots:
    void getCapital(Capital *);

private slots:
    void on_addButton_clicked();
    void onFinished(QNetworkReply * reply);
    void on_editButton_clicked();

    void on_executeButton_clicked();

    void on_listWidget_2_itemSelectionChanged();

    void on_removeButton_clicked();

    void on_saveBtn_clicked();

    void on_loadBtn_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
