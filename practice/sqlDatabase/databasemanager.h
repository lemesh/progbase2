// databasemanager.h

#ifndef DATABASEMANAGER_H
#define DATABASEMANAGER_H

#include <QList>
#include "student.h"
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>

class DatabaseManager
{
    QSqlDatabase _db;
public:
    DatabaseManager(QString & databasePath);

    bool open();
    void close();

    QList<Student *> selectAllStudents();
    void updateStudent(Student * s);
};

#endif // DATABASEMANAGER_H
