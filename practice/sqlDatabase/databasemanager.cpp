#include "databasemanager.h"

DatabaseManager::DatabaseManager(QString & databaseName)
{
    this->_db = QSqlDatabase::addDatabase("QSQLITE");
    this->_db.setDatabaseName(databaseName);
}

bool DatabaseManager::open() {
    return this->_db.open();
}

void DatabaseManager::close() {
    this->_db.close();
}

Student * studentFromQuery(QSqlQuery & query) {
    Student * s = new Student();
    s->id = query.value("id").toInt();
    s->name = query.value("name").toString();
    s->surname = query.value("surname").toString();
    s->score = query.value("score").toInt();
    return s;
}

QList<Student *> DatabaseManager::selectAllStudents() {
    QList<Student *> students;
    QSqlQuery query("SELECT * FROM students");
    while(query.next()) {
        Student * s = studentFromQuery(query);
        students.push_back(s);
    }
    return students;
}

void DatabaseManager::updateStudent(Student * s) {
    QSqlQuery query;
    query.prepare("UPDATE students SET name = :name, surname = :surname, score = :score WHERE id = :id");
    query.bindValue(":id", s->id);
    query.bindValue(":name", s->name);
    query.bindValue(":surname", s->surname);
    query.bindValue(":score", s->score);
    if (!query.exec()) {
        // @todo
    }
}
