#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "student.h"
#include "databasemanager.h"
#include <QFileDialog>


#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QString path = QFileDialog::getOpenFileName(this, "Open file", "", "*.sqlite3");
    _db = new DatabaseManager(path);
    if(!_db->open()) {
        return;
        //qDebug() << "can't connect: " << db.lastError();
    }
}

MainWindow::~MainWindow()
{
    removeAllStudents();
    _db->close();
    delete _db;
    delete ui;
}

void MainWindow::removeAllStudents() {
    for (int i = 0; i < ui->studentsTable->rowCount(); i++) {
        auto item = ui->studentsTable->takeItem(i, 0);
        Student * s = getItemStudent(item);
        delete s;
        for(int j = 0; j < 4; j++) {
            ui->studentsTable->takeItem(i, j);
        }
        ui->studentsTable->removeRow(i);
    }
}

void MainWindow::addStudent(Student * s) {
    int newRowIndex = ui->studentsTable->rowCount();
    ui->studentsTable->insertRow(newRowIndex);
    auto idItem = new QTableWidgetItem(QString::number(s->id));
    QVariant variant;
    variant.setValue(s);
    idItem->setData(Qt::UserRole, variant);
    ui->studentsTable->setItem(newRowIndex, 0, idItem);
    ui->studentsTable->setItem(newRowIndex, 1, new QTableWidgetItem(s->name));
    ui->studentsTable->setItem(newRowIndex, 2, new QTableWidgetItem(s->surname));
    ui->studentsTable->setItem(newRowIndex, 3, new QTableWidgetItem(QString::number(s->score)));
}

void MainWindow::on_selectBtn_clicked()
{
    QList<Student *> students = _db->selectAllStudents();
    removeAllStudents();
    for (auto student : students) {
        addStudent(student);
    }
}

Student * MainWindow::getItemStudent(QTableWidgetItem *item)
{
    int row = ui->studentsTable->row(item);
    QTableWidgetItem * idItem = ui->studentsTable->item(row, 0);
    QVariant variant = idItem->data(Qt::UserRole);
    return variant.value<Student *>();
}

void MainWindow::on_studentsTable_itemChanged(QTableWidgetItem *item)
{
    Student * student = getItemStudent(item);
    int column = ui->studentsTable->column(item);
    switch (column) {
    case 0: // id
        break;
    case 1: // name
        student->name = item->text();
        break;
    case 2: // student
        student->surname = item->text();
        break;
    case 3: // score
        student->score = item->text().toInt();
        break;
    }
    _db->updateStudent(student);
}

//void MainWindow::deleteRow(QTableWidgetItem * item) {

//}
