#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "student.h"
#include "databasemanager.h"

#include <QTableWidget>
#include <QTableWidgetItem>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_selectBtn_clicked();

    void on_studentsTable_itemChanged(QTableWidgetItem *item);

private:
    Ui::MainWindow *ui;

    DatabaseManager * _db;

    void addStudent(Student * s);
    Student * getItemStudent(QTableWidgetItem *item);
    void removeAllStudents();

};

#endif // MAINWINDOW_H
