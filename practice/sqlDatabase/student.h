#ifndef STUDENT_H
#define STUDENT_H

#include <QObject>

class Student : public QObject
{
    Q_OBJECT
public:
    explicit Student(QObject *parent = nullptr);
    int id;
    QString name;
    QString surname;
    int score;

signals:

public slots:
};

#endif // STUDENT_H
