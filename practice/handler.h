#ifndef HANDLER_H
#define HANDLER_H

#include <QObject>
#include <QDebug>

class Handler : public QObject
{
    Q_OBJECT
public:
    explicit Handler(QObject *parent = nullptr);

signals:
    void started();
    void finished();

public slots:
    void run();
    void onStarted();
};

#endif // HANDLER_H
