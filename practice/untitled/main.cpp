#include <QtWidgets>

int main(int argc, char** argv) {
    QApplication app(argc, argv);

    QScrollArea  sa;

    QWidget*     pwgt = new QWidget;
    QPixmap      pix(":/img.jpg");

    QPalette pal;
    pal.setBrush(pwgt->backgroundRole(), QBrush(pix));
    pwgt->setPalette(pal);
    pwgt->setAutoFillBackground(true);
    pwgt->setFixedSize(pix.width(), pix.height());

    sa.setWidget(pwgt);
    sa.resize(350, 150);
    sa.show();

//    QWidget wgt;
//    QPixmap pix(":/clock.png");
//    QCursor cur(pix);

//    wgt.setCursor(cur);
//    wgt.resize(180, 100);
//    wgt.show();

//    QWidget wgt;

//    QWidget* pwgt1 = new QWidget(&wgt);
//    QPalette pal1;
//    pal1.setColor(pwgt1->backgroundRole(), Qt::blue);
//    pwgt1->setPalette(pal1);
//    pwgt1->resize(100, 100);
//    pwgt1->move(25, 25);
//    pwgt1->setAutoFillBackground(true);

//    QWidget* pwgt2 = new QWidget(&wgt);
//    QPalette pal2;
//    pal2.setBrush(pwgt2->backgroundRole(), QBrush(QPixmap(":/stone.jpg")));
//    pwgt2->setPalette(pal2);
//    pwgt2->resize(100, 100);
//    pwgt2->move(75, 75);
//    pwgt2->setAutoFillBackground(true);

//    wgt.resize(200, 200);
//    wgt.show();

    return app.exec();
}
