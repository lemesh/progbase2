#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "student.h"
#include <iostream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    Student * s1 = new Student();
    s1->name = "Gena";
    s1->surname = "Finito";
    s1->age = 19;

    QListWidgetItem * i1 = new QListWidgetItem();
    i1->setText(s1->caption());
    QVariant variant;
    variant.setValue(s1);
    i1->setData(Qt::UserRole, variant);
    ui->listWidget->addItem(i1);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_addBtn_clicked()
{
    QString new_value = ui->valueEl->text();
    ui->listWidget->addItem(new_value);
}

void MainWindow::on_listWidget_itemSelectionChanged()
{
    QList<QListWidgetItem*> selected_items = ui->listWidget->selectedItems();
    if (selected_items.count() == 0) {
        ui->removeBtn->setEnabled(false);
        ui->nameEl->setText("");
        ui->nameEl->setEnabled(false);
        ui->surnameEl->setText("");
        ui->surnameEl->setEnabled(false);
        ui->ageEl->setValue(0);
        ui->ageEl->setEnabled(false);
    } else {
        ui->removeBtn->setEnabled(true);
        ui->nameEl->setEnabled(true);
        ui->surnameEl->setEnabled(true);
        ui->ageEl->setEnabled(true);
        QListWidgetItem * first = selected_items.at(0);

        QVariant variant = first->data(Qt::UserRole);

        Student * student = variant.value<Student *>();

        //QString caption = first->text();
        ui->nameEl->setText(student->name);
        ui->surnameEl->setText(student->surname);
        ui->ageEl->setValue(student->age);
    }
}

void MainWindow::on_removeBtn_clicked()
{
    QList<QListWidgetItem*> selected_items = ui->listWidget->selectedItems();
    for (QListWidgetItem* item : selected_items) {
        int row = ui->listWidget->row(item);
        QListWidgetItem* i = ui->listWidget->takeItem(row);
        delete i;
    }
}

void MainWindow::on_nameEl_textChanged(const QString &arg1)
{
    QList<QListWidgetItem*> selected_items = ui->listWidget->selectedItems();
    if (selected_items.count() == 0) return;
    QListWidgetItem * first = selected_items.at(0);
    QVariant variant = first->data(Qt::UserRole);
    Student * student = variant.value<Student *>();
    student->name = arg1;
    first->setText(student->caption());
}

void MainWindow::on_surnameEl_textChanged(const QString &arg1)
{
    QList<QListWidgetItem*> selected_items = ui->listWidget->selectedItems();
    if (selected_items.count() == 0) return;
    QListWidgetItem * first = selected_items.at(0);
    QVariant variant = first->data(Qt::UserRole);
    Student * student = variant.value<Student *>();
    student->surname = arg1;
    first->setText(student->caption());
}

void MainWindow::on_ageEl_valueChanged(int arg1)
{
    QList<QListWidgetItem*> selected_items = ui->listWidget->selectedItems();
    if (selected_items.count() == 0) return;
    QListWidgetItem * first = selected_items.at(0);
    QVariant variant = first->data(Qt::UserRole);
    Student * student = variant.value<Student *>();
    student->age = arg1;
    first->setText(student->caption());
}
