#include "student.h"

Student::Student(QObject *parent) : QObject(parent)
{

}

QString Student::caption() {
    return this->name + " " + this->surname + " (" + QString::number(this->age) + ")";
}
