#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_addBtn_clicked();

    void on_listWidget_itemSelectionChanged();

    void on_removeBtn_clicked();

    void on_nameEl_textChanged(const QString &arg1);

    void on_surnameEl_textChanged(const QString &arg1);

    void on_ageEl_valueChanged(int arg1);

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
