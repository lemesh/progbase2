#ifndef STUDENT_H
#define STUDENT_H

#include <QObject>

class Student : public QObject
{
    Q_OBJECT
public:
    QString name;
    QString surname;
    int age;

    explicit Student(QObject *parent = nullptr);

    QString caption();

signals:

public slots:
};

#endif // STUDENT_H
