#ifndef STUDENT_H
#define STUDENT_H

#include <QObject>

class Student : public QObject
{
    Q_OBJECT
public:
    int id;
    QString name;
    QString surname;
    int score;
    explicit Student(QObject *parent = nullptr);

signals:

public slots:
};

#endif // STUDENT_H
