#ifndef DATABASEMANGER_H
#define DATABASEMANGER_H

#include <QString>
#include <QList>
#include "student.h"
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>

class DatabaseManger
{
    QSqlDatabase _db;
public:
    DatabaseManger(QString & databasePath);

    bool open();
    bool close();

    QList<Student *> selectStudents();
};

#endif // DATABASEMANGER_H
