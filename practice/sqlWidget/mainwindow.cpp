#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QDebug>
#include <QSqlError>
#include "student.h"
#include "databasemanger.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

Student * studentFromQuery(QSqlQuery * query) {
    Student * s = new Student();
    s->id = query->value("id").toInt();
    s->name = query->value("name").toString();
    s->surname = query->value("surname").toString();
    s->score = query->value("score").toInt();
    return s;
}

void MainWindow::on_selectBtn_clicked()
{
    QString path = QFileDialog::getOpenFileName(this, tr("sql file"), "", tr("*.sqlite3"));
    DatabaseManger(path);
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(path);
    if(!db.open()) {
        qDebug() << "can't connect" << db.lastError();
    }

    QSqlQuery query("SELECT * FROM students");
    while(query.next()) {
        Student * s = studentFromQuery(&query);

        qDebug() << "Open database";
        int newRowIndex = ui->studentsTable->rowCount();
        ui->studentsTable->insertRow(newRowIndex);
        ui->studentsTable->setItem(newRowIndex, 0, new QTableWidgetItem(QString::number(s->id)));
        ui->studentsTable->setItem(newRowIndex, 1, new QTableWidgetItem(s->name));
        ui->studentsTable->setItem(newRowIndex, 2, new QTableWidgetItem(s->surname));
        ui->studentsTable->setItem(newRowIndex, 3, new QTableWidgetItem(QString::number(s->score)));
    }
    query.clear();
    int idToUpdate = 6;
    query.prepare("UPDATE students SET score= 100 WHERE id = :id");
    query.bindValue(":id", idToUpdate);
    if(!query.exec()) {
        qDebug() << query.lastError();
    }
    db.close();
}
