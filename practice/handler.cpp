#include "handler.h"

Handler::Handler(QObject *parent) : QObject(parent)
{

}

void Handler::run() {
    emit started();
    qDebug() << "RUN";
    emit finished();
}

void Handler::onStarted() {
    qDebug() << "QTCREATOR";
}
