#include <QCoreApplication>
#include <QTimer>
#include "handler.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    Handler * h = new Handler(&a);
    Handler * h2 = new Handler(&a);
    QObject::connect(h, &Handler::started, h2, &Handler::onStarted);
    QTimer::singleShot(2000, h, &Handler::run);
    QObject::connect(h, &Handler::finished, &QCoreApplication::quit);
    return a.exec();
}
