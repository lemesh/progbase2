#ifndef ADDDIALOG_H
#define ADDDIALOG_H

#include <QDialog>
#include "student.h"

namespace Ui {
class addDialog;
}

class addDialog : public QDialog
{
    Q_OBJECT

public:
    explicit addDialog(QWidget *parent = 0);
    ~addDialog();

signals:
    void sendStudent(Student * s);

private slots:
    void on_okBtn_clicked();

private:
    Ui::addDialog *ui;
};

#endif // ADDDIALOG_H
