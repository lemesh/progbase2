#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "student.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void getStudent(Student * s);

private slots:
    void on_addBtn_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
