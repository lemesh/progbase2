#ifndef STUDENT_H
#define STUDENT_H

#include <QObject>

class Student : public QObject
{
    Q_OBJECT
public:
    QString Name;
    QString Surname;
    int age;

    explicit Student(QObject *parent = nullptr);

    QString caption();

signals:

public slots:
};

#endif // STUDENT_H
