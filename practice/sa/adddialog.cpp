#include "adddialog.h"
#include "ui_adddialog.h"
#include "student.h"
#include "mainwindow.h"

addDialog::addDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::addDialog)
{
    ui->setupUi(this);
}

addDialog::~addDialog()
{
    delete ui;
}

void addDialog::on_okBtn_clicked()
{
    Student * s = new Student();
    QString name = ui->nameEl->text();
    QString surname = ui->surnameEl->text();
    int age = ui->ageEl->value();
    s->Name = name;
    s->Surname = surname;
    s->age = age;
    emit sendStudent(s);
    close();
}
