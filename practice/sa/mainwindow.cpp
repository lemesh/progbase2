#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "student.h"
#include "adddialog.h"
#include <iostream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_addBtn_clicked()
{
    addDialog dialog;
    //dialog.setModal(true);
    connect(&dialog, SIGNAL(sendStudent(Student*)), this, SLOT(getStudent(Student*)));
    dialog.exec();
}

void MainWindow::getStudent(Student * s) {
    QListWidgetItem * i = new QListWidgetItem();
    i->setText(s->caption());
    QVariant variant;
    variant.setValue(s);
    i->setData(Qt::UserRole, variant);
    ui->listWidget->addItem(i);
}
