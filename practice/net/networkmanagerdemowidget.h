#ifndef NETWORKMANAGERDEMOWIDGET_H
#define NETWORKMANAGERDEMOWIDGET_H

#include <QWidget>
#include <QNetworkAccessManager>

namespace Ui {
class NetworkManagerDemoWidget;
}

class NetworkManagerDemoWidget : public QWidget
{
    Q_OBJECT

public:
    explicit NetworkManagerDemoWidget(QWidget *parent = 0);
    ~NetworkManagerDemoWidget();

    QStringList results;
    QUrl apiUrl;
    QNetworkAccessManager * manager;
    int maxItems;

    void search(const QString& searchTerm);
    bool processSearchResult(QIODevice * source);

private slots:
    void onFinished(QNetworkReply * reply);

    void on_bnGo_clicked();

private:
    Ui::NetworkManagerDemoWidget *ui;
};

#endif // NETWORKMANAGERDEMOWIDGET_H
