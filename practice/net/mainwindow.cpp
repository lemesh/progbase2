#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QTextCodec>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->lineEdit->setText("http://avazart.zz.mu");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    QNetworkAccessManager * manager = new QNetworkAccessManager(this);
    QNetworkRequest request;
    //request.setUrl("http://qt.nokia.com");
    request.setRawHeader("User-Agent", "MyOwnBrowser 1.0");

    QNetworkReply * reply = manager->get(request);
    connect(reply, SIGNAL(readyRead()),
            this, SLOT(slotReadyRead()));
    connect(reply, SIGNAL(error(QNetworkReply::NetworkError)),
            this, SLOT(slotError(QNetworkReply::NetworkError)));
    connect(reply, SIGNAL(sslErrors(QList<QSslError>)),
            this, SLOT(slotSslError(QList<QSslError>)));
}

void MainWindow::replyFinished(QNetworkReply * reply) {

}
