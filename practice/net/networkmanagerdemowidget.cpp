#include "networkmanagerdemowidget.h"
#include "ui_networkmanagerdemowidget.h"

#include <QNetworkReply>
#include <QUrlQuery>
#include <QDebug>
#include <QXmlStreamReader>

NetworkManagerDemoWidget::NetworkManagerDemoWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::NetworkManagerDemoWidget)
{
    ui->setupUi(this);
    apiUrl = QUrl("http://en.wikipedia.org/w/api.php");
    manager = new QNetworkAccessManager(this);
    maxItems = 10;
    connect(manager, SIGNAL(finished(QNetworkReply*)), SLOT(onFinished(QNetworkReply*)));
}

NetworkManagerDemoWidget::~NetworkManagerDemoWidget()
{
    delete ui;
}

void NetworkManagerDemoWidget::search(const QString& searchTerm) {
    QUrl url = apiUrl;
    QUrlQuery urlQuery;
    urlQuery.addQueryItem(QString("action"), QString("query"));
    urlQuery.addQueryItem(QString("format"), QString("xml"));
    urlQuery.addQueryItem(QString("list"), QString("search"));
    urlQuery.addQueryItem(QString("srsearch"), searchTerm);
    urlQuery.addQueryItem(QString("srlimit"), QString::number(maxItems));
    url.setQuery(urlQuery);

    qDebug() << "Constructed search URL: " << url;
    manager->get(QNetworkRequest(url));
}

void NetworkManagerDemoWidget::onFinished(QNetworkReply *reply) {
    if (reply->error() != QNetworkReply::NoError) {
        qDebug() << "Request failed, " << reply->errorString();
        //emit finished(false);
        return;
    }

    qDebug() << "Request succeeded";
    bool ok = processSearchResult(reply);
    //emit finished(ok);
}

bool NetworkManagerDemoWidget::processSearchResult(QIODevice * source) {
    results.clear();
    QXmlStreamReader reader(source);
    while(!reader.atEnd()) {
        QXmlStreamReader::TokenType tokenType = reader.readNext();
        qDebug() << "Token" << int(tokenType);
        if (tokenType == QXmlStreamReader::StartElement) {
            if (reader.name() == QString("p")) {
                QXmlStreamAttributes attrs = reader.attributes();
                qDebug() << "Found page" << attrs.value("title");
                results << attrs.value("title").toString();
            }
        } else if (tokenType == QXmlStreamReader::Invalid) {
            return false;
        }
    }
    qDebug() << "Results" << results;
    return true;
}

void NetworkManagerDemoWidget::on_bnGo_clicked()
{
    search(ui->edUrl->text());
}
