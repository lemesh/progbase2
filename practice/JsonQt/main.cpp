#include <vector>
#include <iostream>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

using namespace std;

class Student {
public:
    QString name;
    int age;

};

class Group
{
public:
    QString name;
    vector<Student> students;
};

class MessageException : public std::exception {
    string errorMessage;
public:
    MessageException(string err) {
        this->errorMessage = err;
    }
    const char * what() const noexcept {
        return this->errorMessage.c_str();
    }
};

void printGroup(const Group & g);
string toJsonString(const Group & g);
Group fromJsonString(string str);

int main() {
    Group g;
    g.name = "KP72";
    g.students.push_back(Student{"Anna", 17});
    g.students.push_back(Student{"Ivan", 18});
    g.students.push_back(Student{"Misha", 19});

    printGroup(g);

    string str = toJsonString(g);

    cout << "----------------------------" << endl
        << str << endl
        << "----------------------------" << endl;

    try {
        Group g2 = fromJsonString(str);
        printGroup(g2);
    } catch (MessageException & err) {
        cout << err.what() << endl;
    }

    return 0;
}


void printGroup(const Group &g) {
    cout << "name: " << g.name.toStdString() << endl
         << "students(" << g.students.size() << "): " << endl;
    for (const Student & s : g.students) {
        cout << " student: " << s.name.toStdString() << " | " << s.age << endl;
    }
}

string toJsonString(const Group & g) {
    QJsonDocument doc;
    QJsonObject groupObj;
    groupObj.insert("name", g.name);
    QJsonArray studentsArr;
    for (auto & st : g.students) {
        QJsonObject stObj;
        stObj.insert("name", st.name);
        stObj.insert("age", st.age);
        studentsArr.append(stObj);
    }
    groupObj.insert("students", studentsArr);
    doc.setObject(groupObj);
    return doc.toJson().toStdString();
}

Group fromJsonString(string str) {
    QJsonParseError err;
    QJsonDocument doc = QJsonDocument::fromJson(QByteArray::fromStdString("{"), &err);
    if(err.error != QJsonParseError::NoError) {
        string msg = "JSon Parse error " + err.errorString().toStdString();
        throw MessageException(msg);
    }
    QJsonObject groupObj = doc.object();
    QJsonValue nameValue = groupObj.value("name");
    Group g;
    g.name = nameValue.toString();
    QJsonArray studentsArr = groupObj.value("students").toArray();
    for (int i = 0; i < studentsArr.size(); i++) {
        QJsonObject stObj = studentsArr.at(i).toObject();
        g.students.push_back(Student {
                                 stObj.value("name").toString(),
                                 stObj.value("age").toInt()
                             });
    }
    return g;
}
