QT += core
QT -= gui
QT += xml

TARGET = json_xml
CONFIG += c++14
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp

HEADERS += \
    employee.h

DISTFILES += \
    data.json \
    data.xml

