#include <QDebug>
#include <QtXml>
#include <vector>
#include <iostream>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <string>
#include <iterator>
#include <fstream>


using namespace std;

class Department{
public:
    QString name;
};

class Employee {
public:
    QString name;
    int     age;
    double  score;
    vector<Department> departs;
};

class MessageException : public std::exception {
    string _error;
public:
    MessageException(std::string & error) {
        this->_error = error;
    }
    const char * what() const noexcept {
        return this->_error.c_str();
    }
};

class EntityStorage {
     std::string _name;
protected:
     EntityStorage(std::string & name) { this->_name = name; }
public:
     std::string & name() { return this->_name; }

     virtual std::vector<Employee> load() = 0;
     virtual void save(std::vector<Employee> & employees) = 0;
};

class XmlEntityStorage : public EntityStorage {
public:
    XmlEntityStorage(std::string name) : EntityStorage(name) { }
    std::vector<Employee> load(){
        QDomDocument doc;
        ifstream F (name());
        if(!F.is_open()){
            std::string m = "File do not exist";
            throw MessageException(m);
        }
        std::string str {
                std::string(std::istreambuf_iterator<char>(F),  std::istreambuf_iterator<char>())};
            if (!doc.setContent(QString::fromStdString(str))) {
                    std::string m = "error parsing xml!";
                  throw MessageException(m);
            }
            vector<Employee> list;
            QDomElement root = doc.documentElement();

            for (int i = 0; i < root.childNodes().length(); i++) {
                  QDomNode employee = root.childNodes().at(i);
                  QDomElement emplEl = employee.toElement();
                  Employee e;
                  e.name = emplEl.attribute("name");
                  e.age = emplEl.attribute("age").toInt();
                  e.score = emplEl.attribute("score").toDouble();
                  for(int i = 0; i < emplEl.childNodes().length(); i++){
                      QDomNode depart = root.childNodes().at(i);
                      QDomElement departEl = depart.toElement();
                      Department d;
                      d.name = departEl.attribute("name");
                      e.departs.push_back(d);
                  }
                  list.push_back(e);
            }
            return list;
}
    void save(std::vector<Employee> & employees) {
        std::ofstream F(name(), std::ios::out);
        if(!F.is_open()){
            std::string m = "File can't be made";
            throw MessageException(m);
        }
        QDomDocument doc;
            QDomElement emlList = doc.createElement("employees");
            for(const Employee & e: employees){
                QDomElement emlEl = doc.createElement("employee");
                emlEl.setAttribute("name", e.name);
                emlEl.setAttribute("age", e.age);
                emlEl.setAttribute("score", e.score);
                for(const Department & d: e.departs){
                    QDomElement depEl = doc.createElement("department");
                    depEl.setAttribute("department name", d.name);
                    emlEl.appendChild(depEl);
                }
                emlList.appendChild(emlEl);
            }

            doc.appendChild(emlList);
            F<<doc.toString().toStdString();
    }
};

class JsonEntityStorage: public EntityStorage{
public:
    JsonEntityStorage(std::string name) : EntityStorage(name) { }
    std::vector<Employee> load(){
        ifstream F (name());
        if(!F.is_open()){
            std::string m = "File do not exist";
            throw MessageException(m);
        }
        std::string str {
                std::string(std::istreambuf_iterator<char>(F),  std::istreambuf_iterator<char>())};

        QJsonParseError err;
           QJsonDocument doc = QJsonDocument::fromJson(
                  QByteArray::fromStdString(str),
                  &err);
           if (err.error != QJsonParseError::NoError) {
               std::string m = err.errorString().toStdString();
                   throw MessageException(m);
           }
           std::vector<Employee> list;
           QJsonObject listObj = doc.object();;
           QJsonArray employeesArr = listObj.value("employees").toArray();
           for (int i = 0; i < employeesArr.size(); i++) {
                 QJsonValue value = employeesArr.at(i);
                 QJsonObject employeeObj = value.toObject();
                 QJsonArray departsArr = employeeObj.value("departments").toArray();
                 vector<Department> departs;
                 for(int j = 0; j < departsArr.size(); j++){
                    QJsonValue val = departsArr.at(i);
                    QJsonObject departObj = val.toObject();
                    departs.push_back(Department{departObj.value("department name").toString()});
                 }
                 list.push_back(Employee {
                   employeeObj.value("name").toString(),
                   employeeObj.value("age").toInt(),
                   employeeObj.value("score").toDouble(),
                   departs
                 });
           }
           return list;
    }
    void save(std::vector<Employee> & employees) {
        std::ofstream F (name(), std::ios::out);
        if(!F.is_open()){
            std::string m = "File can't be made";
            throw MessageException(m);
        }
        QJsonDocument doc;
        QJsonObject list;
            QJsonArray employeesArr;
            for (auto & e: employees) {
                  QJsonObject es;
                  es.insert("name", e.name);
                  es.insert("age", e.age);
                  es.insert("score", e.score);
                  QJsonArray departsArr;
                  for(auto & d: e.departs){
                      QJsonObject ds;
                      ds.insert("department name",d.name);
                      departsArr.append(ds);
                  }
                  es.insert("departments", departsArr);
                  employeesArr.append(es);
            }
            list.insert("employees", employeesArr);

            doc.setObject(list);
            F<<doc.toJson().toStdString();

    }
};


class StorageManager {
public:
    static EntityStorage * createStorage(std::string & storageFileName){
        std::size_t pos = storageFileName.find('.');
        std::string format = storageFileName.substr (pos);
        if(format == ".json"){
            JsonEntityStorage *jf = new JsonEntityStorage(storageFileName);
            return jf;
        } else if (format == ".xml"){
            XmlEntityStorage *xf = new XmlEntityStorage(storageFileName);
            return xf;
        } else {
            std::string m = "Wrong format";
            throw MessageException(m);
        }
    }
};

int main()
{
     std::vector<Employee> list;

     for(int i = 1; i < 4; i++){
         Employee A1;
         A1.name = "Anya";
         A1.age = i;
         A1.score = 1/i;
         std::string a = "Department ne tvoih sobachyh del";
         A1.departs.push_back(a);
         std::string b = "Department nichego ne delau";
         A1.departs.push_back(b);
         list.push_back(A1);
     }

     list->save(test);
}
