#include <QCoreApplication>
#include <QSqlDatabase>
#include <QFileDialog>
#include <QSqlQuery>
#include <QSqlError>
#include <QDebug>

int main(int argc, char *argv[])
{
    QString path ("/mnt/c/Users/Oleh/sql.sqlite3");
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(path);
    if(!db.open()) {
        qDebug() << "can't connect" << db.lastError();
    }

    QSqlQuery query("SELECT * FROM students");
    while(query.next()) {
        int id = query.value("id").toInt();
        QString name = query.value("name").toString();
        QString surname = query.value("surname").toString();
        int score = query.value("score").toInt();
        qDebug() << "Open database";

        qDebug() << id << " | " << name << " | " << surname << " | " << score;
    }
    query.clear();
    int idToUpdate = 6;
    query.prepare("UPDATE students SET score= 100 WHERE id = :id");
    query.bindValue(":id", idToUpdate);
    if(!query.exec()) {
        qDebug() << query.lastError();
    }
    db.close();
    return 0;
}
