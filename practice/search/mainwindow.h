#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QNetworkAccessManager>
#include <QNetworkReply>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    QStringList results;
    QUrl apiUrl;
    QNetworkAccessManager * manager;
    int maxItems;

    void search(const QString& searchTerm);
    bool processSearchResult(QIODevice * source);

private slots:
        void onFinished(QNetworkReply * reply);
        void slotError(QNetworkReply::NetworkError);
        void slotSslErrors(QList<QSslError>);
        void on_pushButton_clicked();
        void slotReadyRead();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
