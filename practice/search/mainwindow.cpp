#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QUrlQuery>
#include <QXmlStreamReader>
#include <QNetworkReply>
#include <QNetworkRequest>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    apiUrl = "https://en.wikipedia.org/w/api.php";
    manager = new QNetworkAccessManager(this);
    maxItems = 10;
    connect(manager, SIGNAL( finished( QNetworkReply* ) ), SLOT( onFinished( QNetworkReply* ) ) );
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::search(const QString& searchTerm) {
    QUrl url = apiUrl;
    //QNetworkReply * post = manager->post(QNetworkRequest(url), "");
    QUrlQuery urlQuery;
    urlQuery.addQueryItem(QString("action"), QString("query"));
    urlQuery.addQueryItem(QString("format"), QString("xml"));
    urlQuery.addQueryItem(QString("list"), QString("search"));
    urlQuery.addQueryItem(QString("srsearch"), searchTerm);
    urlQuery.addQueryItem(QString("srlimit"), QString::number(maxItems));
//    urlQuery.addQueryItem(QString("action"), QString("query"));
//    urlQuery.addQueryItem(QString("titles"), QString("MainPage"));
//    urlQuery.addQueryItem(QString("prop"), QString("revisions"));
//    urlQuery.addQueryItem(QString("rvprop"), QString("content"));
//    urlQuery.addQueryItem(QString("format"), QString("jsonfm"));
    url.setQuery(urlQuery);

    QNetworkRequest request;
    request.setUrl(url);
    request.setRawHeader("User-Agent", "MyOwnBrowser 0.1");

    qDebug() << "Constructed search URL: " << request.url();
    QNetworkReply * reply = manager->get(request);
    //qDebug() << "Request succeeded0" << QString::fromUtf8(reply->readAll());
    connect(reply, SIGNAL(readyRead()), this, SLOT(slotReadyRead()));
    connect(reply, SIGNAL(error(QNetworkReply::NetworkError)),
             this, SLOT(slotError(QNetworkReply::NetworkError)));
    connect(reply, SIGNAL(sslErrors(QList<QSslError>)),
             this, SLOT(slotSslErrors(QList<QSslError>)));
}

void MainWindow::onFinished(QNetworkReply *reply) {
    //qDebug() << "Request succeeded1" << QString::fromUtf8(reply->readAll());
    if (reply->error() != QNetworkReply::NoError) {
        qDebug() << "Request failed, " << reply->errorString();
        //emit QNetworkReply::finished(false);
        return;
    }

    //qDebug() << "Request succeeded" << QString::fromUtf8(reply->readAll());
    bool ok = processSearchResult(reply);
    //emit QNetworkReply::finished(ok);
    ui->label->setText("< a href=\"https://en.wikipedia.org/?curid=" + results.at(0) + "\"> Link to Wiki </a>");
    reply->deleteLater();
}

bool MainWindow::processSearchResult(QIODevice * source) {
    results.clear();
    QXmlStreamReader reader(source);
    qDebug() << reader.attributes().value("title");
    while(!reader.atEnd()) {
        QXmlStreamReader::TokenType tokenType = reader.readNext();
        qDebug() << "Token" << int(tokenType);
        if (tokenType == QXmlStreamReader::StartElement) {
            qDebug() << "Here";
            if (reader.name() == QString("p")) {
                QXmlStreamAttributes attrs = reader.attributes();
                qDebug() << "Found page" << attrs.value("pageid");
                results << attrs.value("pageid").toString();
                return true;
            }
        } else if (tokenType == QXmlStreamReader::Invalid) {
            qDebug() << "Invalid";
            //return false;
        }
    }
    qDebug() << "Results" << results;
    return true;
}

void MainWindow::on_pushButton_clicked()
{
    search(ui->lineEdit->text());
}

void MainWindow::slotError(QNetworkReply::NetworkError) {
    qDebug() << "slotError";
}

void MainWindow::slotSslErrors(QList<QSslError>) {
    qDebug() << "slotSslErrors";
}

void MainWindow::slotReadyRead() {
    qDebug() << "slotReadyRead";
}
