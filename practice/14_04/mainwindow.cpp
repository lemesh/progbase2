#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "infodialog.h"
#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <iostream>
#include <QMessageBox>
#include <QCloseEvent>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    auto v = new QValidator();
    ui->nameEl->setValidator(v);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent *bar) {
    QMessageBox::StandardButton resBtn = QMessageBox::question(this, "On close", tr("Are yo sure?/n"), QMessageBox::No | QMessageBox::Yes, QMessageBox::No);

    if (resBtn == QMessageBox::Yes) {
        bar->accept();
    } else {
        bar->ignore();
    }
}

QString scoreToGrade(int score) {
    if (score < 60) {
        return "F";
    } else if (score < 65) {
        return "E";
    } else if (score < 75) {
        return "D";
    } else if (score < 85) {
        return "C";
    } else if (score < 95) {
        return "B";
    } else {
        return "A";
    }
}

void MainWindow::on_spinBox_valueChanged(int arg1){
    ui->gradeLabel->setText(scoreToGrade(arg1));
}

void MainWindow::on_checkBox_toggled(bool checked)
{
    ui->projectEl->setEnabled(checked);
}

void MainWindow::validateInput() {
    bool valid = ui->nameEl->text().isEmpty() || ui->surnameEl->text().isEmpty();
    ui->pushButton->setEnabled(valid);
}

QString MainWindow::toJson(QString name, QString surname, QString bday, int score, QString project) {
    QJsonDocument doc;
    QJsonObject textObj;
    textObj.insert("name", name);
    textObj.insert("surname", surname);
    textObj.insert("BirthDate", bday);
    textObj.insert("score", score);
    textObj.insert("project", project);
    doc.setObject(textObj);
    std::string str = doc.toJson().toStdString();
    return QString::fromStdString(str);
}

void MainWindow::on_pushButton_clicked()
{
    QString name = ui->nameEl->text();
    QString surname = ui->surnameEl->text();
    QDate bday = ui->birthEl->date();
    int score = ui->scoreEl->text().toInt();
    QTextDocument *tDoc = ui->projectEl->document();
    QString projects = tDoc->toPlainText();
    std::cout << name.toStdString() << "|" << surname.toStdString() << "|" << bday.toString().toStdString() << "|" << score << std::endl;
    infoDialog dialog(this);
    QString str = toJson(name, surname, bday.toString(), score, projects);
    dialog.setContents(str);
    int dialogResult = dialog.exec();
    qDebug() << dialogResult;
}

void MainWindow::on_nameEl_textChanged(const QString &arg1)
{
    validateInput();
}

void MainWindow::on_surnameEl_textChanged(const QString &arg1)
{
    validateInput();
}
