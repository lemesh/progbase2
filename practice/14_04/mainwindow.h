#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_spinBox_valueChanged(int arg1);

    void on_checkBox_toggled(bool checked);

    void on_pushButton_clicked();

    QString toJson(QString name, QString surname, QString bday, int score, QString project);

    void on_nameEl_textChanged(const QString &arg1);

    void on_surnameEl_textChanged(const QString &arg1);

private:
    Ui::MainWindow *ui;

    void validateInput();

    void closeEvent(QCloseEvent *bar);
};

#endif // MAINWINDOW_H
