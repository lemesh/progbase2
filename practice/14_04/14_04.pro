#-------------------------------------------------
#
# Project created by QtCreator 2018-04-14T10:34:35
#
#-------------------------------------------------

QT       += core gui
QT += widgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = 14_04
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    infodialog.cpp

HEADERS  += mainwindow.h \
    infodialog.h

FORMS    += mainwindow.ui \
    infodialog.ui
