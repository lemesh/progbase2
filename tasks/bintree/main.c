#include <stdio.h>
#include <bstree.h>
#include <stdlib.h>

int main() {
    int vals[] = {-18, -10, 3, 11, 16, -4, 12};
    BSTree * new = BSTree_new();
    for (int i = 0; i < sizeof(vals) / sizeof(vals[0]); i++) {
        BSTree_insert(new, vals[i]);
    }
    BSTree_printFormat(new);
    BSTree_printTraverse(new);
    BSTree_clear(new);
    BSTree_free(new);
    return 0;
}