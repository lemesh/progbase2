#pragma once

typedef struct __BinTree BinTree;

struct __BinTree {
    int value;
    BinTree * left;
    BinTree * right;
};

BinTree * BinTree_new(int value);
void BinTree_free(BinTree * self);