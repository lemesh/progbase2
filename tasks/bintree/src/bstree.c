#include <stdio.h>
#include <assert.h>
#include <bstree.h>
#include <stdlib.h>

struct __BSTree{
    BSTree * root;
};

BSTree * BSTree_new(void){
    BSTree * self = malloc(sizeof(BSTree));
    self->root = NULL;
    return self;
}

void BSTree_free(BSTree * self){
    free(self);
    return;
}

static void insert(BinTree * node , int key){
    assert(node != NULL);
    if(key < node->value) {
        if (node->left) {
            insert(node->left, key);
        } else {
            node->left = BinTree_new(key);
        }
    } else {
        if (key > node->value) {
            if (node->right) {
                insert(node->right, key);
            } else {
                node->right = BinTree_new(key);
            }
        } else {
            assert(0 && "Error");
        }
    }
    return;
}

void BSTree_insert(BSTree * self, int key){
    if (self->root)
        insert(self->root, key);
    else
        self->root = BinTree_new(key);
    return;
}

static void clear(BinTree * node) {
    if(node->left) clear(node->left);
    if(node->right) clear(node->right);
    BinTree_free(node);
}

/**
 *  @brief remove all tree nodes and free their memory
 */
void BSTree_clear(BSTree * self) {
    if (self->root) clear(self->root);
    else return;
}


// extra

static void print(BinTree * node, int level){
    for(int i = 0 ; i < level; i++){
        putchar('.');
        putchar('.');
    }
    if(node == NULL){
        printf("(null)\n");
    }else{
        printf("%i\n", node->value);
    }
}

static void preOrder(BinTree * node, int i) {
    print(node, i);
    if (node->left == NULL && node->right == NULL) {
        return;
    }
    if (node->left) {
        preOrder(node->left, ++i);
    } else if(node->left == NULL) {
        print(node->left, ++i);
    }
    if (node->right) {
        preOrder(node->right, i);
    } else if(node->right == NULL) {
        print(node->right, i);
    }
}

void BSTree_printFormat(BSTree * self) {
    int i = 0;
    preOrder(self->root, i);
    return;
}

static void inOrder(BinTree * node){
    assert(node != NULL);
    if(node->left) inOrder(node->left);
    printf("%i ", node->value);
    if(node->right) inOrder(node->right);
}

void BSTree_printTraverse(BSTree * self){
    if(self->root){
        inOrder(self->root);
    }
    else {
        printf("(EMPTY)");
    }
}