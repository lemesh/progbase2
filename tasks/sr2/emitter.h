#pragma once
#include <progbase/events.h>

/*
    emits the sequence of events to process in handler
*/

typedef struct Emitter Emitter;

Emitter * Emitter_new(void);
void Emitter_free(Emitter * self);

void Emitter_onEvent(EventHandler * self, Event * event);