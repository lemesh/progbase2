#include <progbase/events.h>
#include "emitter.h"
#include "handler.h"

int main(int argc, char * argv[]) {
    EventSystem_init();

    EventSystem_addHandler(
            EventHandler_new(
                    Emitter_new(),
                    (DestructorFunction)Emitter_free,
                    Emitter_onEvent));

    EventSystem_addHandler(
            EventHandler_new(
                    Handler_new(),
                    (DestructorFunction)Handler_free,
                    Handler_onEvent));

    EventSystem_loop();
    EventSystem_cleanup();
    return 0;
}