#include "emitter.h"
#include "eventtypes.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#define __LEN(A) (sizeof((A)) / sizeof((A)[0]))
#define __DUP(S) ({ __typeof(S) * copy = malloc(sizeof(S)); *copy = S; copy; })

static void registerTimer(double totalMillis);

static void Event_checkAsserts(Event * event);
static void Emitter_checkResult(Emitter * emitter, const char * expected);

static void Command_free(Command * self);
static int Result_equals(Result * a, Result * b);
static void Result_fprintf(Result * self, FILE * stream);

static const ResultRequest ResultRequestEmpty = { 0 };

static char *__strdup (const char *s);

//
typedef struct X X;
struct X {
    enum {
        XCommand,
        XResultRequest,
        XResult,
        XError
    } type;
    Command command;
    ResultRequest resultRequest;
    Result result;
};

static void Emitter_initX(Emitter * self);

static void addX(Emitter * self, X x);
static void addXCom(Emitter * self, Command c);
static void addXErr(Emitter * self);
static void addXRes(Emitter * self, Result res);
static void addXReqRes(Emitter * self, ResultRequest req, Result res);

static void sendXCommands(EventHandler * self);
static void matchXResult(Emitter * emitter, Result * result);
static void matchXError(Emitter * emitter);
//
struct Emitter {
    X x[100];
    int xlength;
    int xindex;
};

void Emitter_onEvent(EventHandler * self, Event * event) {
    Emitter * emitter = self->data;
    Event_checkAsserts(event);
    switch (event->type) {
        case StartEventTypeId: {
            registerTimer(1000);
            sendXCommands(self);
            break;
        }
        case HandlerResultResponseEventId: {
            Result result = HandlerResultResponseEventId_data(event);
            matchXResult(emitter, &result);
            sendXCommands(self);
            break;
        }
        case HandlerErrorResponseEventId: {
            matchXError(emitter);
            sendXCommands(self);
            break;
        }
    }
}


//==============================================================================================
int Result_equals(Result * a, Result * b) { return a->value == b->value; }
static void Result_fprintf(Result * self, FILE * stream) { fprintf(stream, "%i", self->value); }

static Command commandOp(int op) { return (Command){ .operation = op }; }
static Command commandVal(int value) { return (Command){ .operation = 0, .value = value }; }
static void Command_free(Command * self) { free(self); }

static void Emitter_initX(Emitter * self) {
    addXCom(self, commandVal(2));
    addXCom(self, commandVal(5));
    addXCom(self, commandOp(OpSum));
    addXCom(self, commandVal(3));
    addXCom(self, commandOp(OpMult));
    addXRes(self, (Result){ 21 });
    addXCom(self, commandOp(OpDiv));
    addXErr(self);
}
//==============================================================================================

Emitter * Emitter_new(void) {
    Emitter * self = malloc(sizeof(Emitter));
    self->xlength = 0;
    self->xindex = 0;
    Emitter_initX(self);
    return self;
}
void Emitter_free(Emitter * self) { free(self); }

static void sendCommand(EventHandler * self, Command command) {
    EventSystem_emit(
            Event_new(
                    self,
                    EmitterCommandEventId,
                    __DUP(command),
                    (DestructorFunction)Command_free));
}
static void requestResult(EventHandler * self, ResultRequest request) {
    EventSystem_emit(
            Event_new(
                    self,
                    EmitterResultRequestEventId,
                    __DUP(request),
                    free));
}
static void Event_checkAsserts(Event * event) {
    assert(event && "check event is NULL");
    if (event->type != StartEventTypeId
        && event->type != UpdateEventTypeId
        && event->type != ExitEventTypeId) {
        assert(event->sender && "check event sender is NULL");
    }
}
static char *__strdup (const char *s) { return strcpy(malloc(strlen(s) + 1), s); }

// X

static void addX(Emitter * self, X x) {
    if (x.type == XResult) {
        assert(self->xlength > 0
               && self->x[self->xlength - 1].type == XResultRequest
               && "XResultRequest expected before XResult");
    }
    self->x[self->xlength++] = x;
}
static X createXCommand(Command x) { return (X){ XCommand, .command = x }; }
static X createXResultRequest(ResultRequest x) { return (X){ XResultRequest, .resultRequest = x }; }
static X createXResult(Result x) { return (X){ XResult, .result = x }; }
static X createXError(void) { return (X){ XError }; }

static void addXCom(Emitter * self, Command c) { addX(self, createXCommand(c)); }
static void addXErr(Emitter * self) { addX(self, createXError()); }
static void addXRes(Emitter * self, Result res) { addXReqRes(self, ResultRequestEmpty, res); }
static void addXReqRes(Emitter * self, ResultRequest req, Result res) {
    addX(self, createXResultRequest(req));
    addX(self, createXResult(res));
}

static void sendXCommands(EventHandler * self) {
    Emitter * e = self->data;
    for (; e->xindex < e->xlength; e->xindex++) {
        X x = e->x[e->xindex];
        if (x.type == XCommand) {
            sendCommand(self, x.command);
        } else if (x.type == XResultRequest) {
            requestResult(self, x.resultRequest);
        } else {
            return;
        }
    }
    EventSystem_exit();
}

static void errorResultReceivedButNotExpected(Result * result) {
    fprintf(stderr, "got result: '");
    Result_fprintf(result, stderr);
    fprintf(stderr, "'\n");
    assert(0 && "result received but not expected");
}

static void matchXResult(Emitter * emitter, Result * result) {
    if (emitter->xindex >= emitter->xlength) {
        errorResultReceivedButNotExpected(result);
    }
    X * xresult = &(emitter->x[emitter->xindex]);
    if (xresult->type != XResult) {
        errorResultReceivedButNotExpected(result);
    }
    int resultsEquals = Result_equals(&(xresult->result), result);
    if (!resultsEquals) {
        fprintf(stderr, "got result mismatch \nactual: '");
        Result_fprintf(result, stderr);
        fprintf(stderr, "'\nexpected: '");
        Result_fprintf(&xresult->result, stderr);
        fprintf(stderr, "'\n");
        assert(0 && "temp result missmatch");
    }
    emitter->xindex++;
}

static void errorErrorReceivedButNotExpected() {
    fprintf(stderr, "got unexpected error\n");
    assert(0 && "error received but not expected");
}

static void matchXError(Emitter * emitter) {
    if (emitter->xindex >= emitter->xlength) {
        errorErrorReceivedButNotExpected();
    }
    X * xresult = &(emitter->x[emitter->xindex]);
    if (xresult->type != XError) {
        errorErrorReceivedButNotExpected();
    }
    emitter->xindex++;
}

// Timer

typedef struct Timer Timer;
static Timer * Timer_new(double total);
static void Timer_onEvent(EventHandler * self, Event * event);
static void registerTimer(double totalMillis) {
    EventSystem_addHandler(
            EventHandler_new(
                    Timer_new(totalMillis),
                    (DestructorFunction)free,
                    Timer_onEvent));
}
struct Timer {
    double millis;
    double totalMillis;
};
static Timer * Timer_new(double total) {
    Timer * self = malloc(sizeof(Timer));
    self->millis = 0;
    self->totalMillis = total;
    return self;
}
static void Timer_onEvent(EventHandler * self, Event * event) {
    Timer * timer = self->data;
    switch (event->type) {
        case UpdateEventTypeId: {
            double * elapsed = event->data;
            timer->millis += *elapsed;
            if (timer->millis > timer->totalMillis) {
                assert(0 && "timeout");
            }
            break;
        }
    }
}