#include "handler.h"
#include "eventtypes.h"
#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>

bool isEmpty(Handler * self);
void push(Handler * self, int data);
int size(Handler * self);
int pop(Handler * self);
int top(Handler * self);

struct Handler {
    char _;  // @todo you can modify this
    int * array;
    int top;
};

Handler * Handler_new(void) {
    Handler * self = malloc(sizeof(Handler));
    self->array = malloc(sizeof(int) * 100);
    self->top = -1;
    return self;
}
void Handler_free(Handler * self) {
    //  @todo cleanup
    free(self->array);
    free(self);
}

void Handler_onEvent(EventHandler * self, Event * event) {
    Handler * handler = self->data;
    switch (event->type) {
        case EmitterCommandEventId: {
            Command * c = event->data;
            switch (c->operation) {
                case OpNone: {
                    push(handler, *(int *)event->data);
                    break;
                }
                case OpSum: {
                    push(handler, handler->array[handler->top - 2] + handler->array[handler->top - 1]);
                }
                case OpMult: {
                    push(handler, handler->array[handler->top - 2] * handler->array[handler->top - 1]);
                }
                case OpDiv: {
                    push(handler, handler->array[handler->top - 1] + handler->array[handler->top - 2]);
                }
            }
        }
        case EmitterResultRequestEventId: {
            struct Result r = {
                .value = top(handler);
            };
            if (r.value == 404) EventSystem_emit(Event_new(self, EmitterResultRequestEventId, NULL, NULL));
            else EventSystem_emit(Event_new(self, EmitterResultRequestEventId, &data, NULL));
            break;
        }
        case HandlerResultResponseEventId: {

        }
        default:
            break;
    }
}

bool isEmpty(Handler * self) {
    if (self->top == -1) return false;
    return true;
}

void push(Handler * self, int data) {
    if (size(self) >= 100) return;
    if (isEmpty(self)) {
        self->top = 0;
    } else {
        self->top++;
    }
    self->array[self->top] = data;
}

int size(Handler * self) {
    if (isEmpty(self)) return 0;
    return self->top;
}

int pop(Handler * self) {
    return self->array[self->top--];
}

int top(Handler * self) {
    if (isEmpty(self)) return 404;
    return self->array[self->top];
}