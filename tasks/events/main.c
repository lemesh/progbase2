#include <stdio.h>
#include <progbase/events.h>
#include <progbase/console.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

typedef struct __File File;
typedef struct __Sum Sum;

struct __File {
    FILE * f;
};

struct __Sum {
    int sum;
    int N;
    int K;
    int N_count;
    int K_count;
};

enum {
    HexadecimalEvent,
    HexadecimalBigEvent,
    HexadecimalHugeEvent,
};

void Logger_update(EventHandler * self, Event * event);
void Hexadecimal_search(EventHandler * self, Event * event);
void HexadecimalSum_Event(EventHandler * self, Event * event);

File * File_new(void);
void File_free(File * self);

Sum * Sum_new(void);
void Sum_free(Sum * self);

int main(void) {
    srand(time(0));
    EventSystem_init();

    EventSystem_addHandler(EventHandler_new(NULL, NULL, Logger_update));
    EventSystem_addHandler(EventHandler_new(File_new(), (DestructorFunction)File_free, Hexadecimal_search));
    EventSystem_addHandler(EventHandler_new(Sum_new(), (DestructorFunction)Sum_free, HexadecimalSum_Event));

    EventSystem_loop();
    EventSystem_cleanup();
    printf("\n\n\nBye!\n");
    return 0;
}

void Logger_update(EventHandler * self, Event * event) {
    switch (event->type) {
        case StartEventTypeId: {
            Console_clear();
            printf("Hi, you are Welcome!\n\n");
            break;
        }
        case ExitEventTypeId: {
            break;
        }
        case HexadecimalBigEvent: {
            Console_setCursorPosition(9, 0);
            printf("MORE = %i   ", *(int *)event->data);
            break;
        }
        case HexadecimalHugeEvent: {
            Console_setCursorPosition(10, 0);
            printf("CLEAR! = %i   ", *(int *)event->data);
            break;
        }
        default:
            break;
    }
}

void Hexadecimal_search(EventHandler * self, Event * event) {
    File * file = self->data;
    switch (event->type) {
        case StartEventTypeId: {
            printf("Обробник 1 по події старту програми зчитує довільний текстовий файл і на зчитування кожної\n"
            "шістнадцяткової цифри генерує повідомлення, що містить цифру як дані. Обробник 2 слухає такі події і\n"
            "сумує цифри у число, якщо сума перевищує деяке N - генерується спеціальна подія, що містить цю суму,\n"
            "якщо сума перевищує деяке K (> N) - генерується подія іншого типу, а сама сума обнуляється. Створити два\n"
            "обробника типу 'Обробник 2', кожен з який при створенні отримує різні значення N та K..\n\n");
            if (!file->f) {
                EventSystem_exit();
            }
        }
        case UpdateEventTypeId: {
            if (file->f) {
                char c;
                while (fscanf(file->f, "%c", &c) != EOF) {
                    if (strchr("abcdefABCDEF0123456789", c)) {
                        char * hex = malloc(sizeof(char));
                        *hex = c;
                        EventSystem_emit(Event_new(self, HexadecimalEvent, hex, free));
                    }
                }
            }
        }
        case ExitEventTypeId: {
            EventSystem_removeHandler(self);
            EventSystem_exit();
        }
        default:
            break;
    }
}

void HexadecimalSum_Event(EventHandler * self, Event * event) {
    switch (event->type) {
        case HexadecimalEvent: {
            Sum * sum = self->data;
            char c = *(char *) event->data;
            int value = -1;
            if (strchr("ABCDEF", c)) {
                value = c - 'A' + 10;
            }
            if (strchr("abcdef", c)) {
                value = c - 'a' + 10;
            }
            if (strchr("0123456789", c)) {
                value = c - '0';
            }
            sum->sum +=value;
            if (sum->sum >= sum->N) {
                sum->N_count++;
                EventSystem_emit(Event_new(self, HexadecimalBigEvent, &sum->N_count, NULL));
            }
            if (sum->sum >= sum->K) {
                sum->sum = 0;
                sum->K_count++;
                EventSystem_emit(Event_new(self, HexadecimalHugeEvent, &sum->K_count, NULL));
            }
        }
        case ExitEventTypeId: {
            EventSystem_removeHandler(self);
            EventSystem_exit();
        }
        default:
            break;
    }
}

File * File_new(void) {
    File * self = malloc(sizeof(File));
    self->f = fopen("../events.txt", "r");
    return self;
}

void File_free(File * self) {
    if(self->f) fclose(self->f);
    free(self);
}

Sum * Sum_new(void) {
    Sum * self = malloc(sizeof(Sum));
    self->sum = 0;
    self->N = rand() % 200;
    self->K = rand() % 200 + self->N + 1;
    self->K_count = 0;
    self->N_count = 0;
    return self;
}

void Sum_free(Sum * self) {
    free(self);
}