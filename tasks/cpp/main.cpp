#include <iostream>
#include <cstdio>
#include <vector>
#include <ThreeDimensionDot.h>
#include <string>
#include <cstdlib>

int main() {
    system("clear");
    std::cout << "Hi, it's task `C++`. I think next task would be more interesting.\n\n";
    std::cout << "Please, choose a task:\n";
    std::cout << "1. Print all objects.\n";
    std::cout << "2. Add new object.\n";
    std::cout << "3. Print all objects, which vector length less than some L.\n";
    std::cout << "4. Exit the programm.\n\n";
    std::vector<ThreeDimensionDot*> dots;
    int menu = 0;
    bool exit = false;
    do {
        std::cout << "<<<<< Please, make your choice -> ";
        std::cin >> menu;
        std::cout << std::endl;
        switch (menu) {
            case 1: {
                if (!dots.empty()) {
                    for (auto dot : dots) {
                        dot->Print();
                    }
                } else {
                    std::cout << "<<<<< It's empty. >>>>>\n\n";
                }
                break;
            }
            case 2: {
                int x, y, z = 0;
                std::string name;
                std::cout << "Type x -> ";
                std::cin >> x;
                std::cout << std::endl;
                std::cout << "Type y -> ";
                std::cin >> y;
                std::cout << std::endl;
                std::cout << "Type z -> ";
                std::cin >> z;
                std::cout << std::endl;
                std::cout << "Enter the name of your object -> ";
                std::cin.get();
                std::getline(std::cin, name);
                std::cout << std::endl;
                if (!name.empty()) {
                    dots.push_back(new ThreeDimensionDot(name, x, y, z));
                }
                break;
            }
            case 3: {
                double L = 0;
                std::cout << "Enter L - max length, which allow to print object -> ";
                std::cin >> L;
                std::cout << std::endl;
                for (auto dot : dots) {
                    if (dot->GetVectorLength() <= L) {
                        dot->Print();
                    }
                }
                break;
            }
            case 4: {
                if (!dots.empty()) {
                    for (auto dot : dots) {
                        delete dot;

                    }
                }
                exit = true;
                break;
            }
            default:
                system("clear");
                std::cout << "Type a right point of menu.\n\n";
                break;

        }
    } while (!exit);
    return 0;
}