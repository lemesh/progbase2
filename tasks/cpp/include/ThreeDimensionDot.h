#pragma once

#include <string>

class ThreeDimensionDot {
public:
    ThreeDimensionDot(std::string, int, int, int);

    double GetVectorLength() const;
    void Print() const;

private:
    std::string Name;

    int x0;
    int y0;
    int z0;
};