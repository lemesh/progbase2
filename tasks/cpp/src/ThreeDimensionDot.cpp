#include <ThreeDimensionDot.h>
#include <iostream>
#include <math.h>

ThreeDimensionDot::ThreeDimensionDot(std::string name, int x, int y, int z) {
    Name = name;

    x0 = x;
    y0 = y;
    z0 = z;
}

double ThreeDimensionDot::GetVectorLength() const {
    return sqrt(pow(this->x0, 2) + pow(this->y0, 2) + pow(this->z0, 2));
}


void ThreeDimensionDot::Print() const {
    std::cout << this->Name << " - coordinates <" << this->x0 << ", " << this->y0 << ", " << this->z0 << ">\n\n";
}