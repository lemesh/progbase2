#include <stdio.h>
#include <stdlib.h>

#include <List.h>
#include <check.h>
#include <Lexer.h>

START_TEST (split_empty_empty)
    {
        List * tokens = List_new();
        int status = Lexer_splitTokens("", tokens);
        ck_assert_int_eq(status, 0);
        ck_assert_int_eq(List_count(tokens), 0);
        List_free(tokens);
    }
END_TEST

START_TEST (split_oneNumber_oneNumberToken)
    {
        List * tokens = List_new();
        int status = Lexer_splitTokens("13", tokens);
        ck_assert_int_eq(status, 0);
        ck_assert_int_eq(List_count(tokens), 1);
        Token * firstToken = (Token *)List_at(tokens, 0);
        Token * testToken = Token_new(TokenType_NUMBER, "13");
        ck_assert(Token_equals(firstToken, testToken));
        Lexer_clearTokens(tokens);
        List_free(tokens);
    }
END_TEST

START_TEST (split_onePow_onePowToken)
    {
        List * tokens = List_new();
        int status = Lexer_splitTokens("&&", tokens);
        ck_assert_int_eq(status, 0);
        ck_assert_int_eq(List_count(tokens), 1);
        Token * firstToken = (Token *)List_at(tokens, 0);
        Token * testToken = Token_new(TokenType_AND, "&&");
        ck_assert(Token_equals(firstToken, testToken));
        Lexer_clearTokens(tokens);
        List_free(tokens);
    }
END_TEST


START_TEST (equal_token_Eq)
    {
        Token * new = Token_new(TokenType_UNDEF, "fkds");
        Token * other = Token_new(TokenType_UNDEF, "fkds");
        ck_assert_int_eq(Token_equals(new, other), 1);
        Token_free(new);
        Token_free(other);
    }
END_TEST

START_TEST (equal_empty_Eq)
    {
        Token * new = Token_new(TokenType_UNDEF, "");
        Token * other = Token_new(TokenType_UNDEF, "");
        ck_assert_int_eq(Token_equals(new, other), 1);
        Token_free(new);
        Token_free(other);
    }
END_TEST

START_TEST (atAdd_int_Equal)
    {
        List * new = List_new();
        List_add(new, 3);
        ck_assert_int_eq(List_at(new, List_count(new) - 1), 3);
        List_add(new, 3);
        ck_assert_int_ne(List_at(new, List_count(new) - 1), 4);
        List_free(new);
    }
END_TEST

START_TEST (count_Empty)
    {
        List * new = List_new();
        ck_assert_int_eq(List_count(new), 0);
        List_add(new, 3);
        ck_assert_int_eq(List_count(new), 1);
        List_add(new, 4);
        ck_assert_int_ne(List_count(new), 3);
        List_free(new);
    }
END_TEST

START_TEST (remove_int_Equal)
    {
        List * new = List_new();
        List_add(new, 1);
        List_add(new, 2);
        List_add(new, 3);
        List_add(new, 4);
        List_add(new, 5);
        ck_assert_int_eq(List_removeAt(new, 3), 4);
        ck_assert_int_eq(List_removeAt(new, 3), 5);
        ck_assert_int_ne(List_removeAt(new, 0), 2);
        ck_assert_int_eq(List_removeAt(new, 0), 2);
        List_free(new);
    }
END_TEST

START_TEST (insert_int_Equal)
    {
        List * new = List_new();
        List_add(new, 1);
        List_add(new, 2);
        List_add(new, 3);
        List_add(new, 4);
        List_add(new, 5);
        List_add(new, 6);
        List_insert(new, 80, 4);
        ck_assert_int_eq(List_at(new, 4), 80);
        ck_assert_int_eq(List_at(new, 5), 5);
        List_insert(new, 100, List_count(new) - 1);
        ck_assert_int_eq(List_at(new, List_count(new) - 2), 100);
        ck_assert_int_eq(List_at(new, List_count(new) - 1), 6);
        List_free(new);
    }
END_TEST

Suite *test_suite(void);

int main(void) {
    Suite *s = test_suite();
    SRunner *sr = srunner_create(s);
    srunner_set_fork_status(sr, CK_NOFORK);  // important for debugging!

    srunner_run_all(sr, CK_VERBOSE);

    int num_tests_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return num_tests_failed;
}

Suite *test_suite(void) {
    Suite *s = suite_create("List");
    TCase *tc_sample;

    tc_sample = tcase_create("TestCases");
    tcase_add_test(tc_sample, atAdd_int_Equal);
    tcase_add_test(tc_sample, count_Empty);
    tcase_add_test(tc_sample, remove_int_Equal);
    tcase_add_test(tc_sample, insert_int_Equal);
    tcase_add_test(tc_sample, equal_token_Eq);
    tcase_add_test(tc_sample, equal_empty_Eq);
    tcase_add_test(tc_sample, split_empty_empty);
    tcase_add_test(tc_sample, split_oneNumber_oneNumberToken);
    tcase_add_test(tc_sample, split_onePow_onePowToken);

    suite_add_tcase(s, tc_sample);
    return s;
}
