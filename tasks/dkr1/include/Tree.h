#pragma once

#include <List.h>

typedef struct __Tree Tree;
struct __Tree {
    void * value;
    List * children;
};

Tree * Tree_newDegree(void * value, size_t degree, Memory * memory);
Tree * Tree_new(void * value, Memory * memory);
void Tree_free(Tree * self);
void Tree_clear(Tree * self);