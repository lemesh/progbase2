#pragma once

#include <stdlib.h>
#include <stdbool.h>
#include <List.h>
#include <Memory.h>

typedef struct __BSTree BSTree;

BSTree * BSTree_new(Memory * memory);
void BSTree_free(BSTree * self);

void BSTree_insert(BSTree * self, int key, void * value, Memory * memory);
bool BSTree_lookup(BSTree * self, int key);
void * BSTree_search(BSTree * self, int key);
void * BSTree_delete(BSTree * self, int key);

void BSTree_clear(BSTree * self);
size_t BSTree_count(BSTree * self);

void BSTree_keys(BSTree * self, List * keys);
void BSTree_values(BSTree * self, List * values);