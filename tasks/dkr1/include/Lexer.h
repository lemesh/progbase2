#pragma once

#include <stdbool.h>
#include <List.h>
#include <stdio.h>
#include <Memory.h>

typedef enum {
    TokenType_PLUS,
    TokenType_MINUS,
    TokenType_MULT,
    TokenType_DIV,
    TokenType_NUMBER,
    TokenType_VAR,
    TokenType_EQUAL,
    TokenType_SQRT,
    TokenType_POW,
    TokenType_STRLEN,
    TokenType_SUBSTR,
    TokenType_PUSH,
    TokenType_COUNT,
    TokenType_AT,
    TokenType_PRINT,
    TokenType_SCAN,
    TokenType_STRTONUM,
    TokenType_NUMTOSTR,
    TokenType_IF,
    TokenType_ELSE,
    TokenType_LOOP,
    TokenType_LOP,
    TokenType_ROP,
    TokenType_UNDEF,
    TokenType_ASSIGN,
    TokenType_CLOP,
    TokenType_CROP,
    TokenType_SEMICOLON,
    TokenType_WS,
    TokenType_QUOTE,
    TokenType_DQUOTE,
    TokenType_FLOP,
    TokenType_FROP,
    TokenType_DOT,
    TokenType_COMMA,
    TokenType_OR,
    TokenType_AND,
    TokenType_NEQUAL,
    TokenType_ID,
    TokenType_STRING,
    TokenType_MOREQ,
    TokenType_MORE,
    TokenType_LESSQ,
    TokenType_LESS,
    TokenType_MOD,
    TokenType_NOT
} TokenType;

typedef struct __Token Token;

struct __Token {
    TokenType type;
    char * lexeme;
};

Token * Token_new(TokenType type, const char * name, Memory * memory);
void Token_free(Token * self);

bool Token_equals(Token * self, Token * other);

char * TokenType_toString(TokenType type);

int Lexer_splitTokens(const char * input, List * tokens, Memory * memory);
void Lexer_clearTokens(List * tokens);

void Lexer_printTokens(List * tokens, Memory * memory);
void Lexer_writeToFile(List * tokens, Memory * memory);