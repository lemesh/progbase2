#pragma once

typedef struct __Memory Memory;

Memory * Memory_new(void);
void Memory_free(Memory * self);
void Memory_add(Memory * self, void * address);