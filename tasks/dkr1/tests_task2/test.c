#include <stdio.h>
#include <stdlib.h>

#include <List.h>
#include <check.h>
#include <Lexer.h>
#include <Parser.h>
#include <File.h>
#include <Ast.h>

START_TEST(parser_make_tree)
    {
        Memory * memory = Memory_new();
        long length = getFileSize("../test.svag") + 1;
        char * buff = calloc(length, sizeof(char));
        readFileToBuffer("../test.svag", buff, length);
        List * list = List_new(memory);
        Lexer_splitTokens(buff, list, memory);
        Tree * root = Parser_buildNewAstTree(list, memory);
        AstTree_prettyPrint(root, memory);
        Memory_free(memory);
    }
END_TEST

START_TEST(ast_new_makeAndDelete)
    {
        Memory * memory = Memory_new();
        AstNode * self = AstNode_new(AstNodeType_PROGRAM, "PROGRAM", memory);
        Memory_free(memory);
    }
END_TEST

Suite *test_suite(void);

int main(void) {
    Suite *s = test_suite();
    SRunner *sr = srunner_create(s);
    srunner_set_fork_status(sr, CK_NOFORK);  // important for debugging!

    srunner_run_all(sr, CK_VERBOSE);

    int num_tests_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return num_tests_failed;
}

Suite *test_suite(void) {
    Suite *s = suite_create("TEST");
    TCase *tc_sample;

    tc_sample = tcase_create("TestCases");
    tcase_add_test(tc_sample, parser_make_tree);
    tcase_add_test(tc_sample, ast_new_makeAndDelete);

    suite_add_tcase(s, tc_sample);
    return s;
}