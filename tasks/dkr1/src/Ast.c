#include <Ast.h>
#include <stdio.h>
#include <string.h>
#include <StringBuffer.h>

static char * str_append(const char * str, const char * append);
static void PrintPretty(Tree * node, const char * indent, int root, int last);

AstNode * AstNode_new(AstNodeType type, const char * name, Memory * memory) {
    AstNode * self = malloc(sizeof(AstNode));
    Memory_add(memory, (void *)self);
    self->type = type;
    self->name = name;
    return self;
}

void AstNode_free(AstNode * self) {
    free(self);
}

void AstNode_freeName(AstNode * self) {
    if (self->name == NULL) {
        //free(self);
        return;
    }
    free((void *)self->name);
}

void AstTree_prettyPrint(Tree * astTree, Memory * memory) {
    char * indent = strdup("", memory);
    PrintPretty(astTree, indent, 1, 1);
    free((void *)indent);
}

static char * str_append(const char * str, const char * append) {
    size_t newLen = strlen(str) + strlen(append) + 1;
    char * buf = malloc(sizeof(char) * newLen);
    buf[0] = '\0';
    sprintf(buf, "%s%s", str, append);
    return buf;
}

static void PrintPretty(Tree * node, const char * indent, int root, int last) {
    if (node == NULL)
        return;
    printf("%s", indent);
    char * newIndent = NULL;
    if (last) {
        if (!root) {
            printf("└╴");
            newIndent = str_append(indent, "  ");
        } else {
            newIndent = str_append(indent, "");
        }
    } else {
        printf("├");
        newIndent = str_append(indent, "| ");
    }
    AstNode * astNode = NULL;
    if(node->value != NULL) {
        astNode = node->value;
    }
    printf("%s\n", astNode->name);
    List * children = node->children;
    size_t count = List_count(children);
    for (int i = 0; i < count; i++) {
        void * child = List_at(children, (size_t)i);
        PrintPretty(child, newIndent, 0, i == count - 1);
    }
    free((void *)newIndent);
}