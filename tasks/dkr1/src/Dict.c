#include <Dict.h>
#include <string.h>
#include <BsTree.h>

typedef struct {
    char * key;
    void * value;
} KeyValuePair;

KeyValuePair * Kvp_new(char * key, void * value, Memory * memory) {
    KeyValuePair * self = malloc(sizeof(KeyValuePair));
    Memory_add(memory, self);
    self->key = key;
    self->value = value;
    return self;
}

void Kvp_free(KeyValuePair * self) {
    free(self);
}

struct __Dict {
    BSTree * pairs;
};

Dict * Dict_new(Memory * memory) {
    Dict * self = malloc(sizeof(Dict));
    Memory_add(memory, self);
    self->pairs = BSTree_new(memory);
    return self;
}

void Dict_free(Dict * self) {
    BSTree_free(self->pairs);
    free(self);
}

void Dict_add(Dict * self, char * key, void * value, Memory * memory) {
    KeyValuePair * pair = Kvp_new(key, value, memory);
    int intKey = 0;
    BSTree_insert(self->pairs, intKey,pair, memory);
}

bool Dict_contains(Dict * self, char * key) {
    for (int i = 0; i < BSTree_count(self->pairs); i++) {
        KeyValuePair * pair = List_at(self->pairs, i);
        if (!strcmp(pair->key, key)) return true;
    }
    return false;
}

void * Dict_get(Dict * self, char * key) {
    for (int i = 0; i < List_count(self->pairs); i++) {
        KeyValuePair * pair = List_at(self->pairs, i);
        if (!strcmp(pair->key, key)) return pair->value;
    }
    return NULL;
}

void * Dict_set(Dict * self, char * key, void * value) {
    for (int i = 0; i < List_count(self->pairs); i++) {
        KeyValuePair * pair = List_at(self->pairs, i);
        if (!strcmp(pair->key, key)) {
            void * oldValue = pair->value;
            pair->value = value;
            return oldValue;
        }
    }
    return NULL;
}

void * Dict_remove(Dict * self, char * key) {
    int index = -1;
    for (int i = 0; i < List_count(self->pairs); i++) {
        KeyValuePair * pair = List_at(self->pairs, i);
        if (!strcmp(pair->key, key)) {
            index = i;
            break;
        }
    }
    if (index > -1) {
        KeyValuePair * removedPair = List_removeAt(self->pairs, index);
        void * oldValue = removedPair->value;
        Kvp_free(removedPair);
        return oldValue;
    }
    return NULL;
}

void Dict_clear(Dict * self) {
    for (int i = List_count(self->pairs) - 1; i >= 0; i--) {
        KeyValuePair * pair = List_at(self->pairs, i);
        (void)List_removeAt(self->pairs, i);
    }
}

size_t Dict_count(Dict * self) {
    return List_count(self->pairs);
}

void Dict_keys(Dict * self, List * keys) {
    for (int i = 0; i < List_count(self->pairs); ++i) {
        KeyValuePair * pair = List_at(self->pairs, i);
        List_add(keys, pair->key);
    }
}

void Dict_values(Dict * self, List * values) {
    for (int i = 0; i < List_count(self->pairs); ++i) {
        KeyValuePair * pair = List_at(self->pairs, i);
        List_add(values, pair->value);
    }
}