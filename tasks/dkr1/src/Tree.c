#include <Tree.h>
#include <Ast.h>

Tree * Tree_new(void * value, Memory * memory) {
    Tree * self = malloc(sizeof(Tree));
    Memory_add(memory, self);
    self->value = value;
    self->children = List_new(memory);
    return self;
}

Tree * Tree_newDegree(void * value, size_t degree, Memory * memory) {
    Tree * self = malloc(sizeof(Tree));
    Memory_add(memory, self);
    self->value = value;
    self->children = List_newCapacity(degree, memory);
    return self;
}

void Tree_free(Tree * self) {
    List_free(self->children);
    free(self);
}

//void Tree_clear(Tree * self) {
//    AstNode_freeName(self->value);
//    List_freeTree(self->children);
//    free(self->value);
//    free(self);
//}

void Tree_clear(Tree * self){
    if(self == NULL) return;
    if(self->children == NULL) return;

    size_t size = List_count(self->children);
    for(size_t i = 0 ; i < size; i++){
        Tree * tree = List_at(self->children, i);
        Tree_clear(tree);
    }

    AstNode_free(self->value);
    //List_freeAst(self->children);
    List_free(self->children);
    free(self);
}