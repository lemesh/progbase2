#include <List.h>
#include <Parser.h>
#include <stdio.h>
#include <Lexer.h>
#include <StringBuffer.h>
#include <Ast.h>

typedef struct {
    Iterator * tokens;
    Iterator * tokensEnd;
    char * error;
    int level;
} Parser;

typedef Tree * (*GrammarRule)(Parser * parser);

static bool eoi(Parser * self);
static bool ebnf_multiple(Parser * parser, List * nodes, GrammarRule rule, Memory * memory);
static Tree * ebnf_select (Parser * parser, GrammarRule rules[], size_t rulesLen, Memory * memory);
static Tree * ebnf_selectLexemes (Parser * parser, TokenType types[], size_t typesLen, Memory * memory);
static Tree * ebnf_ap_main_rule (Parser * parser, GrammarRule next, GrammarRule ap);
static Tree * ebnf_ap_recursive_rule (Parser * parser,
                                    TokenType types[],
                                    size_t typesLen,
                                    GrammarRule next,
                                    GrammarRule ap, Memory * memory);

static Tree * accept (Parser * self, TokenType tokenType, Memory * memory);
static Tree * expect (Parser * parser, TokenType tokenType, Memory * memory);

static Tree * prog (Parser * parser, Memory * memory);
static Tree * var_decl (Parser * parser, Memory * memory);
static Tree * st (Parser * parser, Memory * memory);
static Tree * ID (Parser * parser, Memory * memory);
static Tree * expr (Parser * parser, Memory * memory);
static Tree * expr_st (Parser * parser, Memory * memory);
static Tree * block_st (Parser * parser, Memory * memory);
static Tree * select_st (Parser * parser, Memory * memory);
static Tree * iter_st (Parser * parser, Memory * memory);
static Tree * assign (Parser * parser, Memory * memory);
static Tree * assign_ap (Parser * parser, Memory * memory);
static Tree * log_or (Parser * parser, Memory * memory);
static Tree * log_or_ap (Parser * parser, Memory * memory);
static Tree * log_and (Parser * parser, Memory * memory);
static Tree * log_and_ap (Parser * parser, Memory * memory);
static Tree * eq (Parser * parser, Memory * memory);
static Tree * eq_ap (Parser * parser, Memory * memory);
static Tree * rel (Parser * parser, Memory * memory);
static Tree * rel_ap (Parser * parser, Memory * memory);
static Tree * add (Parser * parser, Memory * memory);
static Tree * add_ap (Parser * parser, Memory * memory);
static Tree * mult (Parser * parser, Memory * memory);
static Tree * mult_ap (Parser * parser, Memory * memory);
static Tree * unary (Parser * parser, Memory * memory);
static Tree * primary (Parser * parser, Memory * memory);
static Tree * NUMBER (Parser * parser, Memory * memory);
static Tree * STRING (Parser * parser, Memory * memory);
static Tree * var_or_call (Parser * parser, Memory * memory);
static Tree * parentheses (Parser * parser, Memory * memory);
static Tree * fn_call (Parser * parser, Memory * memory);
static Tree * arg_list (Parser * parser, Memory * memory);
static Tree * ID_expect (Parser * parser, Memory * memory);
static void trace(Parser * parser, const char * fn);
static void traceExpect(Parser * parser, TokenType expectedType);
static void traceLevel(int level);
static void Parser_decLevel(Parser ** parserPtr);
static AstNodeType TokenType_toAstNodeType(TokenType type);

static AstNodeType TokenType_toAstNodeType(TokenType type) {
    switch (type) {
        case TokenType_ASSIGN:
            return AstNodeType_ASSIGN;
        case TokenType_PLUS:
            return AstNodeType_ADD;
        case TokenType_MINUS:
            return AstNodeType_SUB;
        case TokenType_MULT:
            return AstNodeType_MUL;
        case TokenType_DIV:
            return AstNodeType_DIV;
        case TokenType_MOD:
            return AstNodeType_MOD;
        case TokenType_EQUAL:
            return AstNodeType_EQ;
        case TokenType_NEQUAL:
            return AstNodeType_NEQ;
        case TokenType_NOT:
            return AstNodeType_NOT;
        case TokenType_MORE:
            return AstNodeType_GT;
        case TokenType_LESS:
            return AstNodeType_LT;
        case TokenType_MOREQ:
            return AstNodeType_GTEQ;
        case TokenType_LESSQ:
            return AstNodeType_LTEQ;
        case TokenType_AND:
            return AstNodeType_AND;
        case TokenType_OR:
            return AstNodeType_OR;
        case TokenType_ID:
            return AstNodeType_ID;
        case TokenType_NUMBER:
            return AstNodeType_NUMBER;
        case TokenType_STRING:
            return AstNodeType_STRING;
        default:
            return AstNodeType_UNKNOWN;
    }
}

Tree * Parser_buildNewAstTree(List * tokens, Memory * memory) {
    Parser parser = {
            .tokens = List_getNewBeginIterator(tokens, memory),
            .tokensEnd = List_getNewEndIterator(tokens, memory),
            .error = NULL,
            .level = -1
    };
    Tree * progNode = prog(&parser, memory);
    if (parser.error) {
        fprintf(stderr, ">>>>> ERROR: %s\n", parser.error);
        free((void *)parser.error);
        Iterator_free(parser.tokens);
        Iterator_free(parser.tokensEnd);
        Tree_clear(progNode);
        return NULL;
    } else {
        if (!Iterator_equals(parser.tokens, parser.tokensEnd)) {
            Token * token = Iterator_value(parser.tokens);
            fprintf(stderr, ">>>>> ERROR: unexpected token '%s'.\n", TokenType_toString(token->type));
        }
    }
    Iterator_free(parser.tokens);
    Iterator_free(parser.tokensEnd);
    return progNode;
}

//bool Parser_match(List * tokens) {
//    Parser parser = {
//            .tokens = List_getNewBeginIterator(tokens),
//            .tokensEnd = List_getNewEndIterator(tokens),
//            .error = NULL,
//            .level = -1
//    };
//    bool match = prog(&parser);
//    if (parser.error) {
//        fprintf(stderr, ">>>>> ERROR: %s\n", parser.error);
//        free((void *)parser.error);
//        Iterator_free(parser.tokens);
//        Iterator_free(parser.tokensEnd);
//        return false;
//    } else {
//        if (!Iterator_equals(parser.tokens, parser.tokensEnd)) {
//            Token * token = Iterator_value(parser.tokens);
//            fprintf(stderr, ">>>>> ERROR: unexpected token '%s'.\n", TokenType_toString(token->type));
//        }
//    }
//    Iterator_free(parser.tokens);
//    Iterator_free(parser.tokensEnd);
//    return match;
//}


#define TRACE_CALL() \
    parser->level++; \
    Parser * parserPtr __attribute__((cleanup(Parser_decLevel))) = parser; \
    trace(parser, __func__);

static void Parser_decLevel(Parser ** parserPtr) {
    (*parserPtr)->level--;
}

static void traceLevel(int level) {
    for (int i = 0; i < level; ++i) {
        putchar('.');
        putchar('.');
    }
}

static void trace(Parser * parser, const char * fn) {
    traceLevel(parser->level);
    if (eoi(parser)) {
        printf("%s: <EOI>\n", fn);
        return;
    }
    Token * token = Iterator_value(parser->tokens);
    TokenType type = token->type;
    switch (type) {
        case TokenType_ID: case TokenType_NUMBER: case TokenType_STRING: {
            printf("%s: <%s,%s>\n", fn, TokenType_toString(type), token->lexeme);
            break;
        }
        default: {
            printf("%s: <%s>\n", fn, TokenType_toString(type));
            break;
        }
    }
}

#define TRACE_EXPECT(TYPE) \
    traceExpect(parser, TYPE);

static void traceExpect(Parser * parser, TokenType expectedType) {
    traceLevel(parser->level);
    printf("<%s>\n", TokenType_toString(expectedType));
}

static bool eoi(Parser * self) {
    return Iterator_equals(self->tokens, self->tokensEnd);
}

static Tree * accept (Parser * self, TokenType tokenType, Memory * memory) {
    if (eoi(self)) return NULL;
    Token * token = Iterator_value(self->tokens);
    if (token->type == tokenType) {
        AstNodeType astType = TokenType_toAstNodeType(tokenType);
        char * astName = strdup(token->lexeme, memory);
        AstNode * node = AstNode_new(astType, astName, memory);
        Tree * tree = Tree_new(node, memory);
        Iterator_next(self->tokens);
        return tree;
    }
    return NULL;
}

static Tree * expect (Parser * parser, TokenType tokenType, Memory * memory) {
    Tree * tree = accept(parser, tokenType, memory);
    if (tree != NULL) {
        TRACE_EXPECT(tokenType);
        return tree;
    }
    char * currentTokenType = eoi(parser) ? "end-of-input"
                                        : TokenType_toString(((Token *)Iterator_value(parser->tokens))->type);
    StringBuffer * sb = StringBuffer_new(memory);
    StringBuffer_appendFormat(sb, "expected '%s' got '%s'.\n", (const char *)TokenType_toString(tokenType), currentTokenType);
    parser->error = StringBuffer_toNewString(sb, memory);
    StringBuffer_free(sb);
    return NULL;
}

static bool ebnf_multiple (Parser * parser, List * nodes, GrammarRule rule, Memory * memory) {
    Tree * node = NULL;
    while ((node = rule(parser)) && !parser->error) {
        List_add(nodes, node);
    }
    return parser->error == NULL ? true : false;
}

static Tree * ebnf_select (Parser * parser, GrammarRule rules[], size_t rulesLen, Memory * memory) {
    Tree * node = NULL;
    for (int i = 0; i < rulesLen && !node; ++i) {
        GrammarRule rule = rules[i];
        node = rule(parser);
        if (parser->error) return NULL;
    }
    return node;
}

static Tree * ebnf_selectLexemes (Parser * parser, TokenType types[], size_t typesLen, Memory * memory) {
    Tree * node = NULL;
    for (int i = 0; i < typesLen && !node; ++i) {
        node = accept(parser, types[i], memory);
    }
    return node;
}

static Tree * ebnf_ap_main_rule (Parser * parser, GrammarRule next, GrammarRule ap) {
    Tree * nextNode = next(parser);
    if (nextNode != NULL) {
        Tree * apNode = ap(parser);
        if (apNode != NULL) {
            List_insert(apNode->children, nextNode, 0);
            return apNode;
        }
        return nextNode;
    }
    return NULL;
}

static Tree * ebnf_ap_recursive_rule (Parser * parser, TokenType types[], size_t typesLen, GrammarRule next,
                                    GrammarRule ap, Memory * memory) {
    Tree * opNode = ebnf_selectLexemes(parser, types, typesLen, memory);
    if (opNode == NULL) return NULL;
    Tree * node = NULL;
    Tree * nextNode = next(parser);
    Tree * apNode = ap(parser);
    if (apNode != NULL) {
        List_insert(apNode->children, nextNode, 0);
        node = apNode;
    } else {
        node = nextNode;
    }
    List_add(opNode->children, node);
    return opNode;
}

static Tree * prog (Parser * parser, Memory * memory) {
    TRACE_CALL();
    Tree * progNode = Tree_new(AstNode_new(AstNodeType_PROGRAM, strdup("PROGRAM", memory), memory), memory);
    Tree * tree = var_decl(parser, memory);
    (void)ebnf_multiple(parser, progNode->children, var_decl, memory);
    (void)ebnf_multiple(parser, progNode->children, st, memory);
    return progNode;
}

static Tree * var_decl (Parser * parser, Memory * memory) {
    TRACE_CALL();
    Tree * tree = accept(parser, TokenType_VAR, memory);
    if (tree == NULL) {
        return NULL;
    }
    Tree_clear(tree);
    Tree * idNode = ID_expect(parser, memory);
    if (!idNode) {
        return NULL;
    }
    Tree * tree1 = expect(parser, TokenType_ASSIGN, memory);
    if (tree1 == NULL) {
        Tree_clear(idNode);
        return NULL;
    }
    Tree_clear(tree1);
    Tree * exprNode = expr(parser, memory);
    if (exprNode == NULL) {
        Tree_clear(idNode);
        Tree_clear(exprNode);
        return NULL;
    }
    Tree * tree2 = expect(parser, TokenType_SEMICOLON, memory);
    if (tree2 == NULL) {
        Tree_clear(idNode);
        Tree_clear(exprNode);
        return NULL;
    }
    Tree_clear(tree2);
    Tree * varDecl = Tree_new(AstNode_new(AstNodeType_DECLAREVAR, strdup("DECLAREVAR", memory), memory), memory);
    List_add(varDecl->children, idNode);
    List_add(varDecl->children, exprNode);
    return varDecl;
}

static Tree * st (Parser * parser, Memory * memory) {
    TRACE_CALL();
    return ebnf_select(parser, (GrammarRule[]) {
            block_st, select_st, iter_st, expr_st
    }, 4, memory);
}

static Tree * ID (Parser * parser, Memory * memory) {
    TRACE_CALL();
    return accept(parser, TokenType_ID, memory);
}

static Tree * ID_expect (Parser * parser, Memory * memory) {
    return expect(parser, TokenType_ID, memory);
}

static Tree * expr (Parser * parser, Memory * memory) {
    TRACE_CALL();
    return assign(parser, memory);
}

static Tree * expr_st (Parser * parser, Memory * memory) {
    TRACE_CALL();
    Tree * exprNode = expr(parser, memory);
    if (exprNode != NULL) {
        Tree * tree = expect(parser, TokenType_SEMICOLON, memory);
        if (tree != NULL) {
            Tree_clear(tree);
        }
    } else {
        Tree * tree = accept(parser, TokenType_SEMICOLON, memory);
        if (tree != NULL) {
            Tree_clear(tree);
        }
    }
    return exprNode;
}

static Tree * block_st (Parser * parser, Memory * memory) {
    TRACE_CALL();
    Tree * tree = accept(parser, TokenType_CLOP, memory);
    if (tree == NULL) {
        return NULL;
    }
    Tree_clear(tree);
    Tree * blockNode = Tree_new(AstNode_new(AstNodeType_BLOCK, strdup("BLOCK", memory), memory), memory);
    if(ebnf_multiple(parser, blockNode->children, st, memory)) {
        Tree * tree1 = expect(parser, TokenType_CROP, memory);
        if(tree1 != NULL) {
            Tree_clear(tree1);
        }
    }
    return blockNode;
}

static Tree * select_st (Parser * parser, Memory * memory) {
    TRACE_CALL();
    Tree * tree = accept(parser, TokenType_IF, memory);
    if (tree == NULL)
        return NULL;
    Tree_clear(tree);
    Tree * tree1 = expect(parser, TokenType_LOP, memory);
    if (tree1 == NULL)
        return NULL;
    Tree_clear(tree1);
    Tree * testExpr = expr(parser, memory);
    if (testExpr == NULL) return NULL;
    Tree * tree3 = expect(parser, TokenType_ROP, memory);
    if (tree3 == NULL) {
        Tree_clear(testExpr);
        return NULL;
    }
    Tree_clear(tree3);
    Tree * stNode = st(parser, memory);
    if (stNode == NULL) {
        Tree_clear(testExpr);
        return NULL;
    }
    Tree * ifNode = Tree_new(AstNode_new(AstNodeType_IF, strdup("IF", memory), memory), memory);
    List_add(ifNode->children, testExpr);
    List_add(ifNode->children, stNode);

    Tree * tree2 = accept(parser, TokenType_ELSE, memory);
    if (tree2 != NULL) {
        Tree * elseNode = st(parser, memory);
        if (elseNode == NULL || parser->error) {
            Tree_clear(tree2);
            return NULL;
        }
        List_add(ifNode->children, elseNode);
        Tree_clear(tree2);
    }
    return ifNode;
}

static Tree * iter_st (Parser * parser, Memory * memory) {
    TRACE_CALL();
    Tree * tree = accept(parser, TokenType_LOOP, memory);
    if (tree == NULL) return NULL;
    Tree_clear(tree);
    Tree * tree1 = expect(parser, TokenType_LOP, memory);
    if (tree1 == NULL) return NULL;
    Tree_clear(tree1);
    Tree * testExpr = expr(parser, memory);
    if (testExpr == NULL) {
        Tree_clear(testExpr);
        return NULL;
    }
    Tree * tree2 = expect(parser, TokenType_ROP, memory);
    if (tree2 == NULL) {
        Tree_clear(testExpr);
        return NULL;
    }
    Tree_clear(tree2);
    Tree * stNode = st(parser, memory);
    if (stNode == NULL) {
        Tree_clear(testExpr);
        Tree_clear(stNode);
        return NULL;
    }
    Tree * whileNode = Tree_new(AstNode_new(AstNodeType_WHILE, strdup("WHILE", memory), memory), memory);
    List_add(whileNode->children, testExpr);
    List_add(whileNode->children, stNode);
    return whileNode;
}

static Tree * assign (Parser * parser, Memory * memory) {
    TRACE_CALL();
    return ebnf_ap_main_rule(parser, log_or, assign_ap);
}

static Tree * assign_ap (Parser * parser, Memory * memory) {
    TRACE_CALL();
    return ebnf_ap_recursive_rule(parser, (TokenType[]) {
            TokenType_ASSIGN
    }, 1, log_or, assign_ap, memory);
}

static Tree * log_or (Parser * parser, Memory * memory) {
    TRACE_CALL();
    return ebnf_ap_main_rule(parser, log_and, log_or_ap);
}

static Tree * log_or_ap (Parser * parser, Memory * memory) {
    TRACE_CALL();
    return ebnf_ap_recursive_rule(parser, (TokenType[]) {
            TokenType_OR
    }, 1, log_and, log_or_ap, memory);
}

static Tree * log_and (Parser * parser, Memory * memory) {
    TRACE_CALL();
    return ebnf_ap_main_rule(parser, eq, log_and_ap);
}

static Tree * log_and_ap (Parser * parser, Memory * memory) {
    TRACE_CALL();
    return ebnf_ap_recursive_rule(parser, (TokenType[]) {
            TokenType_AND
    }, 1, eq, log_and_ap, memory);
}

static Tree * eq (Parser * parser, Memory * memory) {
    TRACE_CALL();
    return ebnf_ap_main_rule(parser, rel, eq_ap);
}

static Tree * eq_ap (Parser * parser, Memory * memory) {
    TRACE_CALL();
    return ebnf_ap_recursive_rule(parser, (TokenType[]) {
            TokenType_EQUAL,
            TokenType_NEQUAL
    }, 2, rel, eq_ap, memory);
}

static Tree * rel (Parser * parser, Memory * memory) {
    TRACE_CALL();
    return ebnf_ap_main_rule(parser, add, rel_ap);
}

static Tree * rel_ap (Parser * parser, Memory * memory) {
    TRACE_CALL();
    return ebnf_ap_recursive_rule(parser, (TokenType[]) {
            TokenType_LESS,
            TokenType_LESSQ,
            TokenType_MORE,
            TokenType_MOREQ
    }, 4, add, rel_ap, memory);
}

static Tree * add (Parser * parser, Memory * memory) {
    TRACE_CALL();
    return ebnf_ap_main_rule(parser, mult, add_ap);
}

static Tree * add_ap (Parser * parser, Memory * memory) {
    TRACE_CALL();
    return ebnf_ap_recursive_rule(parser, (TokenType[]) {
            TokenType_PLUS,
            TokenType_MINUS
    }, 2, mult, add_ap, memory);
}

static Tree * mult (Parser * parser, Memory * memory) {
    TRACE_CALL();
    return ebnf_ap_main_rule(parser, unary, mult_ap);
}

static Tree * mult_ap (Parser * parser, Memory * memory) {
    TRACE_CALL();
    return ebnf_ap_recursive_rule(parser, (TokenType[]) {
            TokenType_MULT,
            TokenType_DIV,
            TokenType_MOD
    }, 3, unary, mult_ap, memory);
}

static Tree * unary (Parser * parser, Memory * memory) {
    TRACE_CALL();
    Tree * opNode = ebnf_selectLexemes(parser, (TokenType[]) {
             TokenType_PLUS,
             TokenType_MINUS,
             TokenType_NOT
    }, 3, memory);
    Tree * primNode = primary(parser, memory);
    if (primNode == NULL) return NULL;
    if (opNode) {
        List_add(opNode->children, primNode);
        return opNode;
    }
    return primNode;
}

static Tree * primary (Parser * parser, Memory * memory) {
    TRACE_CALL();
    return ebnf_select(parser, (GrammarRule[]) {
          NUMBER,
          STRING,
          var_or_call,
          parentheses
    }, 4, memory);
}

static Tree * NUMBER (Parser * parser, Memory * memory) {
    TRACE_CALL();
    return accept(parser, TokenType_NUMBER, memory);
}

static Tree * STRING (Parser * parser, Memory * memory) {
    TRACE_CALL();
    return accept(parser, TokenType_STRING, memory);
}

static Tree * var_or_call (Parser * parser, Memory * memory) {
    TRACE_CALL();
    Tree * varNode = ID(parser, memory);
    Tree * argListNode = fn_call(parser, memory);
    if (argListNode != NULL) {
        List_add(varNode->children, argListNode);
    }
    return varNode;
}

static Tree * parentheses (Parser * parser, Memory * memory) {
    TRACE_CALL();
    Tree * tree = accept(parser, TokenType_LOP, memory);
    if (tree == NULL) return NULL;
    Tree_clear(tree);
    Tree * exprNode = expr(parser, memory);
    Tree * tree1 = expect(parser, TokenType_ROP, memory); // @todo mb
    if (tree1 != NULL) {
        Tree_clear(tree1);
    }
    return exprNode;
}

static Tree * fn_call (Parser * parser, Memory * memory) {
    TRACE_CALL();
    Tree * tree = accept(parser, TokenType_LOP, memory);
    if (tree == NULL) return NULL;
    Tree_clear(tree);
    Tree * argListNode = arg_list(parser, memory);
    Tree * tree1 = expect(parser, TokenType_ROP, memory); // @todo mb
    if (tree1 != NULL) {
        Tree_clear(tree1);
    }
    return argListNode;
}

static Tree * arg_list (Parser * parser, Memory * memory) {
    TRACE_CALL();
    Tree * exprNode = expr(parser, memory);
    Tree * argListNode = Tree_new(AstNode_new(AstNodeType_ARGLIST, strdup("ARGLIST", memory), memory), memory);
    if (exprNode) {
        List_add(argListNode->children, exprNode);
        while (true) {
            Tree * tree = accept(parser, TokenType_COMMA, memory);
            if (tree == NULL) break;
            Tree_clear(tree);
            exprNode = expr(parser, memory);
            if (exprNode) {
                List_add(argListNode->children, exprNode);
            } else {
                break;
            }
        }
    }
    return argListNode;
}