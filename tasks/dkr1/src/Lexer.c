#include <Lexer.h>
#include <ctype.h>
#include <string.h>

typedef struct __String String;

struct __String {
    char * str;
    int length;
};

static void stringBuffer_add(String * self, char symb);
static String * stringBuffer_new(void);
static void stringBuffer_free(String * self);

Token * Token_new(TokenType type, const char * name, Memory * memory) {
    if (name != NULL) {
        Token *self = malloc(sizeof(Token));
        Memory_add(memory, self);
        self->type = type;
        size_t length = strlen(name) + 1;
        self->lexeme = calloc(length, sizeof(char));
        Memory_add(memory, self->lexeme);
        strcat(self->lexeme, name);
        return self;
    }
    return NULL;
}

void Token_free(Token * self) {
    if (self != NULL) {
        free(self->lexeme);
        free(self);
    }
}

bool Token_equals(Token * self, Token * other) {
    if (self == NULL && other == NULL) return true;
    if ((self->type != other->type) || strcmp(self->lexeme, other->lexeme) != 0) {
        return false;
    }
    return true;
}

char * TokenType_toString(TokenType type) {
    switch (type) {
        case TokenType_ID:
            return "ID";
        case TokenType_NUMBER:
            return "NUM";
        case TokenType_PLUS:
            return "+";
        case TokenType_MINUS:
            return "-";
        case TokenType_MULT:
            return "*";
        case TokenType_DIV:
            return "/";
        case TokenType_VAR:
            return "VAR";
        case TokenType_EQUAL:
            return "==";
        case TokenType_SQRT:
            return "SQRT";
        case TokenType_POW:
            return "POW";
        case TokenType_STRLEN:
            return "STRLEN";
        case TokenType_SUBSTR:
            return "SUBSTR";
        case TokenType_PUSH:
            return "PUSH";
        case TokenType_COUNT:
            return "COUNT";
        case TokenType_AT:
            return "AT";
        case TokenType_PRINT:
            return "PRINT";
        case TokenType_SCAN:
            return "SCAN";
        case TokenType_STRTONUM:
            return "STRTONUM";
        case TokenType_NUMTOSTR:
            return "NUMTOSTR";
        case TokenType_IF:
            return "IF";
        case TokenType_ELSE:
            return "ELSE";
        case TokenType_LOOP:
            return "LOOP";
        case TokenType_LOP:
            return "(";
        case TokenType_ROP:
            return ")";
        case TokenType_ASSIGN:
            return "=";
        case TokenType_CLOP:
            return "{";
        case TokenType_CROP:
            return "}";
        case TokenType_SEMICOLON:
            return ";";
        case TokenType_WS:
            return "WS";
        case TokenType_QUOTE:
            return "\'";
        case TokenType_DQUOTE:
            return "\"";
        case TokenType_FLOP:
            return "[";
        case TokenType_FROP:
            return "]";
        case TokenType_DOT:
            return ".";
        case TokenType_COMMA:
            return ",";
        case TokenType_AND:
            return "&&";
        case TokenType_OR:
            return "||";
        case TokenType_NEQUAL:
            return "!=";
        case TokenType_STRING:
            return "STRING";
        case TokenType_LESSQ:
            return "<=";
        case TokenType_LESS:
            return "<";
        case TokenType_MOREQ:
            return ">=";
        case TokenType_MORE:
            return ">";
        case TokenType_MOD:
            return "%";
        default:
            return "UNDEFINED";
    }
}

int Lexer_splitTokens(const char * input, List * tokens, Memory * memory) {
    if (input == NULL) return -1;
    if (tokens == NULL) return -1;
    while(*input != '\0') {
        char c = *input;
        if (c == '+') {
            List_add(tokens, Token_new(TokenType_PLUS, "+", memory));
        } else if (c == '-') {
            List_add(tokens, Token_new(TokenType_MINUS, "-", memory));
        } else if (c == '*') {
            List_add(tokens, Token_new(TokenType_MULT, "*", memory));
        } else if (c == '/') {
            List_add(tokens, Token_new(TokenType_DIV, "/", memory));
        } else if (c == '(') {
            List_add(tokens, Token_new(TokenType_LOP, "(", memory));
        } else if (c == ')') {
            List_add(tokens, Token_new(TokenType_ROP, ")", memory));
        } else if (c == '=') {
            if (*(++input) == '=') {
                List_add(tokens, Token_new(TokenType_EQUAL, "==", memory));
            } else {
                List_add(tokens, Token_new(TokenType_ASSIGN, "=", memory));
                input--;
            }
        } else if (c == '{') {
            List_add(tokens, Token_new(TokenType_CLOP, "{", memory));
        } else if (c == '}') {
            List_add(tokens, Token_new(TokenType_CROP, "}", memory));
        } else if (c == ';') {
            List_add(tokens, Token_new(TokenType_SEMICOLON, ";", memory));
        } else if (c == ' ') {
        } else if (isdigit(c)) {
            char s = *input;
            String * str = stringBuffer_new();
            while (isdigit(s)) {
                stringBuffer_add(str, s);
                s = *(++input);
            }
            List_add(tokens, Token_new(TokenType_NUMBER, str->str, memory));
            stringBuffer_free(str);
            input--;
        } else if (c == '\'') {
            List_add(tokens, Token_new(TokenType_QUOTE, "\'", memory));
        } else if (c == '\"') {
            char s = *++input;
            String * str = stringBuffer_new();
            stringBuffer_add(str, '\"');
            while (!strchr("\"", s)) {
                stringBuffer_add(str, s);
                s = *(++input);
            }
            stringBuffer_add(str, '\"');
            List_add(tokens, Token_new(TokenType_STRING, str->str, memory));
            stringBuffer_free(str);
        } else if (c == '[') {
            List_add(tokens, Token_new(TokenType_FLOP, "[", memory));
        } else if (c == ']') {
            List_add(tokens, Token_new(TokenType_FROP, "]", memory));
        } else if (c == '.') {
            List_add(tokens, Token_new(TokenType_DOT, ".", memory));
        } else if (c == ',') {
            List_add(tokens, Token_new(TokenType_COMMA, ",", memory));
        } else if (c == '\n') {
        } else if (c == '|') {
            if (*(++input) == '|') {
                List_add(tokens, Token_new(TokenType_OR, "||", memory));
            } else {
                input--;
            }
        } else if (c == '&') {
            if (*(++input) == '&') {
                List_add(tokens, Token_new(TokenType_AND, "&&", memory));
            } else {
                input--;
            }
        } else if (c == '!') {
            if (*(++input) == '=') {
                List_add(tokens, Token_new(TokenType_NEQUAL, "!=", memory));
            } else {
                List_add(tokens, Token_new(TokenType_NOT, "!", memory));
                input--;
            }
        } else if (c == '>') {
            if (*(++input) == '=') {
                List_add(tokens, Token_new(TokenType_MOREQ, ">=", memory));
            } else {
                input--;
                List_add(tokens, Token_new(TokenType_MORE, ">", memory));
            }
        } else if (c == '<') {
            if (*(++input) == '=') {
                List_add(tokens, Token_new(TokenType_LESSQ, "<=", memory));
            } else {
                input--;
                List_add(tokens, Token_new(TokenType_LESS, "<", memory));
            }
        } else if (c == '%') {
            List_add(tokens, Token_new(TokenType_MOD, "%", memory));
        } else {
            char s = *input;
            String * str = stringBuffer_new();
            while (!strchr(".,;:=-+*/(){}\"\'[]&|!>< \0\n", s)) {
                stringBuffer_add(str, s);
                s = *(++input);
            }
            if (!strcmp(str->str, "sqrt")) {
                List_add(tokens, Token_new(TokenType_ID, "SQRT", memory));
            } else if (!strcmp(str->str, "pow")) {
                List_add(tokens, Token_new(TokenType_ID, "POW", memory));
            } else if (!strcmp(str->str, "strlen")) {
                List_add(tokens, Token_new(TokenType_ID, "STRLEN", memory));
            } else if (!strcmp(str->str, "substr")) {
                List_add(tokens, Token_new(TokenType_ID, "SUBSTR", memory));
            } else if (!strcmp(str->str, "push")) {
                List_add(tokens, Token_new(TokenType_ID, "PUSH", memory));
            } else if (!strcmp(str->str, "count")) {
                List_add(tokens, Token_new(TokenType_ID, "COUNT", memory));
            } else if (!strcmp(str->str, "at")) {
                List_add(tokens, Token_new(TokenType_ID, "AT", memory));
            } else if (!strcmp(str->str, "print")) {
                List_add(tokens, Token_new(TokenType_ID, "PRINT", memory));
            } else if (!strcmp(str->str, "scan")) {
                List_add(tokens, Token_new(TokenType_ID, "SCAN", memory));
            } else if (!strcmp(str->str, "strtonum")) {
                List_add(tokens, Token_new(TokenType_ID, "STRTONUM", memory));
            } else if (!strcmp(str->str, "numtostr")) {
                List_add(tokens, Token_new(TokenType_ID, "NUMTOSTR", memory));
            } else if (!strcmp(str->str, "if")) {
                List_add(tokens, Token_new(TokenType_IF, "IF", memory));
            } else if (!strcmp(str->str, "else")) {
                List_add(tokens, Token_new(TokenType_ELSE, "ELSE", memory));
            } else if (!strcmp(str->str, "while")) {
                List_add(tokens, Token_new(TokenType_LOOP, "WHILE", memory));
            } else if (!strcmp(str->str, "var")) {
                List_add(tokens, Token_new(TokenType_VAR, "VAR", memory));
            } else  {
                List_add(tokens, Token_new(TokenType_ID, str->str, memory));
            }
            stringBuffer_free(str);
            input--;
        }
        input++;
    }
    return 0;
}

void Lexer_clearTokens(List * tokens) {
    if (tokens != NULL) {
        for (int i = 0; i < List_count(tokens); i++) {
            Token_free(List_at(tokens, (size_t)i));
        }
    }
}

void Lexer_printTokens(List * tokens, Memory * memory) {
    if (tokens != NULL) {
        Iterator *begin = List_getNewBeginIterator(tokens, memory);
        Iterator *end = List_getNewEndIterator(tokens, memory);
        for (; !Iterator_equals(begin, end); Iterator_next(begin)) {
            Token *self = Iterator_value(begin);
            if (self->type == TokenType_NUMBER) {
                printf("<NUM, %s>", self->lexeme);
            } else if (self->type == TokenType_ID) {
                printf("<ID, %s>", self->lexeme);
            } else if (self->type == TokenType_STRING) {
                printf("<STRING, %s>", self->lexeme);
            } else {
                printf("<%s>", self->lexeme);
            }
        }
        Iterator_free(begin);
        Iterator_free(end);
    }
}

void Lexer_writeToFile(List * tokens, Memory * memory) {
    if (tokens != NULL) {
        FILE * f = fopen("../main_tokens.txt", "w");
        Iterator *begin = List_getNewBeginIterator(tokens, memory);
        Iterator *end = List_getNewEndIterator(tokens, memory);
        for (; !Iterator_equals(begin, end); Iterator_next(begin)) {
            Token *self = Iterator_value(begin);
            if (self->type == TokenType_NUMBER) {
                fprintf(f, "<NUM, %s>", self->lexeme);
            } else if (self->type == TokenType_ID) {
                fprintf(f, "<ID, %s", self->lexeme);
            } else {
                fprintf(f, "<%s>", self->lexeme);
            }
        }
        Iterator_free(begin);
        Iterator_free(end);
        fclose(f);
    }
}

static String * stringBuffer_new(void) {
    String * self = malloc(sizeof(String));
    self->str = malloc(sizeof(char));
    self->str[0] = '\0';
    self->length = 1;
    return self;
}

static void stringBuffer_add(String * self, char symb) {
    if (self != NULL) {
        if (self->length == 1) {
            self->str[0] = symb;
        } else {
            self->str[self->length - 1] = symb;
        }
        self->length++;
        self->str = realloc(self->str, self->length * sizeof(char));
        self->str[self->length - 1] = '\0';
    }
}

static void stringBuffer_free(String * self) {
    if (self != NULL) {
        free(self->str);
        free(self);
    }
}