#include <Lexer.h>
#include <Parser.h>
#include <Ast.h>
#include <File.h>
#include <Memory.h>

int main(void) {
    if (fileExists("../main.svag") == 0) {
        return EXIT_FAILURE;
    } else {
        Memory * memory = Memory_new();
        long length = getFileSize("../main.svag") + 1;
        char * buff = calloc((size_t)length, sizeof(char));
        readFileToBuffer("../main.svag", buff, (int)length);
        List *list = List_new(memory);
        Lexer_splitTokens(buff, list, memory);
        Lexer_printTokens(list, memory);
        Lexer_writeToFile(list, memory);

        puts("\n\n=======================\n\n");

        Tree * root = Parser_buildNewAstTree(list, memory);
        AstTree_prettyPrint(root, memory);
        //bool match = Parser_match(list);
        //printf("Grammar match: %s", match ? "TRUE" : "FALSE");
        Memory_free(memory);
    }
    return EXIT_SUCCESS;
}