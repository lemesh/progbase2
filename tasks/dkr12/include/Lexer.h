#pragma once
#include "List.h"
#include <stdbool.h>
#include <string.h>
#include <stdio.h>

typedef enum {
    TokenType_PLUS,
    TokenType_MINUS,
    TokenType_MULT,
    TokenType_DIV,
    TokenType_NUMBER,
    TokenType_VAR,
    TokenType_EQUAL,
    TokenType_OPER,
    TokenType_SQRT,
    TokenType_POW,
    TokenType_STRLEN,
    TokenType_SUBSTR,
    TokenType_PUSH,
    TokenType_COUNT,
    TokenType_AT,
    TokenType_PRINT,
    TokenType_SCAN,
    TokenType_STRTONUM,
    TokenType_NUMTOSTR,
    TokenType_IF,
    TokenType_ELSE,
    TokenType_LOOP,
    TokenType_LOP,
    TokenType_ROP,
    TokenType_UNDEF,
    TokenType_ASSIGN,
    TokenType_CLOP,
    TokenType_CROP,
    TokenType_SEMICOLON,
    TokenType_WS,
    TokenType_QUOTE,
    TokenType_DQUOTE,
    TokenType_FLOP,
    TokenType_FROP,
    TokenType_DOT,
    TokenType_COMMA
} TokenType;

typedef struct __Token Token;

Token * Token_new(TokenType type, const char * name);
void Token_free(Token * self);

bool Token_equals(Token * self, Token * other);

void TokenType_print(TokenType type);

int Lexer_splitTokens(const char * input, List * tokens);
void Lexer_clearTokens(List * tokens);

void Lexer_printTokens(List * tokens);