#include "../include/List.h"

const int LIST_CAPACITY = 15;

struct __List {
    void ** items;
    int capacity;
    int length;
};

List * List_new(void) {
    List * self = malloc(sizeof(List));
    self->capacity = LIST_CAPACITY;
    self->length = 0;
    self->items = calloc(LIST_CAPACITY,sizeof(void *));
    return self;
}

void List_free(List * self) {
    free(self->items);
    free(self);
    return;
}

void List_insert(List * self, void * value, size_t index) {
    if (self->length == self->capacity) {
        self->capacity++;
        self->items = realloc(self->items, sizeof(void *) * self->capacity);
    }
    for (int i = self->length - 1; i >= index; i--) {
        self->items[i + 1] = self->items[i];
    }
    self->length++;
    self->items[index] = value;
    return;
}

void List_add(List * self, void * value) {
    if (self->length == self->capacity) {
        self->capacity++;
        self->items = realloc(self->items, sizeof(void *) * self->capacity);
    }
    self->items[self->length] = value;
    self->length++;
    return;
}

void * List_at(List * self, size_t index) {
    return self->items[index];
}

void * List_removeAt(List * self, size_t index) {
    void * value = self->items[index];
    for (int i = index; i < self->length; i++) {
        self->items[i] = self->items[i + 1];
    }
    return value;
}

size_t List_count(List * self) {
    return self->length;
}

struct __Iterator {

};

Iterator * List_getNewBeginIterator(List * self);
Iterator * List_getNewEndIterator(List * self);

void Iterator_free(Iterator * self);

void * Iterator_value(Iterator * self);
void Iterator_next(Iterator * self);
void Iterator_prev(Iterator * self);
void Iterator_advance(Iterator * self, IteratorDistance n);
bool Iterator_equals(Iterator * self, Iterator * other);
IteratorDistance Iterator_distance(Iterator * begin, Iterator * end);