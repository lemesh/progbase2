#include "../include/Lexer.h"
#include <ctype.h>

typedef struct __String String;

struct __String {
    char * str;
    int length;
};

static void stringBuffer_add(String * self, char symb);
static String * stringBuffer_new(void);
static void stringBuffer_free(String * self);

struct __Token {
    TokenType type;
    char * lexeme;
};

Token * Token_new(TokenType type, const char * name) {
    Token * self = malloc(sizeof(Token));
    self->type = type;
    size_t length = strlen(name) + 1;
    self->lexeme = calloc(length, sizeof(char));
    strcat(self->lexeme, name);
    return self;
}

void Token_free(Token * self) {
    free(self->lexeme);
    free(self);
}

bool Token_equals(Token * self, Token * other) {
    if ((self->type != other->type) || strcmp(self->lexeme, other->lexeme) != 0) {
        return false;
    }
    return true;
}

void TokenType_print(TokenType type) {
    switch (type) {
        case TokenType_OPER:
            break;
        case TokenType_NUMBER:
            printf("NUM");
            break;
        case TokenType_PLUS:
            printf("+");
            break;
        case TokenType_MINUS:
            printf("-");
            break;
        case TokenType_MULT:
            printf("*");
            break;
        case TokenType_DIV:
            printf("/");
            break;
        case TokenType_VAR:
            printf("VAR");
            break;
        case TokenType_EQUAL:
            printf("==");
            break;
        case TokenType_SQRT:
            printf("SQRT");
            break;
        case TokenType_POW:
            printf("POW");
            break;
        case TokenType_STRLEN:
            printf("STRLEN");
            break;
        case TokenType_SUBSTR:
            printf("SUBSTR");
            break;
        case TokenType_PUSH:
            printf("PUSH");
            break;
        case TokenType_COUNT:
            printf("COUNT");
            break;
        case TokenType_AT:
            printf("AT");
            break;
        case TokenType_PRINT:
            printf("PRINT");
            break;
        case TokenType_SCAN:
            printf("SCAN");
            break;
        case TokenType_STRTONUM:
            printf("STRTONUM");
            break;
        case TokenType_NUMTOSTR:
            printf("NUMTOSTR");
            break;
        case TokenType_IF:
            printf("IF");
            break;
        case TokenType_ELSE:
            printf("ELSE");
            break;
        case TokenType_LOOP:
            printf("LOOP");
            break;
        case TokenType_LOP:
            printf("(");
            break;
        case TokenType_ROP:
            printf(")");
            break;
        case TokenType_ASSIGN:
            printf("=");
            break;
        case TokenType_CLOP:
            printf("{");
            break;
        case TokenType_CROP:
            printf("}");
            break;
        case TokenType_SEMICOLON:
            printf(";");
            break;
        case TokenType_WS:
            printf("WS");
            break;
        case TokenType_QUOTE:
            printf("\'");
            break;
        case TokenType_DQUOTE:
            printf("\"");
            break;
        case TokenType_FLOP:
            printf("[");
            break;
        case TokenType_FROP:
            printf("]");
            break;
        case TokenType_DOT:
            printf(".");
            break;
        case TokenType_COMMA:
            printf(",");
            break;
        default:
            printf("UNDEFINED");
            break;
    }
}

int Lexer_splitTokens(const char * input, List * tokens) {
    if (input == NULL) return 1;
    while(*input != '\0') {
        char c = *input;
        //printf("%c", c);
        if (c == '+') {
            List_add(tokens, Token_new(TokenType_PLUS, "+"));
        } else if (c == '-') {
            List_add(tokens, Token_new(TokenType_MINUS, "-"));
        } else if (c == '*') {
            List_add(tokens, Token_new(TokenType_MULT, "*"));
        } else if (c == '/') {
            List_add(tokens, Token_new(TokenType_DIV, "/"));
        } else if (c == '(') {
            List_add(tokens, Token_new(TokenType_LOP, "("));
        } else if (c == ')') {
            List_add(tokens, Token_new(TokenType_ROP, ")"));
        } else if (c == '=') {
            if (*(++input) == '=') {
                List_add(tokens, Token_new(TokenType_EQUAL, "=="));
                input--;
            } else {
                List_add(tokens, Token_new(TokenType_ASSIGN, "="));
            }
        } else if (c == '{') {
            List_add(tokens, Token_new(TokenType_CLOP, "{"));
        } else if (c == '}') {
            List_add(tokens, Token_new(TokenType_CROP, "}"));
        } else if (c == ';') {
            List_add(tokens, Token_new(TokenType_SEMICOLON, ";"));
        } else if (c == ' ') {
            List_add(tokens, Token_new(TokenType_WS, " "));
        } else if (isdigit(c)) {
            char s = *input;
            String * str = stringBuffer_new();
            while (isdigit(s)) {
                stringBuffer_add(str, s);
                s = *(++input);
            }
            List_add(tokens, Token_new(TokenType_NUMBER, str->str));
            stringBuffer_free(str);
            input--;
        } else if (c == '\'') {
            List_add(tokens, Token_new(TokenType_QUOTE, "\'"));
        } else if (c == '\"') {
            List_add(tokens, Token_new(TokenType_DQUOTE, "\""));
        } else if (c == '[') {
            List_add(tokens, Token_new(TokenType_FLOP, "["));
        } else if (c == ']') {
            List_add(tokens, Token_new(TokenType_FROP, "]"));
        } else if (c == '.') {
            List_add(tokens, Token_new(TokenType_DOT, "."));
        } else if (c == ',') {
            List_add(tokens, Token_new(TokenType_COMMA, ","));
        } else {
            char s = *input;
            String * str = stringBuffer_new();
            while (!strchr(".,;:=-+*/(){}\"\'[] \0", s)) {
                stringBuffer_add(str, s);
                s = *(++input);
            }
            if (!strcmp(str->str, "sqrt")) {
                List_add(tokens, Token_new(TokenType_SQRT, "SQRT"));
            } else if (!strcmp(str->str, "pow")) {
                List_add(tokens, Token_new(TokenType_POW, "POW"));
            } else if (!strcmp(str->str, "strlen")) {
                List_add(tokens, Token_new(TokenType_STRLEN, "STRLEN"));
            } else if (!strcmp(str->str, "substr")) {
                List_add(tokens, Token_new(TokenType_SUBSTR, "SUBSTR"));
            } else if (!strcmp(str->str, "push")) {
                List_add(tokens, Token_new(TokenType_PUSH, "PUSH"));
            } else if (!strcmp(str->str, "count")) {
                List_add(tokens, Token_new(TokenType_COUNT, "COUNT"));
            } else if (!strcmp(str->str, "at")) {
                List_add(tokens, Token_new(TokenType_AT, "AT"));
            } else if (!strcmp(str->str, "print")) {
                List_add(tokens, Token_new(TokenType_PRINT, "PRINT"));
            } else if (!strcmp(str->str, "scan")) {
                List_add(tokens, Token_new(TokenType_SCAN, "SCAN"));
            } else if (!strcmp(str->str, "strtonum")) {
                List_add(tokens, Token_new(TokenType_STRTONUM, "STRTONUM"));
            } else if (!strcmp(str->str, "numtostr")) {
                List_add(tokens, Token_new(TokenType_NUMTOSTR, "NUMTOSTR"));
            } else if (!strcmp(str->str, "if")) {
                List_add(tokens, Token_new(TokenType_IF, "IF"));
            } else if (!strcmp(str->str, "else")) {
                List_add(tokens, Token_new(TokenType_ELSE, "ELSE"));
            } else if (!strcmp(str->str, "while")) {
                List_add(tokens, Token_new(TokenType_LOOP, "WHILE"));
            } else {
                List_add(tokens, Token_new(TokenType_UNDEF, str->str));
            }
            stringBuffer_free(str);
            input--;
        }
        input++;
    }
    return 0;
}

void Lexer_clearTokens(List * tokens) {
    for (int i = 0; i < List_count(tokens); i++) {
        Token_free(List_at(tokens, i));
    }
}

void Lexer_printTokens(List * tokens) {
    for (int i = 0; i < List_count(tokens); i++) {
        Token * self = List_at(tokens, i);
        if (self->type == TokenType_NUMBER) {
            printf("<NUM, %s>", self->lexeme);
        } else {
            printf("<%s>", self->lexeme);
        }
    }
}

static String * stringBuffer_new(void) {
    String * self = malloc(sizeof(String));
    self->str = malloc(sizeof(char));
    self->str[0] = '\0';
    self->length = 1;
    return self;
}

static void stringBuffer_add(String * self, char symb) {
    if (self->length == 1) {
        self->str[0] = symb;
    } else {
        self->str[self->length - 1] = symb;
    }
    self->length++;
    self->str = realloc(self->str, self->length * sizeof(char));
    self->str[self->length - 1] = '\0';
}

static void stringBuffer_free(String * self) {
    free(self->str);
    free(self);
}