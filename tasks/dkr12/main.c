#include "include/Lexer.h"

int fileExists(const char *fileName);
int readFileToBuffer(const char *fileName, char *buffer, int bufferLength);
long getFileSize(const char *fileName);

int main(void) {
    if (fileExists("../main.svag") == 0) {
        return EXIT_FAILURE;
    } else {
        long length = getFileSize("../main.svag") + 1;
        char * buff = calloc(length, sizeof(char));
        readFileToBuffer("../main.svag", buff, length);
        List *list = List_new();
        Lexer_splitTokens(buff, list);
        Lexer_printTokens(list);
        Lexer_clearTokens(list);
        List_free(list);
        free(buff);
    }
    return EXIT_SUCCESS;
}

int fileExists(const char *fileName) {
    FILE *f = fopen(fileName, "rb");
    if (!f) return 0;  // false: not exists
    fclose(f);
    return 1;  // true: exists
}

long getFileSize(const char *fileName) {
    FILE *f = fopen(fileName, "rb");
    if (!f) return -1;  // error opening file
    fseek(f, 0, SEEK_END);  // rewind cursor to the end of file
    long fsize = ftell(f);  // get file size in bytes
    fclose(f);
    return fsize;
}

int readFileToBuffer(const char *fileName, char *buffer, int bufferLength) {
    FILE *f = fopen(fileName, "rb");
    if (!f) return 0;  // read 0 bytes from file
    long readBytes = fread(buffer, 1, bufferLength, f);
    fclose(f);
    return readBytes;  // number of bytes read
}