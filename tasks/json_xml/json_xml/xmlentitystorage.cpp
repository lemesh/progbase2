#include <xmlentitystorage.h>
#include <QtXml>
#include <iostream>
#include <fstream>

void XmlEntityStorage::save(std::vector<Admin> & entities) {
    QDomDocument doc;
    QDomElement admins = doc.createElement("admins");
    for (auto & tmp : entities) {
        QDomElement adminEl = doc.createElement("admin");
        adminEl.setAttribute("name", QString::fromStdString(tmp.name));
        adminEl.setAttribute("groupId", tmp.groupId);
        adminEl.setAttribute("adminStrength", tmp.adminStrength);
        for (auto us : tmp.users) {
            QDomElement usEl = doc.createElement("users");
            usEl.setAttribute("name", QString::fromStdString(us));
            adminEl.appendChild(usEl);
        }
        admins.appendChild(adminEl);
    }
    doc.appendChild(admins);
    std::string str = doc.toString().toStdString();
    //std::cout << str << std::endl;
//    return doc.toString().toStdString();
    //std::string str1 = name();
    std::ofstream F (name(), std::ios::out);
    F << str;
    F.close();
}

std::vector<Admin> XmlEntityStorage::load() {
    QDomDocument doc;
    std::string fileInclude = writeFileToString(name());
    if(!doc.setContent(QString::fromStdString(fileInclude))) {
        std::cerr << "xml parsing error" << std::endl;
    }
    std::vector<Admin> admins;
    QDomElement root = doc.documentElement();
    QDomNodeList list = root.childNodes();
    for (int j = 0; j < list.length(); j++) {
       QDomNode listEl = list.at(j);
       Admin a;
       a.name = listEl.toElement().attribute("name").toStdString();
       a.groupId = listEl.toElement().attribute("groupId").toInt();
       a.adminStrength = listEl.toElement().attribute("adminStrength").toDouble();
       for(int i = 0; i < listEl.childNodes().length(); i++) {
           QDomNode node = listEl.childNodes().at(i);
           QDomElement el = node.toElement();
           a.users.push_back(el.attribute("name").toStdString());
       }
       admins.push_back(a);
    }
    return admins;
}
