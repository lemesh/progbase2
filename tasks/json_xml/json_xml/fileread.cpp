#include <fileread.h>

#include <iterator>
#include <iostream>
#include <fstream>

std::string writeFileToString(std::string fileName) {
    std::ifstream fin(fileName);
    if(!fin.is_open()) {
        std::cout << "Not exist" << std::endl;
    }
    std::string s {std::string(std::istreambuf_iterator<char>(fin), std::istreambuf_iterator<char>())};
    return s;
}
