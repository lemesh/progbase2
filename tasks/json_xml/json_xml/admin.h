#pragma once

#include <vector>
#include <string>

class Admin {
public:
    std::string name;
    int groupId;
    float adminStrength;
    std::vector<std::string> users;
    void print();
};
