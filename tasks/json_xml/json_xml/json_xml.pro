QT += core
QT -= gui
QT += xml

TARGET = json_xml
CONFIG += c++14
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    jsonentitystorage.cpp \
    xmlentitystorage.cpp \
    fileread.cpp \
    storagemanager.cpp \
    admin.cpp

HEADERS += \
    admin.h \
    entitystorage.h \
    xmlentitystorage.h \
    jsonentitystorage.h \
    storagemanager.h \
    messageexception.h \
    fileread.h

DISTFILES += \
    file.json \
    file.xml \
    data.json \
    data.xml

