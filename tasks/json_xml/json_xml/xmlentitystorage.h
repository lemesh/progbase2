#pragma once

#include <entitystorage.h>
#include <fileread.h>

class XmlEntityStorage : public EntityStorage {
public:
    std::vector<Admin> load();
    void save(std::vector<Admin> & entities);
    XmlEntityStorage(std::string name) : EntityStorage(name) { }
    ~XmlEntityStorage() {}
};
