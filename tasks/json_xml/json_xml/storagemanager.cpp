#include <storagemanager.h>

EntityStorage * StorageManager::createStorage(std::string & storageFileName) {
    int i = 0;
    std::string file;
    for (auto ch : storageFileName) {
        if (ch == '.') {
            file = storageFileName.substr(i);
        }
        i++;
    }
    if (file == ".json") {
        JsonEntityStorage * j = new JsonEntityStorage(storageFileName);
        return j;
    } else if (file == ".xml") {
        XmlEntityStorage * x = new XmlEntityStorage(storageFileName);
        return x;
    } else {
        std::string msg = "wrong file type, it must be json or xml";
        std::ofstream F ("data.fake", std::ios::out);
        F << msg;
        F.close();
        throw MessageException(msg);
    }
}
