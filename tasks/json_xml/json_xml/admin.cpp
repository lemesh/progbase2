#include <admin.h>
#include <iostream>

void Admin::print() {
    std::cout << "----------------------------" << std::endl;
    std::cout << "name: " << name << std::endl;
    std::cout << "groupId: " << groupId << std::endl;
    std::cout << "adminStrength: " << adminStrength << std::endl;
    std::cout << "users:" << std::endl;
    for (auto tmp : users) {
        std::cout << "\tuser_name: " << tmp << std::endl << std::endl;
    }
    std::cout << "----------------------------" << std::endl;
}
