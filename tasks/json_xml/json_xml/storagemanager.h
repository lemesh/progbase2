#pragma once

#include <entitystorage.h>
#include <string>
#include <iostream>
#include <jsonentitystorage.h>
#include <xmlentitystorage.h>
#include <messageexception.h>
#include <fstream>
#include <cstring>

class StorageManager {
public:
    static EntityStorage * createStorage(std::string & storageFileName);
};
