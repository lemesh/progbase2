#pragma once

#include <string>
#include <vector>
#include <admin.h>

class EntityStorage {
    std::string _name;
protected:
    EntityStorage(std::string & name) { this->_name = name; }
public:
    std::string & name() { return this->_name; }

    virtual std::vector<Admin> load() = 0;
    virtual void save(std::vector<Admin> & entities) = 0;
    virtual ~EntityStorage() { }
};
