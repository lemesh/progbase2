#include <jsonentitystorage.h>
#include <admin.h>
#include <iostream>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <messageexception.h>
#include <fstream>
#include <fileread.h>

void JsonEntityStorage::save(std::vector<Admin> & entities) {
    QJsonDocument doc;
    QJsonArray adminArray;
    for(int i = 0; i < entities.size(); i++) {
        QJsonObject adminObj = adminArray.at(i).toObject();
        adminObj.insert("name", QString::fromStdString(entities[i].name));
        adminObj.insert("groupId", entities[i].groupId);
        adminObj.insert("adminStrength", entities[i].adminStrength);
        QJsonArray usersArray;
        for (auto us : entities[i].users) {
            QJsonObject usObj;
            usObj.insert("user_name", QString::fromStdString(us));
            usersArray.append(usObj);
        }
        adminObj.insert("users", usersArray);
        adminArray.append(adminObj);
    }
    doc.setArray(adminArray);
    std::string str = doc.toJson().toStdString();
    //std::cout << str << std::endl;
    std::ofstream F (name(), std::ios::out);
    F << str;
    F.close();
}

std::vector<Admin> JsonEntityStorage::load() {
    QJsonParseError err;
    std::string fileInclude = writeFileToString(name());
    QJsonDocument doc = QJsonDocument::fromJson(QByteArray::fromStdString(fileInclude), &err);
    if (err.error != QJsonParseError::NoError) {
        std::string msg = "JSon Parse Error" + err.errorString().toStdString();
        throw MessageException(msg);
    }
    std::vector<Admin> admins;
    QJsonArray adminsArray = doc.array();
    //QJsonArray adminsArray = usersObj.value("admins").toArray();
    for (int j = 0; j < adminsArray.size(); j++) {
        QJsonObject adminObj = adminsArray.at(j).toObject();
        QJsonValue nameValue = adminObj.value("name");
        QJsonValue idValue = adminObj.value("groupId");
        QJsonValue strengthValue = adminObj.value("adminStrength");
        Admin a;
        a.name = nameValue.toString().toStdString();
        a.groupId = idValue.toInt();
        a.adminStrength = strengthValue.toDouble();
        QJsonArray usersArray = adminObj.value("users").toArray();
        for (int i = 0; i < usersArray.size(); i++) {
            QJsonObject usObj = usersArray.at(i).toObject();
            a.users.push_back(usObj.value("user_name").toString().toStdString());
        }
        admins.push_back(a);
    }
    return admins;
}
