#pragma once

#include <entitystorage.h>
#include <vector>
#include <admin.h>
#include <fileread.h>

class JsonEntityStorage : public EntityStorage {
public:
    std::vector<Admin> load();
    void save(std::vector<Admin> & entities);
    JsonEntityStorage(std::string name) : EntityStorage::EntityStorage(name) { }
    ~JsonEntityStorage() {}
};
