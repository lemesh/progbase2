#include <QCoreApplication>
#include <admin.h>
#include <storagemanager.h>
#include <xmlentitystorage.h>
#include <jsonentitystorage.h>
#include <entitystorage.h>
#include <fileread.h>

int main() {
    std::vector<Admin> admins;
    for (int i = 1; i < 6; i++) {
        Admin a;
        a.adminStrength = 1.0/(double)i;
        a.groupId = i;
        a.name = "Oleh";
        a.users.push_back("Masha");
        a.users.push_back("Dasha");
        admins.push_back(a);
    }

    std::vector<EntityStorage *> e;

    //std::cout << e.size() << std::endl;

    for (std::string str : {"data.json", "data.xml", "data.fake"}) {
        try {
            e.push_back(StorageManager::createStorage(str));
        } catch (MessageException& err){
            std::cerr << err.what() << std::endl;
        }
    }

    //std::cout << e.size() << std::endl;

    for (EntityStorage * tmp : e) {
        tmp->save(admins);
    }

    std::cout << "Please type ENTER to continue..." << std::endl;
    std::cin.get();

    for (EntityStorage * tmp : e) {
        try {
            std::vector<Admin> admins = tmp->load();
            std::cout << tmp->name() << std:: endl;
            std::cout << "************************************\n";
            for (auto admin : admins) {
                admin.print();
            }
            std::cout << "************************************\n";
        } catch (MessageException& err) {
            std::cerr << err.what() << std::endl;
        }
    }

    for (EntityStorage * tmp : e) {
        delete tmp;
    }
}
