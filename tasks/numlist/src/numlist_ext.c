#include <numlist_ext.h>

void NumList_print(NumList * self) {
    if (NumList_count(self) == 0) {
        assert(0 && "Array is empty");
        fprintf(stderr, "Array is empty\n");
        return;
    }
    int len = NumList_count(self);
    for (int i = 0; i < len; i++) {
        printf("%i ", NumList_at(self, i));
    }
    puts("");
}

double NumList_average(NumList * self) {
    if (NumList_count(self) == 0) {
        assert(1 && "Array is empty");
        fprintf(stderr, "Array is empty\n");
        return 0;
    }
    double sum = 0;
    int len = NumList_count(self);
    for (int i = 0; i < len; i++) {
        sum += NumList_at(self, i);
    }
    return sum / NumList_count(self);
}

int NumList_minIndex(NumList * self) {
    if (NumList_count(self) == 0) {
        assert(2 && "Array is empty");
        fprintf(stderr, "Array is empty\n");
        return 0;
    }
    if (NumList_count(self) == 1) return NumList_at(self, 0);
    int min = NumList_at(self, 0);
    int minIndex = 0;
    int len = NumList_count(self);
    for (int i = 1; i < len; i++) {
        if(NumList_at(self, i) < min) {
            min = NumList_at(self, i);
            minIndex = i;
        }
    }
    return minIndex;
}

int NumList_maxIndex(NumList * self) {
    if (NumList_count(self) == 0) {
        assert(3 && "Array is empty");
        fprintf(stderr, "Array is empty\n");
        return 0;
    }
    if(NumList_count(self) == 1) return NumList_at(self, 0);
    int max = NumList_at(self, 0);
    int maxIndex = 0;
    int len = NumList_count(self);
    for (int i = 1; i < len; i++) {
        if(NumList_at(self, i) >max) {
            max = NumList_at(self, i);
            maxIndex = i;
        }
    }
    return maxIndex;
}