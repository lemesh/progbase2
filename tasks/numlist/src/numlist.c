#include <numlist.h>

static const int INITIAL_CAPACITY = 15;

struct __NumList {
    int * items;
    int length;
    int capacity;
};

NumList * NumList_new(void) {
    if(INITIAL_CAPACITY < 0) {
        fprintf(stderr, "Out of memory!\n");
        abort();
    }
    NumList * new = malloc(sizeof(NumList));
    new->capacity = INITIAL_CAPACITY;
    new->items = malloc(sizeof(int) * new->capacity);
    new->length = 0;
    return new;
}

void NumList_add(NumList * self, int value) {
    if (self->length == self->capacity) {
        self->capacity++;
        self->items = realloc(self, sizeof(int) * self->capacity);
    }
    self->items[self->length] = value;
    self->length++;
    return;
}

void NumList_free(NumList * self) {
    free(self->items);
    free(self);
    return;
}

void NumList_insert(NumList * self, size_t index, int value) {
    if (index > self->length) {
        assert(0 && "Invalid index");
        fprintf(stderr, "Invalid index\n");
        return;
    }
    if(self->length == self->capacity) {
        self->capacity++;
        self->items = realloc(self, sizeof(int) * self->capacity);
    }

    if(self->length == 0 && index == 0) {
        self->items[0] = value;
        return;
    }

    if(index > self->length) return;

    self->length++;
    for(int i = self->length - 1; i >= index; i--) {
        self->items[i + 1] = self->items[i];
    }
    self->items[index] = value;
}

int NumList_removeAt(NumList * self, size_t index) {
    if (index > self->length) {
        assert(1 && "Invalid index\n");
        fprintf(stderr, "Invalid index\n");
        return 0;
    }
    int value = self->items[index];
    for(int i = index; i < self->length - 1; i++) {
        self->items[i] = self->items[i + 1];
    }
    self->length--;
    return value;
}

size_t NumList_count(NumList * self) {
    return self->length;
}

int NumList_at(NumList * self, size_t index) {
    if (index > self->length) {
        assert(2 && "Invalid index\n");
        fprintf(stderr, "Invalid index\n");
        return 0;
    }
    return self->items[index];
}

int NumList_set(NumList * self, size_t index, int value) {
    if (index > self->length) {
        assert(3 && "Invalid index\n");
        fprintf(stderr, "Invalid index\n");
        return 0;
    }
    int tmp = self->items[index];
    self->items[index] = value;
    return tmp;
}