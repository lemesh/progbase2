#include <numlist.h>
#include <numlist_ext.h>
#include <stdbool.h>
#include <time.h>

int main() {
    srand(time(0));
    NumList * new = NumList_new();
    for (int i = 0; i < 10; i++) {
        NumList_add(new, rand() % 200 - 100);
    }
    NumList_print(new);
    bool negative = true;
    while (negative) {
        negative = false;
        int i = 0;
        for(int i = 0; i < NumList_count(new); i++) {
            if(NumList_at(new, i) < 0) {
                negative = true;
                NumList_removeAt(new, i);
            }
        }
    };
    NumList_print(new);
    NumList_free(new);
    return 0;
}