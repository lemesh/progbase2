#include <numlist.h>

void NumList_print(NumList * self);
double NumList_average(NumList * self);
int NumList_minIndex(NumList * self);
int NumList_maxIndex(NumList * self);